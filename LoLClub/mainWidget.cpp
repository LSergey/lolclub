#include "mainWidget.h"
#include "ui_mainWidget.h"
#include <QPalette>
#include <QAction>
#include <QActionEvent>
#include <QList>
#include <QNetworkCookie>
#include <QScrollBar>
#include <QDebug>

MainWidget::MainWidget(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWidget),
    settings("TriangleLabs", "JokesReader")
{
    bashOrgGet = new BashOrgGet();
    bashImGet = new BashImGet();
    iBashOrgGet = new IBashOrgGet();
    qdbUsGet = new QdbUsGet();
    itHappensRuGet = new ItHappensRuGet();
    mmtComGet = new MMTComGet();
    tjBashOrgGet = new TjBashOrgGet();
    shortikiComGet = new ShortikiComGet();
    ibashDeGet = new IbashDeGet();
    gBashOrgGet = new GBashOrgGet();
    danstonchatComGet = new DanstonchatComGet();
    laBananeOrgGet = new LaBananeOrgGet();

    saveManager_ = new SaveManager();
    loadManager_ = new LoadManager();
    deleteManager_ = new DeleteLocalPagesManager();
    updateManager_ = new UpdateManager();
    donateWindow = new DonateWindow();

    cookieJar_ = new CookieJar();
    startStatisticManager = new QNetworkAccessManager();

    readSettings();

    splash = new QSplashScreen();
    ui->setupUi(this);

    this->setGeometry(geometry_);
    this->resize(geometry_.size());
    setConnections();

    if (state_ == STANDARTRUN)
    {
        emit(loadLastPage());
        ui->scrollArea->verticalScrollBar()->setValue(lastScrollPosition_);
    } else if (state_ == FIRSTRUN)
    {
        this->resize(800, 600);
        showTotorialPage();
    } else if (state_ == UPDATEDRUN)
    {
        emit(loadLastPage());
        ui->scrollArea->verticalScrollBar()->setValue(lastScrollPosition_);
        state_ = STANDARTRUN;
    }

    updateManager_->checkForUpdate();
    startStatisticManager->get(QNetworkRequest(QUrl(QString("https://sites.google.com/site/somethingforme123456/"))));
}

MainWidget::~MainWidget()
{
    if (QDir("NewExec").exists())
    {
        if (!QDir("Closed").exists())
            QDir().mkdir("Closed");

        state_ = UPDATEDRUN;

        QProcess a;
        a.startDetached("runner.exe");
    }

    writeSettings();
    deleteObjects();

    delete ui;
}

void MainWidget::deleteObjects()
{
    delete bashOrgGet;
    delete bashImGet;
    delete iBashOrgGet;
    delete qdbUsGet;
    delete itHappensRuGet;
    delete mmtComGet;
    delete tjBashOrgGet;
    delete shortikiComGet;
    delete ibashDeGet;
    delete gBashOrgGet;
    delete danstonchatComGet;
    delete laBananeOrgGet;

    delete saveManager_;
    delete loadManager_;
    delete deleteManager_;
    delete updateManager_;
    delete donateWindow;

    delete cookieJar_;
    delete startStatisticManager;
}

void MainWidget::setConnections()
{
    setBashOrgConnections();
    setBashImConnections();
    setIBashOrgConnections();
    setQdbUsConnections();
    setItHappensRuConnections();
    setMmtComConnections();
    setTjBashOrgConnections();
    setShortikiComConnections();
    setIbashDeConnections();
    setGBashOrgConnections();
    setDanstonchatComConnections();
    setLaBananeOrgConnections();

    connect(ui->actionNext, SIGNAL(triggered()), this, SLOT(nextPage()));
    connect(ui->actionPrevious, SIGNAL(triggered()), this, SLOT(previousPage()));
    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(savePage()));
    connect(ui->actionSavedPagesList, SIGNAL(triggered()), loadManager_, SLOT(load()));
    connect(ui->actionMyTop, SIGNAL(triggered()), loadManager_, SLOT(loadTop()));
    connect(ui->actionDelete_saved_pages, SIGNAL(triggered()), deleteManager_, SLOT(deletePages()));
    connect(this, SIGNAL(save(SITE,SECTIONS)), saveManager_, SLOT(save(SITE, SECTIONS)));
    connect(loadManager_, SIGNAL(loadResultToShow(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)),
            this, SLOT(showParsedPage(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)));
    connect(loadManager_, SIGNAL(loadTopResults(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)),
            this, SLOT(showParsedPage(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)));

    //save/load manager connections
    connect(this, SIGNAL(prepearToSave(QVector<QString>&,QVector<QString>&,QVector<QString>&, SITE, SECTIONS)),
            saveManager_, SLOT(prepearToSave(QVector<QString>&,QVector<QString>&,QVector<QString>&, SITE, SECTIONS)));
    connect(this, SIGNAL(saveToTopPageSignal(QString&,QString&,SITE)), saveManager_,
            SLOT(saveToTopPage(QString&,QString&,SITE)));
    connect(saveManager_, SIGNAL(saved()), this, SLOT(showSavedImg()));
    connect(this, SIGNAL(loadLastPage()), loadManager_, SLOT(loadLocal()));

    //clear social cookie connection
    connect(ui->actionLog_out_socials, SIGNAL(triggered()), this, SLOT(clearCookie()));
    connect(ui->actionColor_mode_day_night, SIGNAL(triggered()), this, SLOT(changeColorMode()));
    connect(ui->actionSay_thank_you, SIGNAL(triggered()), this, SLOT(showDonatePage()));
    connect(ui->actionHow_to, SIGNAL(triggered()), this, SLOT(showTotorialPage()));
}

void MainWidget::showParsedPage(QVector<QString>& text, QVector<QString>& number,
                                QVector<QString>& raiting, SITE site, SECTIONS section)
{
    delete ui->scrollArea->widget();
    if (colorStyle == DAY)
        ui->centralwidget->setStyleSheet("");
    else
        ui->centralwidget->setStyleSheet("background-color: black; color : white;");
    if (state_ != UPDATEDRUN)
    {
        state_ = STANDARTRUN;
    }
    emit prepearToSave(text, number, raiting, site, section);
    text_.clear();
    raiting_.clear();
    number_.clear();
    text_.resize(text.size());
    raiting_.resize(raiting.size());
    number_.resize(number.size());
    for (int i = 0; i < text.size(); i++)
    {
#ifdef WIN32
{
        QTextCodec *codec = QTextCodec::codecForName("CP1251");
        QTextCodec *codecUnicode = QTextCodec::codecForName("Utf-8");
        QByteArray a;
        a = codec->fromUnicode(text[i]);
        text[i] = codecUnicode->toUnicode(a);
        text_[i] = text[i];
        number_[i] = number[i];
        raiting_[i] = raiting[i];
}
#else
{
        text_[i] = text[i];
        number_[i] = number[i];
        raiting_[i] = raiting[i];
}
#endif
    }


    QVBoxLayout* vbox = new QVBoxLayout;
    QWidget* widget = new QWidget;
    int quotesSize = text.size();
    for (int i = 0; i < quotesSize; i++)
    {
        QuoteView* quoteW = new QuoteView;
        connect(quoteW, SIGNAL(runVkPost(QString&)), this, SLOT(runVkPost(QString&)));
        connect(quoteW, SIGNAL(runFbPost(QString&)), this, SLOT(runFbPost(QString&)));
        connect(quoteW, SIGNAL(saveToTopSignal(QString&, QString&)),
                this, SLOT(saveToTop(QString&, QString&)));

        quoteW->setQuote(text[i],i);
        quoteW->setNumAndRate(number[i], raiting[i], i);

        if (colorStyle == DAY)
        {
            if( i % 2 == 0)
            {
                QPalette Pal(palette());
                Pal.setColor(QPalette::Background, QColor(229,229,229));
                quoteW->setAutoFillBackground(true);
                quoteW->setPalette(Pal);
            }
            else
            {
                QPalette Pal(palette());
                Pal.setColor(QPalette::Background, QColor(178,178,178));
                quoteW->setAutoFillBackground(true);
                quoteW->setPalette(Pal);
            }
        } else if (colorStyle == NIGHT)
        {
            if( i % 2 == 0)
            {
                QPalette Pal(palette());
                Pal.setColor(QPalette::Background, QColor(54,50,50));
                quoteW->setAutoFillBackground(true);
                quoteW->setPalette(Pal);
            }
            else
            {
                QPalette Pal(palette());
                Pal.setColor(QPalette::Background, QColor(18,14,14));
                quoteW->setAutoFillBackground(true);
                quoteW->setPalette(Pal);
            }
        }
        vbox->addWidget(quoteW);
    }

    widget->setLayout(vbox);
    ui->scrollArea->autoFillBackground();
    ui->scrollArea->setWidget(widget);

    site_ = site;
    section_ = section;
}

void MainWidget::showTopPage(QVector<QString>& text, QVector<QString>& number,
                             QVector<QString>& raiting, SITE site, SECTIONS section)
{
    delete ui->scrollArea->widget();
    if (colorStyle == DAY)
        ui->centralwidget->setStyleSheet("");
    else
        ui->centralwidget->setStyleSheet("background-color: black; color : white;");
    if (state_ != UPDATEDRUN)
    {
        state_ = STANDARTRUN;
    }
    emit prepearToSave(text, number, raiting, site, section);
    text_.clear();
    raiting_.clear();
    number_.clear();
    text_.resize(text.size());
    raiting_.resize(raiting.size());
    number_.resize(number.size());
    for (int i = 0; i < text.size(); i++)
    {
#ifdef WIN32
{
//        QTextCodec *codec = QTextCodec::codecForName("CP1251");
//        QTextCodec *codecUnicode = QTextCodec::codecForName("Utf-8");
//        QByteArray a;
//        a = codec->fromUnicode(text[i]);
//        text[i] = codecUnicode->toUnicode(a);
        text_[i] = text[i];
        number_[i] = number[i];
        raiting_[i] = raiting[i];
}
#else
{
        text_[i] = text[i];
        number_[i] = number[i];
        raiting_[i] = raiting[i];
}
#endif
    }


    QVBoxLayout* vbox = new QVBoxLayout;
    QWidget* widget = new QWidget;
    int quotesSize = text.size();
    for (int i = 0; i < quotesSize; i++)
    {
        QuoteView* quoteW = new QuoteView;
        connect(quoteW, SIGNAL(runVkPost(QString&)), this, SLOT(runVkPost(QString&)));
        connect(quoteW, SIGNAL(runFbPost(QString&)), this, SLOT(runFbPost(QString&)));
        connect(quoteW, SIGNAL(saveToTopSignal(QString&, QString&)),
                this, SLOT(saveToTop(QString&, QString&)));

        quoteW->setQuote(text[i],i);
        quoteW->setNumAndRate(number[i], raiting[i], i);

        if (colorStyle == DAY)
        {
            if( i % 2 == 0)
            {
                QPalette Pal(palette());
                Pal.setColor(QPalette::Background, QColor(229,229,229));
                quoteW->setAutoFillBackground(true);
                quoteW->setPalette(Pal);
            }
            else
            {
                QPalette Pal(palette());
                Pal.setColor(QPalette::Background, QColor(178,178,178));
                quoteW->setAutoFillBackground(true);
                quoteW->setPalette(Pal);
            }
        } else if (colorStyle == NIGHT)
        {
            if( i % 2 == 0)
            {
                QPalette Pal(palette());
                Pal.setColor(QPalette::Background, QColor(178,178,178));
                quoteW->setAutoFillBackground(true);
                quoteW->setPalette(Pal);
            }
            else
            {
                QPalette Pal(palette());
                Pal.setColor(QPalette::Background, QColor(229,229,229));
                quoteW->setAutoFillBackground(true);
                quoteW->setPalette(Pal);
            }
        }
        vbox->addWidget(quoteW);
    }

    widget->setLayout(vbox);
    ui->scrollArea->autoFillBackground();
    ui->scrollArea->setWidget(widget);

    site_ = site;
    section_ = section;
}

void MainWidget::showTotorialPage()
{
    ui->centralwidget->setStyleSheet("background-color: white;");
//    this->setStyleSheet("background-color: white;");
    QVBoxLayout* vbox = new QVBoxLayout;
    QWidget* widget = new QWidget;
    TotorialWidget *tw = new TotorialWidget;

    tw->resize(this->size());

    vbox->addWidget(tw);

    widget->setLayout(vbox);
    ui->scrollArea->autoFillBackground();
    ui->scrollArea->setWidget(widget);

}

void MainWidget::writeSettings()
{
    settings.beginGroup("/Settings");

    settings.setValue("/Site", site_);
    settings.setValue("/Section", section_);
    settings.setValue("/Geometry", this->geometry());
    settings.setValue("/ScrollPosition", ui->scrollArea->verticalScrollBar()->value());
    settings.setValue("/Style", colorStyle);
    settings.setValue("/LoadingState", state_);

//    saveLastPageLocal();

    settings.endGroup();
}

void MainWidget::showLoadingImg()
{
    delete splash;
    splash = new QSplashScreen();
    splash->setPixmap(QPixmap(":/images/res/Lol_Club_Ss_Loading.png"));
    splash->setWindowFlags( Qt::FramelessWindowHint | Qt::Popup);
    QRect rect= this->geometry();

    splash->setGeometry(rect.topLeft().x(), rect.topLeft().y(), 600, 400);
    splash->show();
}

void MainWidget::showConnectionFailImg()
{
    delete splash;
    splash = new QSplashScreen();
//    splash->close();
    splash->setPixmap(QPixmap(":/images/res/Lol_Club_Ss_No_connection.png"));
    splash->setWindowFlags( Qt::FramelessWindowHint | Qt::Popup);
    QRect rect= this->geometry();
    splash->setGeometry(rect.topLeft().x(), rect.topLeft().y(), 600, 400);
    splash->show();
    QTimer::singleShot(1000, splash, SLOT(close()));
}

void MainWidget::showSavedImg()
{
    delete splash;
    splash = new QSplashScreen();
    splash->setPixmap(QPixmap(":/images/res/Lol_Club_Ss_Saved.png"));
    splash->setWindowFlags( Qt::FramelessWindowHint | Qt::Popup);
    QRect rect= this->geometry();
    splash->setGeometry(rect.topLeft().x(), rect.topLeft().y(), 600, 400);
    splash->show();
    QTimer::singleShot(1000, splash, SLOT(close()));
}

void MainWidget::showPostedImg()
{
    delete splash;
    splash = new QSplashScreen();
    splash->setPixmap(QPixmap(":/images/res/Lol_Club_Ss_Posted.png"));
    splash->setWindowFlags( Qt::FramelessWindowHint | Qt::Popup);
    QRect rect= this->geometry();
    splash->setGeometry(rect.topLeft().x(), rect.topLeft().y(), 600, 400);
    splash->show();
    QTimer::singleShot(1000, splash, SLOT(close()));
}

void MainWidget::closeLoadingImg()
{
    splash->close();
}

void MainWidget::readSettings()
{
    settings.beginGroup("/Settings");

    int siteNumber = settings.value("/Site", "1").toInt();
    int sectionNumber = settings.value("/Section", "0").toInt();

    if (siteNumber == BASH_ORG)
    {
        site_ = BASH_ORG;
        switch (sectionNumber)
        {
        case 0 : section_ = BASH_ORG_LATEST;     break;
        case 1 : section_ = BASH_ORG_BROWSE;     break;
        case 2 : section_ = BASH_ORG_RANDOM;     break;
        case 3 : section_ = BASH_ORG_TOP100_200; break;
        }
    }
    else if (siteNumber == BASH_IM)
    {
        site_ = BASH_IM;
        switch (sectionNumber)
        {
        case 5 : section_ = BASH_IM_NEW;        break;
        case 6 : section_ = BASH_IM_RANDOM;     break;
        case 7 : section_ = BASH_IM_BEST;       break;
        case 8 : section_ = BASH_IM_BYRAITING;  break;
        case 9 : section_ = BASH_IM_ABYSS;      break;
        case 10 : section_ = BASH_IM_ABYSSBEST; break;
        case 11 : section_ = BASH_IM_ABYSSTOP;  break;
        }
    }
    else if (siteNumber == IBASH_ORG)
    {
        site_ = IBASH_ORG;
        switch (sectionNumber)
        {
        case 12 : section_ = IBASH_ORG_BYDATE;    break;
        case 13 : section_ = IBASH_ORG_RANDOM;    break;
        case 14 : section_ = IBASH_ORG_BYRAITING; break;
        }
    }
    else if (siteNumber == QDB_US)
    {
        site_ = QDB_US;
        switch (sectionNumber)
        {
        case 15 : section_ == QDB_US_TODAY;  break;
        case 16 : section_ == QDB_US_LATEST; break;
        case 17 : section_ == QDB_US_BEST;   break;
        case 18 : section_ == QDB_US_TOP;    break;
        case 19 : section_ == QDB_US_RANDOM; break;
        }
    }
    else if (siteNumber == ITHAPPENS_RU)
    {
        site_ = ITHAPPENS_RU;
        switch (sectionNumber)
        {
        case 20 : section_ = ITHAPPENS_RU_NEW;    break;
        case 21 : section_ = ITHAPPENS_RU_BEST;   break;
        case 22 : section_ = ITHAPPENS_RU_RANDOM; break;
        }
    }
    else if (siteNumber == MMT_COM)
    {
        site_ = MMT_COM;
        switch (sectionNumber)
        {
        case 23 : section_ = MMT_NEW;    break;
        case 24 : section_ = MMT_RANDOM; break;
        case 25 : section_ = MMT_TOP;    break;
        }
    }
    else if (siteNumber == TJBASH_ORG)
    {
        site_ = TJBASH_ORG;
        switch (sectionNumber)
        {
        case 26: section_ = TJBASH_ORG_BROWSE;
        case 27: section_ = TJBASH_ORG_RANDOM;
        case 28: section_ = TJBASH_ORG_TOP;
        }
    }
    else if (siteNumber == SHORTIKI_COM)
    {
        site_ = SHORTIKI_COM;
        switch (sectionNumber)
        {
        case 29: section_ = SHORTIKI_NEW;       break;
        case 30: section_ = SHORTIKI_RANDOM;    break;
        case 31: section_ = SHORTIKI_BYRAITING; break;
        case 32: section_ = SHORTIKI_TOP;       break;
        }
    }
    else if (siteNumber == IBASH_DE)
    {
        site_ = IBASH_DE;
        switch (sectionNumber)
        {
        case 33: section_ = IBASH_DE_NEW;    break;
        case 34: section_ = IBASH_DE_TOP;    break;
        case 35: section_ = IBASH_DE_RANDOM; break;
        }
    }
    else if (siteNumber == GBASH_ORG)
    {
        site_ = GBASH_ORG;
        switch (sectionNumber)
        {
        case 35: section_ = GBASH_ORG_LATEST;  break;
        case 36: section_ = GBASH_ORG_BROWSE;  break;
        case 37: section_ = GBASH_ORG_RANDOM;  break;
        case 38: section_ = GBASH_ORG_TOP;     break;
        }
    }
    else if (siteNumber == DANSTONCHAT_COM)
    {
        site_ = DANSTONCHAT_COM;
        switch (sectionNumber)
        {
        case 39: section_ = DANSTONCHAT_COM_LATEST; break;
        case 40: section_ = DANSTONCHAT_COM_TOP50;  break;
        case 41: section_ = DANSTONCHAT_COM_RANDOM; break;
        case 42: section_ = DANSTONCHAT_COM_BROWSE; break;
        }
    }
    else if (siteNumber == LABANANE_ORG)
    {
        site_ = LABANANE_ORG;
        switch (sectionNumber)
        {
        case 43: section_ = LABANANE_ORG_LATEST;  break;
        case 44: section_ = LABANANE_ORG_BROWSE;  break;
        case 45: section_ = LABANANE_ORG_RANDOM;  break;
        case 46: section_ = LABANANE_ORG_TOP;     break;
        }
    }

    int stateNumber = settings.value("/LoadingState", 0).toInt();
    if (stateNumber == 0)
        state_ = FIRSTRUN;
    else if (stateNumber == 1)
        state_ = UPDATEDRUN;
    else if (stateNumber == 2)
        state_ = STANDARTRUN;
    else
        state_ = FIRSTRUN;

    geometry_ = settings.value("/Geometry", "800, 600").toRect();
    lastScrollPosition_ = settings.value("/ScrollPosition", 0).toInt();

    int styleNumber = settings.value("/Style", 0).toInt();
    if (styleNumber == 0)
        colorStyle = DAY;
    else if (styleNumber == 1)
        colorStyle = NIGHT;

    settings.endGroup();
}

void MainWidget::nextPage()
{
    switch (site_)
    {
    case BASH_ORG:         emit(bashOrgNextSignal(section_));        break;
    case BASH_IM:          emit(bashImNextSignal(section_));         break;
    case IBASH_ORG:        emit(iBashOrgNextSignal(section_));       break;
    case QDB_US:           emit(qdbUsNextSignal(section_));          break;
    case ITHAPPENS_RU:     emit(itHappensRuNextSignal(section_));    break;
    case MMT_COM:          emit(mmtComNextSignal(section_));         break;
    case TJBASH_ORG:       emit(tjBashOrgNextSignal(section_));      break;
    case SHORTIKI_COM:     emit(shortikiComNextSignal(section_));    break;
    case IBASH_DE:         emit(ibashDeNextSignal(section_));        break;
    case GBASH_ORG:        emit(gBashOrgNextSignal(section_));       break;
    case DANSTONCHAT_COM:  emit(danstonchatComNextSignal(section_)); break;
    case LABANANE_ORG:     emit(laBananeOrgGetNextSignal(section_)); break;
    }
}

void MainWidget::previousPage()
{
    switch (site_)
    {
    case BASH_ORG:          emit(bashOrgPreviousSignal(section_));          break;
    case BASH_IM:           emit(bashImPreviousSignal(section_));           break;
    case IBASH_ORG:         emit(iBashOrgPreviousSignal(section_));         break;
    case QDB_US:            emit(qdbUsPrevioseSignal(section_));            break;
    case ITHAPPENS_RU:      emit(itHappensRuPrevioseSignal(section_));      break;
    case MMT_COM:           emit(mmtComPrevioseSignal(section_));           break;
    case TJBASH_ORG:        emit(tjBashOrgPrevioseSignal(section_));        break;
    case SHORTIKI_COM:      emit(shortikiComPrevioseSignal(section_));      break;
    case IBASH_DE:          emit(ibashDePrevioseSignal(section_));          break;
    case GBASH_ORG:         emit(gBashOrgPrevioseSignal(section_));         break;
    case DANSTONCHAT_COM:   emit(danstonchatComPrevioseSignal(section_));   break;
    case LABANANE_ORG:      emit(laBananeOrgGetPrevioseSignal(section_));   break;
    }
}

void MainWidget::savePage()
{
    switch (site_)
    {
    case BASH_ORG:          emit(save(site_, section_)); break;
    case BASH_IM:           emit(save(site_, section_)); break;
    case IBASH_ORG:         emit(save(site_, section_)); break;
    case QDB_US:            emit(save(site_, section_)); break;
    case ITHAPPENS_RU:      emit(save(site_, section_)); break;
    case MMT_COM:           emit(save(site_, section_)); break;
    case TJBASH_ORG:        emit(save(site_, section_)); break;
    case SHORTIKI_COM:      emit(save(site_, section_)); break;
    case IBASH_DE:          emit(save(site_, section_)); break;
    case GBASH_ORG:         emit(save(site_, section_)); break;
    case DANSTONCHAT_COM:   emit(save(site_, section_)); break;
    case LABANANE_ORG:      emit(save(site_, section_)); break;
    }
}

void MainWidget::saveToTop(QString &numAndRait, QString &text)
{
    emit (saveToTopPageSignal(numAndRait, text, site_));
}

void MainWidget::runVkPost(QString &text)
{
    vk_ = new VKAuth("4046058", 0, text, cookieJar_);
    connect(vk_, SIGNAL(cookie(CookieJar*)), this, SLOT(setNewCookie(CookieJar*)));
    connect(vk_, SIGNAL(posted()), this, SLOT(showPostedImg()));
    Qt::WindowFlags flags = this->windowFlags();
    vk_->setWindowFlags(flags | Qt::CustomizeWindowHint | Qt::WindowStaysOnTopHint);
//    if (cookieJar_->cookiesForUrl(QUrl("https://oauth.vk.com/authorize?")).size() < 2)
        vk_->show();
}

void MainWidget::setNewCookie(CookieJar *jar)
{
    cookieJar_ = jar;
    cookieJar_ = new CookieJar();
}

void MainWidget::runFbPost(QString &text)
{
    fb_ = new FbAuth("176762725832967", 0, text, cookieJar_);
    connect(fb_, SIGNAL(cookie(CookieJar*)), this, SLOT(setNewCookie(CookieJar*)));
    connect(fb_, SIGNAL(posted()), this, SLOT(showPostedImg()));
    Qt::WindowFlags flags = this->windowFlags();
    fb_->setWindowFlags(flags | Qt::CustomizeWindowHint | Qt::WindowStaysOnTopHint);
//    if (cookieJar_->cookiesForUrl(QUrl("https://graph.facebook.com/oauth/authorize?")).size() < 1)
        fb_->show();
}

void MainWidget::clearCookie()
{
    delete cookieJar_;

    cookieJar_ = new CookieJar();
    cookieJar_->save();
    cookieJar_->loadSettings();
}

void MainWidget::changeColorMode()
{
    switch (colorStyle)
    {
    case 0: colorStyle = NIGHT; break;
    case 1: colorStyle = DAY; break;
    }

    QVector<QString> t, n, r;
    t.resize(text_.size());
    r.resize(raiting_.size());
    n.resize(number_.size());
    for (int i = 0; i < text_.size(); i++)
    {
        t[i] = text_[i];
        n[i] = number_[i];
        r[i] = raiting_[i];
    }
    showTopPage(t, n, r, site_, section_);
}

void MainWidget::showDonatePage()
{
    donateWindow->show();
}

void MainWidget::setBashOrgConnections()
{
    connect(bashOrgGet, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&, SITE, SECTIONS)), this,
            SLOT(showParsedPage(QVector<QString>&, QVector<QString>& , QVector<QString>&, SITE, SECTIONS)));

    connect(ui->actionLatestBashOrg, SIGNAL(triggered()), bashOrgGet, SLOT(getLatestPage()));
    connect(ui->actionBrowseBashOrg, SIGNAL(triggered()), bashOrgGet, SLOT(getBrowsePage()));
    connect(ui->actionRandomBashOrg, SIGNAL(triggered()), bashOrgGet, SLOT(getRandomPage()));
    connect(ui->actionTopBashOrg, SIGNAL(triggered()), bashOrgGet, SLOT(getTopp100_200Page()));

    connect(this, SIGNAL(bashOrgNextSignal(SECTIONS)), bashOrgGet, SLOT(nextPage(SECTIONS)));
    connect(this, SIGNAL(bashOrgPreviousSignal(SECTIONS)), bashOrgGet, SLOT(previousePage(SECTIONS)));

    connect(bashOrgGet, SIGNAL(loading()), this, SLOT(showLoadingImg()));
    connect(bashOrgGet, SIGNAL(closeLoading()), this, SLOT(closeLoadingImg()));
    connect(bashOrgGet, SIGNAL(connectionFail()), this, SLOT(showConnectionFailImg()));
}

void MainWidget::setBashImConnections()
{
    connect(bashImGet, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&, SITE, SECTIONS)), this,
            SLOT(showParsedPage(QVector<QString>& , QVector<QString>& , QVector<QString>&, SITE, SECTIONS)));

    connect(ui->actionNewBashIm, SIGNAL(triggered()), bashImGet, SLOT(getBashImPage()));
    connect(ui->actionRandomBashIm, SIGNAL(triggered()), bashImGet, SLOT(getRandomPage()));
    connect(ui->actionBestBashIm, SIGNAL(triggered()), bashImGet, SLOT(getBestPage()));
    connect(ui->actionByRaitingBashIm, SIGNAL(triggered()), bashImGet, SLOT(getByRaitingPage()));
    connect(ui->actionAbyssBashIm, SIGNAL(triggered()), bashImGet, SLOT(getAbyssPage()));
    connect(ui->actionAbyssBestBashIm, SIGNAL(triggered()), bashImGet, SLOT(getAbyssBestPage()));
    connect(ui->actionAbyssBestBashIm, SIGNAL(triggered()), bashImGet, SLOT(getNumOfAbyssbestPages()));
    connect(ui->actionAbyssTopBashIm, SIGNAL(triggered()), bashImGet, SLOT(getAbyssTopPage()));

    connect(this, SIGNAL(bashImNextSignal(SECTIONS)), bashImGet, SLOT(nextPage(SECTIONS)));
    connect(this, SIGNAL(bashImPreviousSignal(SECTIONS)), bashImGet, SLOT(previousPage(SECTIONS)));
//    connect(this, SIGNAL(changeBashImPageNumber(QString,SECTIONS)), bashImGet, SLOT(changePage(QString,SECTIONS)));
//    connect(this, SIGNAL(changeSection(SECTIONS)), bashImGet, SLOT(changeSection(SECTIONS)));

    connect(bashImGet, SIGNAL(loading()), this, SLOT(showLoadingImg()));
    connect(bashImGet, SIGNAL(closeLoading()), this, SLOT(closeLoadingImg()));
    connect(bashImGet, SIGNAL(connectionFail()), this, SLOT(showConnectionFailImg()));
}

void MainWidget::setIBashOrgConnections()
{
    connect(iBashOrgGet, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)), this,
            SLOT(showParsedPage(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)));

    connect(ui->actionNewIbashOrg, SIGNAL(triggered()), iBashOrgGet, SLOT(getIBashPage()));
    connect(ui->actionByRaitingIBashOrg, SIGNAL(triggered()), iBashOrgGet, SLOT(getByRaitingPage()));
    connect(ui->actionRandomIbashOrg, SIGNAL(triggered()), iBashOrgGet, SLOT(getRandomPage()));

    connect(this, SIGNAL(iBashOrgNextSignal(SECTIONS)), iBashOrgGet, SLOT(getNextPage()));
    connect(this, SIGNAL(iBashOrgPreviousSignal(SECTIONS)), iBashOrgGet, SLOT(getPreviosePage()));

    connect(iBashOrgGet, SIGNAL(loading()), this, SLOT(showLoadingImg()));
    connect(iBashOrgGet, SIGNAL(closeLoading()), this, SLOT(closeLoadingImg()));
    connect(iBashOrgGet, SIGNAL(connectionFail()), this, SLOT(showConnectionFailImg()));
}

void MainWidget::setQdbUsConnections()
{
    connect(qdbUsGet, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)), this,
            SLOT(showParsedPage(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)));

    connect(ui->actionTodayQdbUs, SIGNAL(triggered()), qdbUsGet, SLOT(getTodayPage()));
    connect(ui->actionLatestQdbUs, SIGNAL(triggered()), qdbUsGet, SLOT(getLatestPage()));
    connect(ui->actionBestQDbUs, SIGNAL(triggered()), qdbUsGet, SLOT(getBestPage()));
    connect(ui->actionTopQdbUs, SIGNAL(triggered()), qdbUsGet, SLOT(getTopPage()));
    connect(ui->actionRandomQdbUs, SIGNAL(triggered()), qdbUsGet, SLOT(getRandomPage()));

    connect(this, SIGNAL(qdbUsNextSignal(SECTIONS)), qdbUsGet, SLOT(getNextPage(SECTIONS)));
    connect(this, SIGNAL(qdbUsPrevioseSignal(SECTIONS)), qdbUsGet, SLOT(getPreviosePage(SECTIONS)));

    connect(qdbUsGet, SIGNAL(loading()), this, SLOT(showLoadingImg()));
    connect(qdbUsGet, SIGNAL(closeLoading()), this, SLOT(closeLoadingImg()));
    connect(qdbUsGet, SIGNAL(connectionFail()), this, SLOT(showConnectionFailImg()));
}

void MainWidget::setItHappensRuConnections()
{
    connect(itHappensRuGet, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)), this,
            SLOT(showParsedPage(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)));

    connect(ui->actionNewItHappensRu, SIGNAL(triggered()), itHappensRuGet, SLOT(getNewPage()));
    connect(ui->actionBestitHappensRu, SIGNAL(triggered()), itHappensRuGet, SLOT(getBestPage()));
    connect(ui->actionRandomItHappensRu, SIGNAL(triggered()), itHappensRuGet, SLOT(getRandomPage()));

    connect(this, SIGNAL(itHappensRuNextSignal(SECTIONS)), itHappensRuGet, SLOT(getNextPage()));
    connect(this, SIGNAL(itHappensRuPrevioseSignal(SECTIONS)), itHappensRuGet, SLOT(getPreviosePage()));

    connect(itHappensRuGet, SIGNAL(loading()), this, SLOT(showLoadingImg()));
    connect(itHappensRuGet, SIGNAL(closeLoading()), this, SLOT(closeLoadingImg()));
    connect(itHappensRuGet, SIGNAL(connectionFail()), this, SLOT(showConnectionFailImg()));
}

void MainWidget::setMmtComConnections()
{
    connect(mmtComGet, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)), this,
            SLOT(showParsedPage(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)));

    connect(ui->actionNewMMTCom, SIGNAL(triggered()), mmtComGet, SLOT(getNewPage()));
    connect(ui->actionRandomMMTCom, SIGNAL(triggered()), mmtComGet, SLOT(getRandomPage()));
    connect(ui->actionBestMMTCom, SIGNAL(triggered()), mmtComGet, SLOT(getTopPage()));

    connect(this, SIGNAL(mmtComNextSignal(SECTIONS)), mmtComGet, SLOT(getNextPage(SECTIONS)));
    connect(this, SIGNAL(mmtComPrevioseSignal(SECTIONS)), mmtComGet, SLOT(getPreviosePage(SECTIONS)));

    connect(mmtComGet, SIGNAL(loading()), this, SLOT(showLoadingImg()));
    connect(mmtComGet, SIGNAL(closeLoading()), this, SLOT(closeLoadingImg()));
    connect(mmtComGet, SIGNAL(connectionFail()), this, SLOT(showConnectionFailImg()));
}

void MainWidget::setTjBashOrgConnections()
{
    connect(tjBashOrgGet, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)), this,
            SLOT(showParsedPage(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)));

    connect(ui->actionBrowseTjBashOrg, SIGNAL(triggered()), tjBashOrgGet, SLOT(getBrowsePage()));
    connect(ui->actionRandom_0TjBashOrg, SIGNAL(triggered()), tjBashOrgGet, SLOT(getRandomPage()));
    connect(ui->actionTopTjBashOrg, SIGNAL(triggered()), tjBashOrgGet, SLOT(getTopPage()));

    connect(this, SIGNAL(tjBashOrgNextSignal(SECTIONS)), tjBashOrgGet, SLOT(getNextPage(SECTIONS)));
    connect(this, SIGNAL(tjBashOrgPrevioseSignal(SECTIONS)), tjBashOrgGet, SLOT(getPreviosePage(SECTIONS)));

    connect(tjBashOrgGet, SIGNAL(loading()), this, SLOT(showLoadingImg()));
    connect(tjBashOrgGet, SIGNAL(closeLoading()), this, SLOT(closeLoadingImg()));
    connect(tjBashOrgGet, SIGNAL(connectionFail()), this, SLOT(showConnectionFailImg()));
}

void MainWidget::setShortikiComConnections()
{
    connect(shortikiComGet, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)), this,
            SLOT(showParsedPage(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)));

    connect(ui->actionNewShorikiCom, SIGNAL(triggered()), shortikiComGet, SLOT(getNewPage()));
    connect(ui->actionRandomShortikiCom, SIGNAL(triggered()), shortikiComGet, SLOT(getRandomPage()));
    connect(ui->actionTopShortikiCom, SIGNAL(triggered()), shortikiComGet, SLOT(getTopPage()));
    connect(ui->actionBy_raitingShortikiCom, SIGNAL(triggered()), shortikiComGet, SLOT(getByRaitingPage()));

    connect(this, SIGNAL(shortikiComNextSignal(SECTIONS)), shortikiComGet, SLOT(getNextPage()));
    connect(this, SIGNAL(shortikiComPrevioseSignal(SECTIONS)), shortikiComGet, SLOT(getPreviosePage()));

    connect(shortikiComGet, SIGNAL(loading()), this, SLOT(showLoadingImg()));
    connect(shortikiComGet, SIGNAL(closeLoading()), this, SLOT(closeLoadingImg()));
    connect(shortikiComGet, SIGNAL(connectionFail()), this, SLOT(showConnectionFailImg()));
}

void MainWidget::setIbashDeConnections()
{
    connect(ibashDeGet, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)), this,
            SLOT(showParsedPage(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)));

    connect(ui->actionNeuIbashDe, SIGNAL(triggered()), ibashDeGet, SLOT(getNewPage()));
    connect(ui->actionBesteIbashDe, SIGNAL(triggered()), ibashDeGet, SLOT(getTopPage()));
    connect(ui->actionZuffalIbashDe, SIGNAL(triggered()), ibashDeGet, SLOT(getRandomPage()));

    connect(this, SIGNAL(ibashDeNextSignal(SECTIONS)), ibashDeGet, SLOT(getNextPage(SECTIONS)));
    connect(this, SIGNAL(ibashDePrevioseSignal(SECTIONS)), ibashDeGet, SLOT(getPreviosePage(SECTIONS)));

    connect(ibashDeGet, SIGNAL(loading()), this, SLOT(showLoadingImg()));
    connect(ibashDeGet, SIGNAL(closeLoading()), this, SLOT(closeLoadingImg()));
    connect(ibashDeGet, SIGNAL(connectionFail()), this, SLOT(showConnectionFailImg()));
}

void MainWidget::setGBashOrgConnections()
{
    connect(gBashOrgGet, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)), this,
            SLOT(showParsedPage(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)));

    connect(ui->actionNeuesGBashOrg, SIGNAL(triggered()), gBashOrgGet, SLOT(getLatestPage()));
    connect(ui->actionSt_bernGBashOrg, SIGNAL(triggered()), gBashOrgGet, SLOT(getBrowsePage()));
    connect(ui->actionTopGBashOrg, SIGNAL(triggered()), gBashOrgGet, SLOT(getTopPage()));
    connect(ui->actionZufallGBashOrg, SIGNAL(triggered()), gBashOrgGet, SLOT(getRandomPage()));

    connect(this, SIGNAL(gBashOrgNextSignal(SECTIONS)), gBashOrgGet, SLOT(getNextPage(SECTIONS)));
    connect(this, SIGNAL(gBashOrgPrevioseSignal(SECTIONS)), gBashOrgGet, SLOT(getPreviosePage(SECTIONS)));

    connect(gBashOrgGet, SIGNAL(loading()), this, SLOT(showLoadingImg()));
    connect(gBashOrgGet, SIGNAL(closeLoading()), this, SLOT(closeLoadingImg()));
    connect(gBashOrgGet, SIGNAL(connectionFail()), this, SLOT(showConnectionFailImg()));
}

void MainWidget::setDanstonchatComConnections()
{
    connect(danstonchatComGet, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)), this,
            SLOT(showParsedPage(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)));

    connect(ui->actionLatestDastonchat, SIGNAL(triggered()), danstonchatComGet, SLOT(getLatestPage()));
    connect(ui->actionBroweDanstonchat, SIGNAL(triggered()), danstonchatComGet, SLOT(getBrowsePage()));
    connect(ui->actionRandomDanstonchat, SIGNAL(triggered()), danstonchatComGet, SLOT(getRandomPage()));
    connect(ui->actionTopDanstonchat, SIGNAL(triggered()), danstonchatComGet, SLOT(getTopPage()));

    connect(this, SIGNAL(danstonchatComNextSignal(SECTIONS)), danstonchatComGet, SLOT(getNextPage(SECTIONS)));
    connect(this, SIGNAL(danstonchatComPrevioseSignal(SECTIONS)), danstonchatComGet, SLOT(getPreviosePage(SECTIONS)));

    connect(danstonchatComGet, SIGNAL(loading()), this, SLOT(showLoadingImg()));
    connect(danstonchatComGet, SIGNAL(closeLoading()), this, SLOT(closeLoadingImg()));
    connect(danstonchatComGet, SIGNAL(connectionFail()), this, SLOT(showConnectionFailImg()));
}

void MainWidget::setLaBananeOrgConnections()
{
    connect(laBananeOrgGet, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)), this,
            SLOT(showParsedPage(QVector<QString>&,QVector<QString>&,QVector<QString>&,SITE,SECTIONS)));

    connect(ui->actionLatestBanane, SIGNAL(triggered()), laBananeOrgGet, SLOT(getLatestPage()));
    connect(ui->actionBrowseBanane, SIGNAL(triggered()), laBananeOrgGet, SLOT(getBrowsePage()));
    connect(ui->actionRandomBanane, SIGNAL(triggered()), laBananeOrgGet, SLOT(getRandomPage()));
    connect(ui->actionTopBanane, SIGNAL(triggered()), laBananeOrgGet, SLOT(getTopPage()));

    connect(this, SIGNAL(laBananeOrgGetNextSignal(SECTIONS)), laBananeOrgGet, SLOT(getNextPage(SECTIONS)));
    connect(this, SIGNAL(laBananeOrgGetPrevioseSignal(SECTIONS)), laBananeOrgGet, SLOT(getPreviosePage(SECTIONS)));

    connect(laBananeOrgGet, SIGNAL(loading()), this, SLOT(showLoadingImg()));
    connect(laBananeOrgGet, SIGNAL(closeLoading()), this, SLOT(closeLoadingImg()));
    connect(laBananeOrgGet, SIGNAL(connectionFail()), this, SLOT(showConnectionFailImg()));
}

void MainWidget::on_actionIf_don_t_find_lovely_site_triggered()
{
    write_to_us *window = new write_to_us();
    if(colorStyle == DAY)
        window->setStyleSheet("background-color: light grey; color : red;");
    else
        window->setStyleSheet("background-color: black; color : red;");
    window->show();
}

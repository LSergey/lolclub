#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QMainWindow>
#include <QWidget>
#include <QPlainTextEdit>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QVBoxLayout>
#include <QPushButton>
#include <QScrollArea>
#include <QSettings>
#include <QSplashScreen>
#include <QStyle>

#include "quoteView.h"
#include "bashOrgGet.h"
#include "state.h"
#include "saveManager.h"
#include "loadManager.h"
#include "bashImGet.h"
#include "cookiejar.h"
#include "vkAuth.h"
#include "fbAuth.h"
#include "iBashOrgGet.h"
#include "qdbUsGet.h"
#include "itHappensRuGet.h"
#include "mmtComGet.h"
#include "tjBashOrgGet.h"
#include "shortikiComGet.h"
#include "deleteLocalPagesManager.h"
#include "ibashDeGet.h"
#include "gBashOrgGet.h"
#include "danstonchatComGet.h"
#include "laBananeOrgGet.h"
#include "updateManager.h"
#include "totorialWidget.h"
#include "donateWindow.h"
#include "write_to_us.h"

namespace Ui {
class MainWidget;
}

class MainWidget : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();
    void deleteObjects();

    CookieJar *cookieJar_;

private:
    Ui::MainWidget *ui;
    QSettings settings;
    QSplashScreen *splash;
    LOADING_STATE state_ = FIRSTRUN;
    QString style;

    SITE site_;
    SECTIONS section_;
    STYLE colorStyle = DAY;
    QRect geometry_;
    int lastScrollPosition_;

    SaveManager *saveManager_;
    LoadManager *loadManager_;
    DeleteLocalPagesManager *deleteManager_;
    UpdateManager *updateManager_;
    DonateWindow *donateWindow;

    BashOrgGet *bashOrgGet;
    BashImGet *bashImGet;
    IBashOrgGet *iBashOrgGet;
    QdbUsGet *qdbUsGet;
    ItHappensRuGet *itHappensRuGet;
    MMTComGet *mmtComGet;
    TjBashOrgGet *tjBashOrgGet;
    ShortikiComGet *shortikiComGet;
    IbashDeGet *ibashDeGet;
    GBashOrgGet *gBashOrgGet;
    DanstonchatComGet *danstonchatComGet;
    LaBananeOrgGet *laBananeOrgGet;

    VKAuth *vk_;
    FbAuth *fb_;
    bool vkCookieEnable_ = false;

    QVector<QString> text_, number_, raiting_;

    // создание соединений
    void setConnections();

    // чтение/запись настроек программы
    void writeSettings();
    void readSettings();

    QNetworkAccessManager *startStatisticManager;

private slots:
    void showParsedPage(QVector<QString>&, QVector<QString>&, QVector<QString>&, SITE, SECTIONS);
    void showTopPage(QVector<QString>&text, QVector<QString>&number, QVector<QString>&raiting, SITE site, SECTIONS section);
    void showTotorialPage();
    void showLoadingImg();
    void showConnectionFailImg();
    void showSavedImg();
    void showPostedImg();
    void closeLoadingImg();

    void nextPage();
    void previousPage();
    void savePage();

    void saveToTop(QString&, QString&);
    void runVkPost(QString&);
    void setNewCookie(CookieJar*);
    void runFbPost(QString&);
    void clearCookie();

    void changeColorMode();

    void showDonatePage();

    void on_actionIf_don_t_find_lovely_site_triggered();

signals:
    void bashOrgNextSignal(SECTIONS);
    void bashOrgPreviousSignal(SECTIONS);
    void bashImNextSignal(SECTIONS);
    void bashImPreviousSignal(SECTIONS);
    void iBashOrgNextSignal(SECTIONS);
    void iBashOrgPreviousSignal(SECTIONS);
    void qdbUsNextSignal(SECTIONS);
    void qdbUsPrevioseSignal(SECTIONS);
    void itHappensRuNextSignal(SECTIONS);
    void itHappensRuPrevioseSignal(SECTIONS);
    void mmtComNextSignal(SECTIONS);
    void mmtComPrevioseSignal(SECTIONS);
    void tjBashOrgNextSignal(SECTIONS);
    void tjBashOrgPrevioseSignal(SECTIONS);
    void shortikiComNextSignal(SECTIONS);
    void shortikiComPrevioseSignal(SECTIONS);
    void ibashDeNextSignal(SECTIONS);
    void ibashDePrevioseSignal(SECTIONS);
    void gBashOrgNextSignal(SECTIONS);
    void gBashOrgPrevioseSignal(SECTIONS);
    void danstonchatComNextSignal(SECTIONS);
    void danstonchatComPrevioseSignal(SECTIONS);
    void laBananeOrgGetNextSignal(SECTIONS);
    void laBananeOrgGetPrevioseSignal(SECTIONS);

    void save(SITE, SECTIONS);
    void saveToTopPageSignal(QString&, QString&, SITE);
    void prepearToSave(QVector<QString>&,QVector<QString>&,QVector<QString>&, SITE, SECTIONS);

    void loadLastPage();

private:
    void setBashOrgConnections();
    void setBashImConnections();
    void setIBashOrgConnections();
    void setQdbUsConnections();
    void setItHappensRuConnections();
    void setMmtComConnections();
    void setTjBashOrgConnections();
    void setShortikiComConnections();
    void setIbashDeConnections();
    void setGBashOrgConnections();
    void setDanstonchatComConnections();
    void setLaBananeOrgConnections();
};

#endif // MAINWIDGET_H

#include "deleteLocalPagesManager.h"

DeleteLocalPagesManager::DeleteLocalPagesManager(QObject *parent) :
    QObject(parent)
{
}

void DeleteLocalPagesManager::deletePages()
{
    QFileDialog *dialog = new QFileDialog();
    QString path = QDir::currentPath();
    QDir currentDir(QDir::currentPath());

    QStringList filesList = QFileDialog::getOpenFileNames(dialog, tr("Delete files"), path + "/Saved/", "");

    for (int i = 0; i < filesList.size(); i++)
    {
        if (filesList[i].contains(path + "/Saved/"))
            currentDir.remove(filesList[i]);
    }
}

#include "updateManager.h"
//#include <QDebug>

UpdateManager::UpdateManager(QObject *parent) :
    QObject(parent)
{
    networkManager = new QNetworkAccessManager;
    loader = NULL;

    urlToCheck = "http://somethingforme.ucoz.ua/index/version/0-4";
    urlToCheckWin32 = "http://somethingforme1.ucoz.ua/index/version/0-4";
    urlToLoadLinuxVersion = "http://somethingforme.ucoz.ua/lolclub.noext";
    urlToLoadWin32Version = "http://somethingforme.ucoz.ua/lolclub";
    version = "1.0.0";

    connect(networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(compareVersions(QNetworkReply*)));
}

UpdateManager::~UpdateManager()
{
    delete networkManager;
    if (loader != NULL)
        delete loader;
}

void UpdateManager::checkForUpdate()
{
    QUrl getVersionUrl(urlToCheck);

    networkManager->get(QNetworkRequest(getVersionUrl));
}

void UpdateManager::update()
{
#ifdef linux
    {
        moveExecToTemp();
        QFile newExec("lolclub");
        newExec.open(QIODevice::WriteOnly /*| QIODevice::Unbuffered*/);
        newExec.write(loader->downloadedData());
        newExec.close();

        QProcess::execute ("chmod +x lolclub");
    }
#else
    {
        if (!QDir("NewExec").exists())
            QDir().mkdir("NewExec");

        QFile newExec("NewExec/lolclub.exe");
        newExec.open(QIODevice::WriteOnly /*| QIODevice::Unbuffered*/);
        newExec.write(loader->downloadedData());
        newExec.close();
    }
#endif
}

void UpdateManager::compareVersions(QNetworkReply *reply)
{
    QString lastVesion;
    if(reply->error() != QNetworkReply::NoError)
        return;
    else
    {
        QByteArray data = reply->readAll();
        QString stringData(data);

        stringData = stringData.mid(stringData.indexOf("Version("));
        lastVesion = stringData.mid(stringData.indexOf("(") + 1, stringData.indexOf(")") - stringData.indexOf("(") - 1);
    }

    if (lastVesion != version)
    {
#ifdef linux
        {
            loader = new FileDownloader(urlToLoadLinuxVersion, this);
            connect(loader, SIGNAL(downloaded()), SLOT(update()));
        }
#else
        {
            loader = new FileDownloader(urlToLoadWin32Version, this);
            connect(loader, SIGNAL(downloaded()), SLOT(update()));
        }
#endif
    }
}

void UpdateManager::moveExecToTemp()
{
    if (!QDir("OldExec").exists())
        QDir().mkdir("OldExec");

    QFile exec("lolclub");
    exec.copy("OldExec/lolclub");
    exec.remove();
}

#include "saveManager.h"
//#include <QDebug>
#include <QXmlStreamWriter>

SaveManager::SaveManager(QObject *parent) :
    QObject(parent)
{
    nameToSaveWindow = new NameToSaveWindow();
    nameToSaveWindow->setWindowFlags(Qt::Window | Qt::CustomizeWindowHint | Qt::Popup);

    connect(this, SIGNAL(setStartName(QString)), nameToSaveWindow, SLOT(setStartOfsavedName(QString)));
    connect(nameToSaveWindow, SIGNAL(nameSavedPage(QString)), this, SLOT(saveToDir(QString)));
}

SaveManager::~SaveManager()
{
delete nameToSaveWindow;
}

void SaveManager::save(SITE site, SECTIONS section)
{
    if (!QDir("Saved").exists())
        QDir().mkdir("Saved");

    if(site == BASH_ORG)
    {
        site_ = site;
        if(!QDir("Saved/Bash_org").exists())
            QDir().mkdir("Saved/Bash_org");

        emit setStartName(QString("bash_org_"));
        nameToSaveWindow->show();
    }
    else if(site == BASH_IM)
    {
        site_ = site;
        if(!QDir("Saved/Bash_im").exists())
            QDir().mkdir("Saved/Bash_im");

        emit setStartName(QString("bash_im_"));
        nameToSaveWindow->show();
    }
    else if(site == IBASH_ORG)
    {
        site_ = site;
        if(!QDir("Saved/IBash_org").exists())
            QDir().mkdir("Saved/IBash_org");

        emit setStartName(QString("ibash_org_"));
        nameToSaveWindow->show();
    }
    else if (site == QDB_US)
    {
        site_ = site;
        if(!QDir("Saved/Qdb_us").exists())
            QDir().mkdir("Saved/Qdb_us");

        emit setStartName(QString("qdb_us_"));
        nameToSaveWindow->show();
    }
    else if (site == ITHAPPENS_RU)
    {
        site_ = site;
        if(!QDir("Saved/Ithappens_ru").exists())
            QDir().mkdir("Saved/Ithappens_ru");

        emit setStartName(QString("ithappens_ru_"));
        nameToSaveWindow->show();
    }
    else if (site == MMT_COM)
    {
        site_ = site;
        if(!QDir("Saved/Makesmethink_com").exists())
            QDir().mkdir("Saved/Makesmethink_com");

        emit setStartName(QString("makesmethink_com_"));
        nameToSaveWindow->show();
    }
    else if (site == TJBASH_ORG)
    {
        site_ = site;
        if(!QDir("Saved/TJbash_org").exists())
            QDir().mkdir("Saved/TJbash_org");

        emit setStartName(QString("tjbash_org_"));
        nameToSaveWindow->show();
    }
    else if (site == SHORTIKI_COM)
    {
        site_ = site;
        if(!QDir("Saved/Shortiki_com").exists())
            QDir().mkdir("Saved/Shortiki_com");

        emit setStartName(QString("shortiki_com_"));
        nameToSaveWindow->show();
    }
    else if (site == IBASH_DE)
    {
        site_ = site;
        if(!QDir("Saved/Ibash_de").exists())
            QDir().mkdir("Saved/Ibash_de");

        emit setStartName(QString("ibash_de_"));
        nameToSaveWindow->show();
    }
    else if (site == GBASH_ORG)
    {
        site_ = site;
        if(!QDir("Saved/german-bash_org").exists())
            QDir().mkdir("Saved/german-bash_org");

        emit setStartName(QString("german-bash_org_"));
        nameToSaveWindow->show();
    }
    else if (site == DANSTONCHAT_COM)
    {
        site_ = site;
        if(!QDir("Saved/danstonchat_com").exists())
            QDir().mkdir("Saved/danstonchat_com");

        emit setStartName(QString("danstonchat_com_"));
        nameToSaveWindow->show();
    }
    else if (site == LABANANE_ORG)
    {
        site_ = site;
        if(!QDir("Saved/labanane_org").exists())
            QDir().mkdir("Saved/labanane_org");

        emit setStartName(QString("labanane_org_"));
        nameToSaveWindow->show();
    }
}

void SaveManager::prepearToSave(QVector<QString>& text, QVector<QString>& number, QVector<QString>& raiting, SITE site, SECTIONS section)
{
    text_ = text;
    number_ = number;
    raiting_ = raiting;
    site_ = site;
    section_ = section;

    tempSave();
}

void SaveManager::saveToDir(QString name)
{
    if(site_ == BASH_ORG)
    {
        QFile outFile( "Saved/Bash_org/" + name );
        if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
          qDebug( "Failed to open file for writing." );
        }

        createXml(&outFile);
        outFile.close();
    }
    else if(site_ == BASH_IM)
    {
        QFile outFile( "Saved/Bash_im/" + name );
        if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
          qDebug( "Failed to open file for writing." );
        }

        createXml(&outFile);
        outFile.close();
    }
    else if(site_ == IBASH_ORG)
    {
        QFile outFile( "Saved/IBash_org/" + name );
        if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
          qDebug( "Failed to open file for writing." );
        }

        createXml(&outFile);
        outFile.close();
    }
    else if(site_ == QDB_US)
    {
        QFile outFile( "Saved/Qdb_us/" + name );
        if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
          qDebug( "Failed to open file for writing." );
        }

        createXml(&outFile);
        outFile.close();
    }
    else if(site_ == ITHAPPENS_RU)
    {
        QFile outFile( "Saved/Ithappens_ru/" + name );
        if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
          qDebug( "Failed to open file for writing." );
        }

        createXml(&outFile);
        outFile.close();
    }
    else if(site_ == MMT_COM)
    {
        QFile outFile( "Saved/Makesmethink_com/" + name );
        if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
          qDebug( "Failed to open file for writing." );
        }

        createXml(&outFile);
        outFile.close();
    }
    else if (site_ == TJBASH_ORG)
    {
        QFile outFile( "Saved/TJbash_org/" + name );
        if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
          qDebug( "Failed to open file for writing." );
        }

        createXml(&outFile);
        outFile.close();
    }
    else if (site_ == SHORTIKI_COM)
    {
        QFile outFile( "Saved/Shortiki_com/" + name );
        if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
          qDebug( "Failed to open file for writing." );
        }

        createXml(&outFile);
        outFile.close();
    }
    else if (site_ == IBASH_DE)
    {
        QFile outFile( "Saved/Ibash_de/" + name );
        if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
          qDebug( "Failed to open file for writing." );
        }

        createXml(&outFile);
        outFile.close();
    }
    else if (site_ == GBASH_ORG)
    {
        QFile outFile( "Saved/german-bash_org/" + name );
        if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
          qDebug( "Failed to open file for writing." );
        }

        createXml(&outFile);
        outFile.close();
    }
    else if (site_ == DANSTONCHAT_COM)
    {
        QFile outFile( "Saved/danstonchat_com/" + name );
        if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
          qDebug( "Failed to open file for writing." );
        }

        createXml(&outFile);
        outFile.close();
    }
    else if (site_ == LABANANE_ORG)
    {
        QFile outFile( "Saved/labanane_org/" + name );
        if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
          qDebug( "Failed to open file for writing." );
        }

        createXml(&outFile);
        outFile.close();
    }
    emit saved();
}

void SaveManager::createXml(QFile* file)
{
//    if (!file->open(QIODevice::WriteOnly))
//    {
//      qDebug( "Failed to open file for writing." );
//    }
//    else
//    {
        QXmlStreamWriter* xmlWriter = new QXmlStreamWriter();
        xmlWriter->setDevice(file);
        xmlWriter->writeStartDocument();

        int siteNum = site_;
        int sectionNum = section_;
        xmlWriter->writeStartElement("quotes");
        xmlWriter->writeStartElement("info");
        xmlWriter->writeAttribute("site", QString::number(siteNum));
        xmlWriter->writeAttribute("section", QString::number(sectionNum));
        xmlWriter->writeCharacters ("");
        xmlWriter->writeEndElement();
        for(int i = 0; i < text_.size(); i++)
        {
            xmlWriter->writeStartElement("quote");
            xmlWriter->writeAttribute("number", number_[i]);
            xmlWriter->writeAttribute("raiting", raiting_[i]);
            xmlWriter->writeCharacters (text_[i]);
            xmlWriter->writeEndElement();
        }

        xmlWriter->writeEndElement();
        xmlWriter->writeEndDocument();
        delete xmlWriter;
//    }
}

void SaveManager::tempSave()
{
    if(!QDir("Temp").exists())
        QDir().mkdir("Temp");

    QString name("lastPage");
    QFile outFile( "Temp/" + name);
    if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
      qDebug( "Failed to open file for writing." );
    }

    createXml(&outFile);
    outFile.close();
}

void SaveManager::saveToTopPage(QString &numAndRait, QString &text, SITE site)
{
    if(!QDir("Top").exists())
        QDir().mkdir("Top");

    QString siteName;

    switch (site)
    {
    case BASH_ORG:      siteName = "bash.org";          break;
    case BASH_IM:       siteName = "bash.im";           break;
    case IBASH_ORG:     siteName = "ibash.org.ru";      break;
    case QDB_US:        siteName = "qdb.us";            break;
    case ITHAPPENS_RU:  siteName = "ithappens.ru";      break;
    case MMT_COM:       siteName = "makesmethink.com";  break;
    case TJBASH_ORG:    siteName = "tjbash.org";        break;
    case SHORTIKI_COM:  siteName = "shortiki.com";      break;
    case IBASH_DE:      siteName = "ibash.de";          break;
    case GBASH_ORG:     siteName = "german-bash.org";   break;
    case DANSTONCHAT_COM: siteName = "danstonchat.com"; break;
    case LABANANE_ORG:  siteName = "labanane.org";      break;
    }

    QDir topDir("Top/");
    int topPagesCount = topDir.count() - 2;
    int pageNum;
    if (topPagesCount == 0)
        pageNum = 0;
    else
        pageNum = topPagesCount - 1;
    QFile outFile( "Top/topQuotes_" + QString::number(pageNum));
    if( !outFile.open( QIODevice::ReadWrite | QIODevice::Text ) )
    {
      qDebug( "Failed to open file for writing." );
    }

    QByteArray fileStr = outFile.readAll();
    if (fileStr.size() == 0)
    {
        QXmlStreamWriter* xmlWriter = new QXmlStreamWriter();
        xmlWriter->setDevice(&outFile);

        xmlWriter->writeStartDocument();
        xmlWriter->writeStartElement("quotes");

        xmlWriter->writeStartElement("quote");
        xmlWriter->writeAttribute("number", numAndRait);
        xmlWriter->writeAttribute("raiting", siteName);
        xmlWriter->writeCharacters (text);
        xmlWriter->writeEndElement();

        delete xmlWriter;
    }
    else
    {
        QDomDocument doc;
        QXmlStreamWriter* xmlWriter = new QXmlStreamWriter();
        LibXml2Reader *reader = new LibXml2Reader();
        QXmlInputSource src;

        src.setData(QString(fileStr));
        doc.setContent(&src, reader);

        QDomElement docElem = doc.documentElement();
        QDomNodeList nodeList = docElem.elementsByTagName("quote");
        int quoteCount = nodeList.size();
        if (quoteCount < 50)
        {
            xmlWriter->setDevice(&outFile);

            xmlWriter->writeStartElement("quote");
            xmlWriter->writeAttribute("number", numAndRait);
            xmlWriter->writeAttribute("raiting", siteName);
            xmlWriter->writeCharacters (text);
            xmlWriter->writeEndElement();

            delete xmlWriter;
        }
        else
        {
            outFile.close();
            QFile outFile( "Top/topQuotes_" + QString::number(pageNum+1));
            if( !outFile.open( QIODevice::ReadWrite | QIODevice::Text ) )
            {
              qDebug( "Failed to open file for writing." );
            }

            QXmlStreamWriter* xmlWriter = new QXmlStreamWriter();
            xmlWriter->setDevice(&outFile);

            xmlWriter->writeStartDocument();
            xmlWriter->writeStartElement("quotes");

            xmlWriter->writeStartElement("quote");
            xmlWriter->writeAttribute("number", numAndRait);
            xmlWriter->writeAttribute("raiting", siteName);
            xmlWriter->writeCharacters (text);
            xmlWriter->writeEndElement();

            delete xmlWriter;
        }
    }
    outFile.close();
    emit saved();
}

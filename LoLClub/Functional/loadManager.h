#ifndef LOADMANAGER_H
#define LOADMANAGER_H

#include <QObject>
#include <QDir>
#include <QFileDialog>
#include <QDomDocument>
#include <QXmlStreamReader>
#include <QtXml/QtXml>
#include <QXmlInputSource>

#include "parserBashOrg.h"
#include "state.h"
#include "libxml2reader.h"

class LoadManager : public QObject
{
    Q_OBJECT
public:
    explicit LoadManager(QObject *parent = 0);

    QVector<QString> text, number, raiting;
signals:
    void loadResultToShow(QVector<QString>&, QVector<QString>&,
                          QVector<QString>&, SITE, SECTIONS);

    void loadTopResults(QVector<QString>&, QVector<QString>&,
                        QVector<QString>&, SITE, SECTIONS);
    
private slots:
    void load();
    void loadLocal();
    void loadTop();
};

#endif // LOADMANAGER_H

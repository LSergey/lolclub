#ifndef DELETELOCALPAGESMANAGER_H
#define DELETELOCALPAGESMANAGER_H

#include <QObject>
#include <QFileDialog>

class DeleteLocalPagesManager : public QObject
{
    Q_OBJECT
public:
    explicit DeleteLocalPagesManager(QObject *parent = 0);
    
public slots:
    void deletePages();
    
};

#endif // DELETELOCALPAGESMANAGER_H

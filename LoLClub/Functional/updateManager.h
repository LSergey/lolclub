#ifndef UPDATEMANAGER_H
#define UPDATEMANAGER_H

#include <QObject>
#include <QDir>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkReply>
#include <QNetworkRequest>
#include "fileDownloader.h"
#include <QProcess>

class UpdateManager : public QObject
{
    Q_OBJECT
public:
    explicit UpdateManager(QObject *parent = 0);
    ~UpdateManager();
    
signals:
    void updateEnable();
    void updated();
    
public slots:
    void checkForUpdate();
    void moveExecToTemp();
    void update();

private slots:
    void compareVersions(QNetworkReply *reply);

private:
    QString urlToCheck;
    QString urlToCheckWin32;
    QString urlToLoadLinuxVersion;
    QString urlToLoadWin32Version;
    QString version;
    FileDownloader *loader;
    QNetworkAccessManager *networkManager;
};

#endif // UPDATEMANAGER_H

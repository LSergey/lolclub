#include "loadManager.h"


LoadManager::LoadManager(QObject *parent) :
    QObject(parent)
{

}

void LoadManager::load()
{
    QFileDialog *dialog = new QFileDialog();
    QString path = QDir::currentPath();
    dialog->setWindowFlags(dialog->windowFlags() | Qt::WindowStaysOnTopHint);
    QString fileName =  QFileDialog::getOpenFileName(dialog, tr("Open File"), path + "/Saved/", "");
    QFile *pageToLoad = new QFile;

    if(fileName.isEmpty())
        loadLocal();
    else
    {
        pageToLoad->setFileName(fileName);
        QByteArray data;
        if(pageToLoad->open(QIODevice::ReadOnly | QIODevice::Text))
            data = pageToLoad->readAll();
        QXmlStreamReader xml(data);

        text.clear();
        number.clear();
        raiting.clear();
        while(!xml.atEnd() && !xml.hasError())
        {
            QXmlStreamReader::TokenType token = xml.readNext();
            if(token == QXmlStreamReader::StartDocument)
                continue;
            if(token == QXmlStreamReader::StartElement)
            {
                if(xml.name() == "quotes")
                    continue;
                if(xml.name() == "quote")
                {
                    QXmlStreamAttributes attributes = xml.attributes();
                    if(attributes.hasAttribute("number"))
                        number.append(attributes.value("number").toString());
                    if(attributes.hasAttribute("raiting"))
                        raiting.append(attributes.value("raiting").toString());
                    xml.readNext();
                    text.append(xml.text().toString());
                }
        }

        if(xml.hasError())
            qDebug() << "error on xml reading" << xml.lineNumber() << "\n" << xml.errorString();
        }
        xml.clear();

        emit loadResultToShow(text, number, raiting, LOCAL_DIR_SITE, LOCAL_DIR_SECTION);
    }
}

void LoadManager::loadLocal()
{
    QFile *pageToLoad = new QFile;
    pageToLoad->setFileName(QString("Temp/lastPage"));
    QByteArray data;
    if(pageToLoad->open(QIODevice::ReadOnly | QIODevice::Text))
        data = pageToLoad->readAll();
    QXmlStreamReader xml(data);

    text.clear();
    number.clear();
    raiting.clear();
    while(!xml.atEnd() && !xml.hasError())
    {
        QXmlStreamReader::TokenType token = xml.readNext();
        if(token == QXmlStreamReader::StartDocument)
            continue;
        if(token == QXmlStreamReader::StartElement)
        {
            if(xml.name() == "quote")
            {
                QXmlStreamAttributes attributes = xml.attributes();
                if(attributes.hasAttribute("number"))
                    number.append(attributes.value("number").toString());
                if(attributes.hasAttribute("raiting"))
                    raiting.append(attributes.value("raiting").toString());
                xml.readNext();
                text.append(xml.text().toString());
            }
    }

    if(xml.hasError())
        qDebug() << "error on xml reading";
    }
    xml.clear();

    delete pageToLoad;
    emit loadResultToShow(text, number, raiting, LOCAL_DIR_SITE, LOCAL_DIR_SECTION);
}

void LoadManager::loadTop()
{
    QFileDialog *dialog = new QFileDialog();
    dialog->setWindowFlags(dialog->windowFlags() | Qt::WindowStaysOnTopHint);
    QString path = QDir::currentPath();
    QString fileName =  QFileDialog::getOpenFileName(dialog, tr("Open File"), path + "/Top/", "");
    QFile *pageToLoad = new QFile;

    if(fileName.isEmpty())
        loadLocal();
    else
    {
            pageToLoad->setFileName(fileName);
            QByteArray data;
            if(pageToLoad->open(QIODevice::ReadOnly | QIODevice::Text))
                data = pageToLoad->readAll();
            QXmlStreamReader xml(data);

            text.clear();
            number.clear();
            raiting.clear();
            while(!xml.atEnd() && !xml.hasError())
            {
                QXmlStreamReader::TokenType token = xml.readNext();
                if(token == QXmlStreamReader::StartDocument)
                    continue;
                if(token == QXmlStreamReader::StartElement)
                {
        //            if(xml.name() == "quotes")
        //                continue;
                    if(xml.name() == "quote")
                    {
                        QXmlStreamAttributes attributes = xml.attributes();
                        if(attributes.hasAttribute("number"))
                            number.append(attributes.value("number").toString());
                        if(attributes.hasAttribute("raiting"))
                            raiting.append(attributes.value("raiting").toString());
                        xml.readNext();
                        text.append(xml.text().toString());
                    }
            }

            if(xml.hasError())
                qDebug() << "error on xml reading" << xml.lineNumber() << "\n" << xml.errorString();
            }
            xml.clear();

            emit loadTopResults(text, number, raiting, LOCAL_DIR_SITE, LOCAL_DIR_SECTION);
    }
}

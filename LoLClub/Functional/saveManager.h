#ifndef SAVEMANAGER_H
#define SAVEMANAGER_H

#include <QObject>
#include <QDir>
#include <QDomDocument>
#include <QIODevice>
#include <QTextStream>
#include <QXmlInputSource>
#include <QXmlReader>

#include "state.h"
#include "nameToSaveWindow.h"
#include "libxml2reader.h"


class SaveManager : public QObject
{
    Q_OBJECT
public:
    explicit SaveManager(QObject *parent = 0);
    ~SaveManager();
    
signals:
    void saved();
    void setStartName(QString);
    
private slots:
    void save(SITE, SECTIONS);
    void prepearToSave(QVector<QString>&,QVector<QString>&,QVector<QString>&, SITE, SECTIONS);
    void saveToDir(QString);
    void tempSave();
    void saveToTopPage(QString&, QString&, SITE);

private:
    void createXml(QFile*);
private:
    QDomDocument docTosave;
    NameToSaveWindow *nameToSaveWindow;
    QVector<QString> text_, number_, raiting_;
    
    SITE site_;
    SECTIONS section_;
};

#endif // SAVEMANAGER_H

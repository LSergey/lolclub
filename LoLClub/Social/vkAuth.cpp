#include <QUrl>
#include <QUrlQuery>
#include <QRegExp>
#include <QCryptographicHash>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QtXml>
#include "vkAuth.h"
#include <QStringList>
#include <QUrl>
//#include <QDebug>
#include <QWebHistory>
#include <QWebFrame>
#include <QNetworkCookieJar>

void VKAuth::loadLoginPage()
{
    QUrlQuery url("https://oauth.vk.com/authorize?");

    url.addQueryItem("client_id"     ,  m_app   );
    url.addQueryItem("scope"  , "wall"  );
    url.addQueryItem("redirect" , "https://oauth.vk.com/blank.html");
    url.addQueryItem("display", "popup");
    url.addQueryItem("response_type ", "token");

    load(QUrl(url.toString()));
}

VKAuth::VKAuth(QString app,QWidget* parent, QString text, CookieJar *cookie) : QWebView(parent)
{
    cookie_ = new CookieJar();
    cookie_ = cookie;
    quoteText_ = text;
    QObject::connect(this, SIGNAL(urlChanged(QUrl)), SLOT( slotLinkChanged(QUrl)));

    m_app = app;

    m_http = NULL;

    this->page()->networkAccessManager()->setCookieJar(cookie_);
    loadLoginPage();
}

VKAuth::~VKAuth()
{
    delete cookie_;
    if (m_http != NULL)
        delete m_http;
}

void VKAuth::slotLinkChanged(QUrl url)
{
    if(url.toString().indexOf(QString("__q_hash")) != -1)
    {
        int q_hashIndex = url.toString().indexOf(QString("__q_hash"));
        _q_hash = url.toString().mid(q_hashIndex + 9);
    }

    if(url.toString().indexOf(QString("#code=")) != -1)
    {
        int codeIndex = url.toString().indexOf(QString("#code="));
        QString code = url.toString().mid(codeIndex + 6);

        QUrlQuery request("https://api.vk.com/oauth/access_token?");

        request.addQueryItem("client_id",m_app);
        request.addQueryItem("client_secret", "97dMj7WiGScF1qM8xfxa");
        request.addQueryItem("code",code);

        QString req = request.toString().remove(request.toString().indexOf("&"), 1);

        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
        m_http = manager->get(QNetworkRequest(QUrl(req)));
        QObject::connect(m_http,SIGNAL(finished()),this, SLOT(slotDone()));
    }
}

void VKAuth::slotDone()
{
    QString callBack = m_http->readAll();
    int codeIndex = callBack.indexOf(QString("access_token"));
    int coma = callBack.indexOf(QString(","));
    int userIdStrNum = callBack.indexOf("user_id");
    QString code = callBack.mid(codeIndex + 15, coma - 18);
    QString useId = callBack.mid(userIdStrNum + 9);

    QUrlQuery request("https://api.vk.com/method/wall.post.xml?");

    request.addQueryItem("message", quoteText_);
    request.addQueryItem("access_token",code);

    QString req = request.toString().remove(request.toString().indexOf("&"), 1);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    manager->get(QNetworkRequest(QUrl(req)));

    emit cookie(cookie_);
    emit posted();
    this->close();
}

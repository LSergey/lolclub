#include "fbAuth.h"
#include <QUrl>
#include <QUrlQuery>
#include <QRegExp>
#include <QCryptographicHash>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QtXml>
#include <QStringList>
#include <QUrl>
//#include <QDebug>
#include <QWebHistory>
#include <QWebFrame>
#include <QNetworkCookieJar>

FbAuth::FbAuth(QString app,QWidget* parent, QString text, CookieJar *cookie) : QWebView(parent)
{
    cookie_ = new CookieJar();
    cookie_ = cookie;
    quoteText_ = text;
    QObject::connect(this, SIGNAL(urlChanged(QUrl)), SLOT( slotLinkChanged(QUrl)));
    m_app = app;
    m_http = NULL;

    this->page()->networkAccessManager()->setCookieJar(cookie_);
    loadLoginPage();
}

FbAuth::~FbAuth()
{
    delete cookie_;
    if (m_http != NULL)
        delete m_http;
}

void FbAuth::loadLoginPage()
{
    QUrlQuery url("https://graph.facebook.com/oauth/authorize?");

    url.addQueryItem("client_id"     ,  m_app   );
    url.addQueryItem("redirect_uri" , "https://www.facebook.com/connect/login_success.html");
    url.addQueryItem("response_type", "token");
    url.addQueryItem("scope"  , "publish_stream,read_stream"  ); //read_stream,publish_stream

    load(QUrl(url.toString()));
}

void FbAuth::slotLinkChanged(QUrl url)
{
    if(url.toString().indexOf(QString("login_success.html#access_token=")) != -1)
    {
        int codeIndex = url.toString().indexOf(QString("#access_token="));
        QString accessTokenIsFirst = url.toString().mid(codeIndex + 14);
        int accessEnd = accessTokenIsFirst.indexOf(QString("&"));
        accessToken = accessTokenIsFirst.left(accessEnd);

        QUrlQuery request("https://graph.facebook.com/me?");

        request.addQueryItem("access_token",accessToken);

        QString req = request.toString().remove(request.toString().indexOf("&"), 1);

        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
        m_http = manager->get(QNetworkRequest(QUrl(req)));
        QObject::connect(m_http,SIGNAL(finished()),this, SLOT(slotDone()));
    }
}

void FbAuth::slotDone()
{
        QString callBack = m_http->readAll();

        callBack.remove("{\"id\":\"");
        QString id = callBack.left(callBack.indexOf("\",\"name\":"));

        QUrlQuery postData;
        postData.addQueryItem("access_token",accessToken);
        postData.addQueryItem("message", quoteText_);
        postData.addQueryItem("caption", QString("Caption"));

        QString req = QString("https://graph.facebook.com/") + id + QString("/feed");
        QNetworkRequest postRequest(req);

        postRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
        m_http = manager->post(postRequest, postData.toString().toUtf8());

        emit cookie(cookie_);
        emit posted();
        this->close();
}

#ifndef FBAUTH_H
#define FBAUTH_H

#include <QWebView>
#include <QNetworkCookie>

#include <QList>
#include <QNetworkCookie>
#include "cookiejar.h"

class CookieJar;
class QStringList;
class QNetworkReply;
class QByteArray;
class QString;

class FbAuth : public QWebView
{
    Q_OBJECT
protected:
    QString m_app;
    QString m_mid;
    QString m_secret;
    QString m_sid;

    QNetworkReply* m_http;

    void loadLoginPage();
public:
    explicit FbAuth(QString app,QWidget* parent=0, QString text = QString(),
                    CookieJar* cookie = new CookieJar);
    ~FbAuth();
    
signals:
    void success(QStringList);
    void unsuccess();
    void isCookieEnable();
    void cookie(CookieJar*);
    void posted();

public slots:
    void slotLinkChanged(QUrl url);
    void slotDone();

private:
    QString quoteText_;
    QString _q_hash;
    QString accessToken;
    CookieJar *cookie_;
    
};

#endif // FBAUTH_H

QT += widgets network core xml webkit webkitwidgets

INCLUDEPATH += ./UI \
                ./Social \
                ./Parsers \
                ./GetSite \
                ./Functional

win32 {
    INCLUDEPATH += libxml2-2.7.6.win32\include\ \
} else {
    INCLUDEPATH += /usr/include/libxml2
}

win32 {
    LIBS += -Llibxml2-2.7.6.win32\bin -lxml2
    LIBS +=  -Llxml2
} else {
    LIBS += -lxml2
}

RC_FILE = myapp.rc

HEADERS += \
    Parsers/libxml2reader.h \
    UI/quoteView.h \
    mainWidget.h \
    GetSite/bashOrgGet.h \
    state.h \
    Functional/loadManager.h \
    Functional/saveManager.h \
    UI/nameToSaveWindow.h \
    Social/vkAuth.h \
    GetSite/bashImGet.h \
    Parsers/parserBashIm.h \
    Parsers/parserBashOrg.h \
    Social/autosaver.h \
    Social/cookiejar.h \
    Social/fbAuth.h \
    GetSite/iBashOrgGet.h \
    Parsers/parserIBashOrg.h \
    GetSite/qdbUsGet.h \
    Parsers/parserQdbUs.h \
    GetSite/itHappensRuGet.h \
    Parsers/parserItHappensRu.h \
    GetSite/mmtComGet.h \
    Parsers/parserMmtCom.h \
    Functional/deleteLocalPagesManager.h \
    GetSite/tjBashOrgGet.h \
    Parsers/parserTjBashOrg.h \
    GetSite/shortikiComGet.h \
    Parsers/parserShortikiCom.h \
    GetSite/ibashDeGet.h \
    Parsers/parserIbashDe.h \
    GetSite/gBashOrgGet.h \
    Parsers/parserGBashOrg.h \
    GetSite/danstonchatComGet.h \
    Parsers/parserDanstonchatCom.h \
    GetSite/laBananeOrgGet.h \
    Parsers/parserLaBananeOrg.h \
    Functional/updateManager.h \
    UI/donateWindow.h \
    UI/totorialWidget.h \
    Functional/fileDownloader.h \
    UI/write_to_us.h \
    Parsers/AbstractParser.h \
    Parsers/QuoteStruc.h

SOURCES += \
    main.cpp \
    Parsers/libxml2reader.cpp \
    UI/quoteView.cpp \
    mainWidget.cpp \
    GetSite/bashOrgGet.cpp \
    Functional/loadManager.cpp \
    Functional/saveManager.cpp \
    UI/nameToSaveWindow.cpp \
    Social/vkAuth.cpp \
    GetSite/bashImGet.cpp \
    Parsers/parserBashIm.cpp \
    Parsers/parserBashOrg.cpp \
    Social/autosaver.cpp \
    Social/cookiejar.cpp \
    Social/fbAuth.cpp \
    GetSite/iBashOrgGet.cpp \
    Parsers/parserIBashOrg.cpp \
    GetSite/qdbUsGet.cpp \
    Parsers/parserQdbUs.cpp \
    GetSite/itHappensRuGet.cpp \
    Parsers/parserItHappensRu.cpp \
    GetSite/mmtComGet.cpp \
    Parsers/parserMmtCom.cpp \
    Functional/deleteLocalPagesManager.cpp \
    GetSite/tjBashOrgGet.cpp \
    Parsers/parserTjBashOrg.cpp \
    GetSite/shortikiComGet.cpp \
    Parsers/parserShortikiCom.cpp \
    GetSite/ibashDeGet.cpp \
    Parsers/parserIbashDe.cpp \
    GetSite/gBashOrgGet.cpp \
    Parsers/parserGBashOrg.cpp \
    GetSite/danstonchatComGet.cpp \
    Parsers/parserDanstonchatCom.cpp \
    GetSite/laBananeOrgGet.cpp \
    Parsers/parserLaBananeOrg.cpp \
    Functional/updateManager.cpp \
    UI/donateWindow.cpp \
    UI/totorialWidget.cpp \
    Functional/fileDownloader.cpp \
    UI/write_to_us.cpp \
    Parsers/AbstractParseractparser.cpp \
    Parsers/QuoteStruc.cpp

FORMS += \
    UI/quoteView.ui \
    mainWidget.ui \
    UI/nameToSaveWindow.ui \
#    UI/gorizontalMenuBar.ui \
#    UI/verticalMenuBar.ui \
    UI/donateWindow.ui \
    UI/totorialWidget.ui \
    UI/write_to_us.ui

RESOURCES += \
    resources.qrc


TARGET = bin/lolclub

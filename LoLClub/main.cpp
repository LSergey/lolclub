#include <QtWidgets>
#include <QSplashScreen>
#include "mainWidget.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName(app.translate("main", "LoL Club"));
    app.setOrganizationName("Triangle labs");
    app.setOrganizationDomain("http://trianglelabs.org/");


   QSplashScreen *splash = new QSplashScreen;
   splash->setPixmap(QPixmap(":/images/res/Lol_Club_Ss.png"));
   splash->show();

   MainWidget w;

   QTimer::singleShot(2000, splash, SLOT(close()));
   QTimer::singleShot(2000, &w, SLOT(show()));
//    w.show();
   delete splash;
    return app.exec();
}

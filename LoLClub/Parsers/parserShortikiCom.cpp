#include "parserShortikiCom.h"

ParserShortikiCom::ParserShortikiCom(AbstractParser *parent) :
    AbstractParser(parent)
{
}

ParserShortikiCom::~ParserShortikiCom()
{
}

void ParserShortikiCom::parse(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
        emit connectionFail();
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        QString stringData(data);

        int firstQuote = stringData.indexOf("<div class=\"id\">");
        QString resultData = stringData.mid(firstQuote-1);
        resultData.replace("<br>", "\n");
        resultData.replace("<br />", "\n");

        int index = resultData.indexOf("<div class=\"share\">");
        while (index != -1)
        {
            resultData.insert(index + 1, "o");
            index = resultData.indexOf("<div class=\"share\">");
        }
        src.setData(resultData);

        doc.setContent(&src, &reader);

        quotes.text.clear();
        quotes.number.clear();
        quotes.raiting.clear();

        if (reply->url().toString().contains(QString("http://shortiki.com/top.php")))
            parseTopDomDocument(&doc);
        else if (reply->url().toString().contains(QString("http://shortiki.com/rating.php")))
            parseByraitingDomDocument(&doc);
        else
            parsePageDomDocument(&doc);

        emit(parseResult(quotes.text, quotes.number, quotes.raiting));
    }
}

void ParserShortikiCom::parsePageDomDocument(QDomDocument *doc)
{
    int sectionSize = 59;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 0, j = 0; j < 28; i += 6, j++)
    {
        quotes.number[j] = nodeList.at(i).toElement().text().mid(1);
        quotes.raiting[j] = nodeList.at(i+1).toElement().text();
        quotes.text[j] = nodeList.at(i+4).toElement().text();
    }

    for (int i = 169, j = 28; j < sectionSize; i += 6, j++)
    {
        quotes.number[j] = nodeList.at(i).toElement().text().mid(1);
        quotes.raiting[j] = nodeList.at(i+1).toElement().text();
        quotes.text[j] = nodeList.at(i+4).toElement().text();
    }
}

void ParserShortikiCom::parseTopDomDocument(QDomDocument *doc)
{
    int sectionSize = 30;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 0, j = 0; j < 50; i += 5, j++)
    {
        quotes.number[j] = nodeList.at(i).toElement().text().mid(1);
        quotes.raiting[j] = nodeList.at(i+1).toElement().text();
        quotes.text[j] = nodeList.at(i+3).toElement().text();
    }

    for (int i = 51, j = 10; j < 20; i += 5, j++)
    {
        quotes.number[j] = nodeList.at(i).toElement().text().mid(1);
        quotes.raiting[j] = nodeList.at(i+1).toElement().text();
        quotes.text[j] = nodeList.at(i+3).toElement().text();
    }

    for (int i = 102, j = 20; j < 30; i += 5, j++)
    {
        quotes.number[j] = nodeList.at(i).toElement().text().mid(1);
        quotes.raiting[j] = nodeList.at(i+1).toElement().text();
        quotes.text[j] = nodeList.at(i+3).toElement().text();
    }
}

void ParserShortikiCom::parseByraitingDomDocument(QDomDocument *doc)
{
    int sectionSize = 59;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 0, j = 0; j < 28; i += 5, j++)
    {
        quotes.number[j] = nodeList.at(i).toElement().text().mid(1);
        quotes.raiting[j] = nodeList.at(i+1).toElement().text();
        quotes.text[j] = nodeList.at(i+3).toElement().text();
    }

    for (int i = 141, j = 28; j < sectionSize; i += 5, j++)
    {
        quotes.number[j] = nodeList.at(i).toElement().text().mid(1);
        quotes.raiting[j] = nodeList.at(i+1).toElement().text();
        quotes.text[j] = nodeList.at(i+3).toElement().text();
    }
}

void ParserShortikiCom::pageNumParser(QNetworkReply *reply)
{
    QByteArray data = reply->readAll();

    QString stringData(data);

    QString toCutStr = "<span class=\"cleaner\"></span><div class=\"pagination\">";
    int firstQuote = stringData.indexOf("<span class=\"cleaner\"></span><div class=\"pagination\">");
    QString resultData = stringData.mid(firstQuote + toCutStr.size());
    int startNum = resultData.indexOf(">") + 1;
    int endNum = resultData.indexOf("</a>");
    QString num = resultData.mid(startNum, endNum - startNum);

    emit(pagesNumParseResult(num.toInt()));
}

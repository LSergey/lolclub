#ifndef PARSERQDBUS_H
#define PARSERQDBUS_H

#include "AbstractParser.h"

class ParserQdbUs : public AbstractParser
{
    Q_OBJECT
public:
    explicit ParserQdbUs(AbstractParser *parent = 0);
    ~ParserQdbUs();

public slots:
    void parse(QNetworkReply*);

private:
    void parsePageDomDocument(QDomDocument*);

};

#endif // PARSERQDBUS_H

#ifndef PARSERDANSTONCHATCOM_H
#define PARSERDANSTONCHATCOM_H

#include "AbstractParser.h"

class ParserDanstonchatCom : public AbstractParser
{
    Q_OBJECT
public:
    explicit ParserDanstonchatCom(AbstractParser *parent = 0);
    ~ParserDanstonchatCom();

public slots:
    void parse(QNetworkReply*);

private:
    void parsePageDomDocument(QDomDocument*);
    void parseTopDomDocument(QDomDocument *);
    void parseRandomDomDocument(QDomDocument *);

    
};

#endif // PARSERDANSTONCHATCOM_H

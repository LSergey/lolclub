#ifndef PARSERIBASHORG_H
#define PARSERIBASHORG_H

#include "AbstractParser.h"

class ParserIBashOrg : public AbstractParser
{
    Q_OBJECT
public:
    explicit ParserIBashOrg(AbstractParser *parent = 0);
    ~ParserIBashOrg();

public slots:
    void parse(QNetworkReply*);

private:
    void parsePageDomDocument(QDomDocument*);
    void parseRandomDomDocument(QDomDocument*);
    
};

#endif // PARSERIBASHORG_H

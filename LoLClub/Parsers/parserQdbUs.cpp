#include "parserQdbUs.h"

ParserQdbUs::ParserQdbUs(AbstractParser *parent) :
    AbstractParser(parent)
{
}

ParserQdbUs::~ParserQdbUs()
{
}

void ParserQdbUs::parse(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
        emit connectionFail();
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        QString stringData(data);

        int firstQuote = stringData.indexOf("<tr><td class=\"q\"");
        QString resultData = stringData.mid(firstQuote-1);
        resultData.replace("<br>", "\n");
        resultData.replace("<br />", "\n");

        src.setData(resultData);

        doc.setContent(&src, &reader);

        quotes.text.clear();
        quotes.number.clear();
        quotes.raiting.clear();

        parsePageDomDocument(&doc);

        emit parseResult(quotes.text, quotes.number, quotes.raiting);
    }
}

void ParserQdbUs::parsePageDomDocument(QDomDocument *doc)
{
    int sectionSize = 25;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("td");

    for (int i = 0, j = 0; j < sectionSize; i++)
    {
        QString allQuote = nodeList.at(i).toElement().text();
        if( allQuote.indexOf("#") == 0)
        {
            QString numAndRait = allQuote.mid(0, allQuote.indexOf("\n"));
            QString quote = allQuote.mid(allQuote.indexOf("\n")+1);
            QString quoteResult;
            QStringList list = quote.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);
            for (int i = 0; i < list.size(); i++)
                quoteResult += list.at(i) + "\n";

            int quoteNumberStartIndex = numAndRait.indexOf("#") + 1;
            int quoteNumberEndIndex = numAndRait.indexOf("(") - 1;
            int quoteRaitStartIndex = numAndRait.indexOf("(") + 1;
            int quoteRaitEndIndex = numAndRait.indexOf(")") - 1;

            quotes.number[j] = numAndRait.mid(quoteNumberStartIndex, quoteNumberEndIndex - quoteNumberStartIndex);
            quotes.raiting[j] = numAndRait.mid(quoteRaitStartIndex, quoteRaitEndIndex - quoteRaitStartIndex);
            quotes.text[j] = quoteResult.remove("-+");
            j++;
        }
    }
}

#include "parserTjBashOrg.h"

ParserTjBashOrg::ParserTjBashOrg(AbstractParser *parent) :
    AbstractParser(parent)
{
}

ParserTjBashOrg::~ParserTjBashOrg()
{
}

void ParserTjBashOrg::parse(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
        emit connectionFail();
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        QString stringData(data);

        int firstQuote = stringData.indexOf("<ul class=\"quote-list\">");
        QString resultData = stringData.mid(firstQuote-1);
        resultData.replace("<br>", "\n");
        resultData.replace("<br />", "\n");

        src.setData(resultData);

        doc.setContent(&src, &reader);

        quotes.text.clear();
        quotes.number.clear();
        quotes.raiting.clear();

        parsePageDomDocument(&doc);

        emit parseResult(quotes.text, quotes.number, quotes.raiting);
    }
}

void ParserTjBashOrg::parsePageDomDocument(QDomDocument *doc)
{
    int sectionSize = 25;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeListRait = docElem.elementsByTagName("span");
    QDomNodeList nodeListQuote = docElem.elementsByTagName("blockquote");

    for (int i = 0, j = 0; j < sectionSize; i += 6, j++)
    {
        QString spaces = "					";
        quotes.text[j] = nodeListQuote.at(j).toElement().text().mid(spaces.size() + 1);
        quotes.number[j] = nodeListRait.at(i+1).toElement().text().mid(1);
        quotes.raiting[j] = nodeListRait.at(i+2).toElement().text() + nodeListRait.at(i+3).toElement().text();
    }
}

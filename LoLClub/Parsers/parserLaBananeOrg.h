#ifndef PARSERLABANANEORG_H
#define PARSERLABANANEORG_H

#include "AbstractParser.h"

class ParserLaBananeOrg : public AbstractParser
{
    Q_OBJECT
public:
    explicit ParserLaBananeOrg(AbstractParser *parent = 0);
    ~ParserLaBananeOrg();

public slots:
    void parse(QNetworkReply*);

private:
    void parsePageDomDocument(QDomDocument*);
};

#endif // PARSERLABANANEORG_H

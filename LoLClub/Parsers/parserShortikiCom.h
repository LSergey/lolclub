#ifndef PARSERSHORTIKICOM_H
#define PARSERSHORTIKICOM_H

#include "AbstractParser.h"

class ParserShortikiCom : public AbstractParser
{
    Q_OBJECT
public:
    explicit ParserShortikiCom(AbstractParser *parent = 0);
    ~ParserShortikiCom();

public slots:
    void parse(QNetworkReply*);
    void pageNumParser(QNetworkReply*);

signals:
    void pagesNumParseResult(int);

private:
    void parsePageDomDocument(QDomDocument*);
    void parseTopDomDocument(QDomDocument*);
    void parseByraitingDomDocument(QDomDocument*);

};

#endif // PARSERSHORTIKICOM_H

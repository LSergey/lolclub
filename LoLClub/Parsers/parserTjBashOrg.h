#ifndef PARSERTJBASHORG_H
#define PARSERTJBASHORG_H

#include "AbstractParser.h"

class ParserTjBashOrg : public AbstractParser
{
    Q_OBJECT
public:
    explicit ParserTjBashOrg(AbstractParser *parent = 0);
    ~ParserTjBashOrg();

public slots:
    void parse(QNetworkReply*);

private:
    void parsePageDomDocument(QDomDocument*);

};

#endif // PARSERTJBASHORG_H

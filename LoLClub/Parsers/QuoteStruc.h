#ifndef QUOTESTRUC_H
#define QUOTESTRUC_H

#include <QObject>
#include <QString>
#include <QVector>

class QuoteStruct : public QObject
{
    Q_OBJECT
public:
    explicit QuoteStruct(QObject *parent = 0);
    ~QuoteStruct();
    QVector<QString> text, number, raiting;
    int size;
};

#endif // QUOTESTRUC_H

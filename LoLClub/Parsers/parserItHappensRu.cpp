#include "parserItHappensRu.h"

ParserItHappensRu::ParserItHappensRu(QObject *parent) :
    QObject(parent)
{
}

void ParserItHappensRu::parseItHappensRuhtml(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
        emit connectionFail();
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        QTextCodec *codec = QTextCodec::codecForName("CP1251");
        QTextCodec *codecUnicode = QTextCodec::codecForName("Utf-8");

        QString stringData = codec->toUnicode(data);
        stringData.replace("windows-1251", "utf-8");

        int firstQuote = stringData.indexOf("<div class=\"text\">");
        QString resultData = stringData.mid(firstQuote-1);
        resultData.replace("<br>", "\n");
        resultData.replace("<br />", "\n");

        int index = resultData.indexOf("<p class=\"storytags\">");
        while (index != -1)
        {
            resultData.insert(index - 1, "</div>");
            resultData.insert(resultData.indexOf("<p class=\"storytags\">") + 2, "lololo");
            index = resultData.indexOf("<p class=\"storytags\">");
        }
//        qDebug() << resultData;
        src.setData(codecUnicode->fromUnicode(resultData));

        doc.setContent(&src, &reader);

        text.clear();
        number.clear();
        raiting.clear();

        if (reply->url().toString().contains(QString("http://ithappens.ru/story")))
            parseRandomDomDocument(&doc);
        else if (reply->url().toString().contains(QString("http://ithappens.ru/top")))
            parseBestDomDocument(&doc);
        else
            parsePageDomDocument(&doc);

        qDebug() << "emit";
        emit(itHappensRuParseResult(text, number, raiting));
    }
}

void ParserItHappensRu::parsePageDomDocument(QDomDocument *doc)
{
    int sectionSize = 9;
    text.resize(sectionSize);
    number.resize(sectionSize);
    raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 0, j = 0; i < 25; i += 3, j++)
    {
        qDebug() << i;
        QString allQuote = nodeList.at(i).toElement().text();
        QStringList lines = allQuote.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);
        number[j] = lines[0].mid(5, lines[0].indexOf(":") - 5);
        raiting[j] = lines[1].mid(lines[1].lastIndexOf(": ") + 1);

        text[j] = allQuote.mid(allQuote.indexOf(lines[1]) + lines[1].size());
        text[j].insert(0, lines[0].mid(lines[0].indexOf(":") + 1) + "\n \n");
    }
}

void ParserItHappensRu::parseBestDomDocument(QDomDocument *doc)
{
    int sectionSize = 25;
    text.resize(sectionSize);
    number.resize(sectionSize);
    raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 0, j = 0; i < 49; i += 2, j++)
    {

        QString allQuote = nodeList.at(i+1).toElement().text();
        QStringList lines = allQuote.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);
        number[j] = lines[0].mid(11, lines[0].lastIndexOf(":") - 11);
        raiting[j] = lines[1].mid(lines[1].indexOf(": ") + 1);
        qDebug() << lines[0];

        text[j] = allQuote.mid(allQuote.indexOf(lines[1]) + lines[1].size());
        text[j].insert(0, lines[0].mid(lines[0].indexOf(":") + 1) + "\n \n");
    }
}

void ParserItHappensRu::parseRandomDomDocument(QDomDocument *doc)
{
    int sectionSize = 1;
    text.resize(sectionSize);
    number.resize(sectionSize);
    raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    QString allQuote = nodeList.at(0).toElement().text();
    QStringList lines = allQuote.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);
    number[0] = lines[0].mid(5, lines[0].indexOf(":") - 5);
    raiting[0] = lines[1].mid(lines[1].indexOf(": ") + 1);

    text[0] = allQuote.mid(allQuote.indexOf(lines[1]) + lines[1].size());
    text[0].insert(0, lines[0].mid(lines[0].indexOf(":") + 1) + "\n \n");
}

void ParserItHappensRu::pageNumParser(QNetworkReply *reply)
{
        QByteArray data = reply->readAll();

        QTextCodec *codec = QTextCodec::codecForName("CP1251");

        QString stringData = codec->toUnicode(data);
        stringData.replace("windows-1251", "utf-8");

        QString toCutStr = "<div class=\"selector\"><span class=\"cur\"><b>";
        int firstQuote = stringData.indexOf("<div class=\"selector\"><span class=\"cur\"><b>");
        QString resultData = stringData.mid(firstQuote + toCutStr.size());
        QString num = resultData.mid(0, resultData.indexOf("</b>"));

        qDebug() << num.toInt();

        emit(pagesNumParseResult(num.toInt()));
}

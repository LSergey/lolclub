#ifndef PARSERITHAPPENSRU_H
#define PARSERITHAPPENSRU_H

#include <QObject>
#include <QString>
#include <QNetworkAccessManager>
#include <QUrl>
//#include <QDebug>
#include <QNetworkReply>
#include <QtXml/QtXml>
#include <QDomDocument>
#include <QXmlInputSource>
#include <QTextCodec>
#include <QVector>

#include <libxml2reader.h>

class ParserItHappensRu : public QObject
{
    Q_OBJECT
public:
    explicit ParserItHappensRu(QObject *parent = 0);
    QVector<QString> text, number, raiting;
    
signals:
    void itHappensRuParseResult(QVector<QString>& text,QVector<QString>& number, QVector<QString>& raiting);
    void pagesNumParseResult(int);
    void connectionFail();

public slots:
    void parseItHappensRuhtml(QNetworkReply*);
    void pageNumParser(QNetworkReply*);

private:
    void parsePageDomDocument(QDomDocument*);
    void parseBestDomDocument(QDomDocument*);
    void parseRandomDomDocument(QDomDocument*);
    
};

#endif // PARSERITHAPPENSRU_H

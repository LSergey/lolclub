#include "parserIbashDe.h"

ParserIbashDe::ParserIbashDe(AbstractParser *parent) :
    AbstractParser(parent)
{
}

ParserIbashDe::~ParserIbashDe()
{
}

void ParserIbashDe::parse(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
        emit connectionFail();
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        QString stringData(data);

        int firstQuote = stringData.indexOf("<table class=\"quotetable\"");
        QString resultData = stringData.mid(firstQuote-1);
        resultData = resultData.replace("</tr>", "\n");

        int index = resultData.indexOf(");return false;");
        while (index != -1)
        {
            resultData = resultData.remove(index, 31);
            index = resultData.indexOf(");return false;");
        }

        src.setData(resultData);

        doc.setContent(&src, &reader);

        quotes.text.clear();
        quotes.number.clear();
        quotes.raiting.clear();

        if (reply->url().toString().contains(QString("http://ibash.de/top_")))
            parseTopDomDocument(&doc);
        else
            parsePageDomDocument(&doc);

        emit parseResult(quotes.text, quotes.number, quotes.raiting);
    }
}

void ParserIbashDe::parsePageDomDocument(QDomDocument *doc)
{
    int sectionSize = 20;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("table");

    for (int i = 6, j = 0; j < sectionSize; i += 3, j++)
    {
        if ( i == 30)
        {
            i -= 2;
            j -= 1;
            continue;
        }
        QString numAndRait = nodeList.at(i).toElement().text();
        quotes.number[j] = numAndRait.mid(1, numAndRait.indexOf("|") - 5);
        quotes.raiting[j] = numAndRait.mid(numAndRait.indexOf("[") + 1, numAndRait.indexOf("]") - numAndRait.indexOf("[") - 1);
        quotes.text[j] = nodeList.at(i+2).toElement().text();
        quotes.text[j].remove(quotes.text[j].size() - 3, 2);
    }
}

void ParserIbashDe::parseTopDomDocument(QDomDocument *doc)
{
    int sectionSize = 20;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("table");

    for (int i = 7, j = 0; j < sectionSize; i += 3, j++)
    {
        if ( i == 31)
        {
            i -= 2;
            j -= 1;
            continue;
        }
        QString numAndRait = nodeList.at(i).toElement().text();
        quotes.number[j] = numAndRait.mid(1, numAndRait.indexOf("|") - 5);
        quotes.raiting[j] = numAndRait.mid(numAndRait.indexOf("[") + 1, numAndRait.indexOf("]") - numAndRait.indexOf("[") - 1);
        quotes.text[j] = nodeList.at(i+2).toElement().text();
        quotes.text[j].remove(quotes.text[j].size() - 3, 2);
    }
}

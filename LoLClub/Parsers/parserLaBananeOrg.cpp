#include "parserLaBananeOrg.h"

ParserLaBananeOrg::ParserLaBananeOrg(AbstractParser *parent) :
    AbstractParser(parent)
{
}

ParserLaBananeOrg::~ParserLaBananeOrg()
{
}

void ParserLaBananeOrg::parse(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
        emit connectionFail();
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        QString stringData(data);

        int firstQuote = stringData.indexOf("<div class=\"item1\">");

        QString resultData = stringData.mid(firstQuote-1);
        resultData = resultData.replace("<br>", "\n");

        src.setData(resultData);

        doc.setContent(&src, &reader);

        quotes.text.clear();
        quotes.number.clear();
        quotes.raiting.clear();

        parsePageDomDocument(&doc);

        emit parseResult(quotes.text, quotes.number, quotes.raiting);
    }
}

void ParserLaBananeOrg::parsePageDomDocument(QDomDocument *doc)
{
    int sectionSize = 30;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 0, j = 0; j < sectionSize; i++, j++)
    {
        QString allQuote = nodeList.at(i).toElement().text();
        QString numAndRait = allQuote.mid(0, allQuote.indexOf(")")+1);
        quotes.number[j] = numAndRait.mid(0, numAndRait.indexOf("."));
        quotes.raiting[j] = numAndRait.mid(numAndRait.indexOf("+") + 2,
                                    numAndRait.lastIndexOf("-") - numAndRait.indexOf("+") -3);
        quotes.text[j] = allQuote.mid(allQuote.indexOf(")")+1);
    }
}

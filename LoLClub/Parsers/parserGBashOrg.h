﻿#ifndef PARSERGBASHORG_H
#define PARSERGBASHORG_H

#include "AbstractParser.h"

class ParserGBashOrg : public AbstractParser
{
    Q_OBJECT
public:
    explicit ParserGBashOrg(AbstractParser *parent = 0);
    ~ParserGBashOrg();
    
public slots:
    void parse(QNetworkReply*);

private:
    void parsePageDomDocument(QDomDocument*);
    void parseTopDomDocument(QDomDocument *);
    void parseBroseDomDocument(QDomDocument *);
    void parseRandomDomDocument(QDomDocument *);

    
};

#endif // PARSERGBASHORG_H

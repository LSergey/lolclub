#include "parserDanstonchatCom.h"

ParserDanstonchatCom::ParserDanstonchatCom(AbstractParser *parent) :
    AbstractParser(parent)
{
}

ParserDanstonchatCom::~ParserDanstonchatCom()
{
}

void ParserDanstonchatCom::parse(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
        emit connectionFail();
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        QString stringData(data);

        QString replyStr = reply->url().toString();
        QString resultData;
        if (replyStr.contains(QString("http://danstonchat.com/top50.html")) ||
                replyStr.contains(QString("http://danstonchat.com/random0.html")))
        {
            int firstQuote = stringData.indexOf("<div class=\"item-listing\">");
            resultData = stringData.mid(firstQuote-1);
        }
        else
        {
            int firstQuote = stringData.indexOf("<div class=\"hidden\">");
            resultData = stringData.mid(firstQuote-1);
        }

        resultData = resultData.replace("<br>", "\n");
        resultData = resultData.replace("<br />", "\n");

        src.setData(resultData);

        doc.setContent(&src, &reader);

        quotes.text.clear();
        quotes.number.clear();
        quotes.raiting.clear();

        if (reply->url().toString().contains(QString("http://danstonchat.com/top50.html")))
            parseTopDomDocument(&doc);
        else if (reply->url().toString().contains(QString("http://danstonchat.com/random0.html")))
            parseRandomDomDocument(&doc);
        else
            parsePageDomDocument(&doc);

        emit parseResult(quotes.text, quotes.number, quotes.raiting);
    }
}


void ParserDanstonchatCom::parsePageDomDocument(QDomDocument *doc)
{
    int sectionSize = 25;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 1, j = 0; j < sectionSize; i++, j++)
    {
        QString allQuote = nodeList.at(i).toElement().text().trimmed();
        QString numAndRait = allQuote.mid(allQuote.lastIndexOf("#"));
        quotes.text[j] = allQuote.mid(0, allQuote.lastIndexOf("#"));
        quotes.number[j] = numAndRait.mid(1, numAndRait.indexOf("-") - 2);
        quotes.raiting[j] = numAndRait.mid(numAndRait.indexOf(")") + 2,
                                    numAndRait.lastIndexOf("(") - numAndRait.indexOf(")") - 3);
    }
}

void ParserDanstonchatCom::parseTopDomDocument(QDomDocument *doc)
{
    int sectionSize = 50;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 1, j = 0; j < sectionSize; i++, j++)
    {
        QString allQuote = nodeList.at(i).toElement().text().trimmed();
        QString numAndRait = allQuote.mid(allQuote.lastIndexOf("#"));
        quotes.text[j] = allQuote.mid(0, allQuote.lastIndexOf("#"));
        quotes.number[j] = numAndRait.mid(1, numAndRait.indexOf("-") - 2);
        quotes.raiting[j] = numAndRait.mid(numAndRait.indexOf(")") + 2,
                                    numAndRait.lastIndexOf("(") - numAndRait.indexOf(")") - 3);
    }
}

void ParserDanstonchatCom::parseRandomDomDocument(QDomDocument *doc)
{
    int sectionSize = 25;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 1, j = 0; j < sectionSize; i++, j++)
    {
        QString allQuote = nodeList.at(i).toElement().text().trimmed();
        QString numAndRait = allQuote.mid(allQuote.lastIndexOf("#"));
        quotes.text[j] = allQuote.mid(0, allQuote.lastIndexOf("#"));
        quotes.number[j] = numAndRait.mid(1, numAndRait.indexOf("-") - 2);
        quotes.raiting[j] = numAndRait.mid(numAndRait.indexOf(")") + 2,
                                    numAndRait.lastIndexOf("(") - numAndRait.indexOf(")") - 3);
    }
}

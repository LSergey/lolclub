#include "parserGBashOrg.h"

ParserGBashOrg::ParserGBashOrg(AbstractParser *parent) :
    AbstractParser(parent)
{
}

ParserGBashOrg::~ParserGBashOrg()
{
}

void ParserGBashOrg::parse(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
        emit connectionFail();
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        QString stringData(data);

        int firstQuote = stringData.indexOf("<!-- memcached set quote -->");
        QString resultData = stringData.mid(firstQuote-1);
        resultData = resultData.replace("</span>", "\n");

        src.setData(resultData);

        doc.setContent(&src, &reader);

        quotes.text.clear();
        quotes.number.clear();
        quotes.raiting.clear();

        if (reply->url().toString().contains(QString("http://german-bash.org/action/browse")))
            parseBroseDomDocument(&doc);
        else if (reply->url().toString().contains(QString("http://german-bash.org/action/random")))
            parseRandomDomDocument(&doc);
        else if (reply->url().toString().contains(QString("http://german-bash.org/action/top/")))
            parseTopDomDocument(&doc);
        else
            parsePageDomDocument(&doc);

        emit parseResult(quotes.text, quotes.number, quotes.raiting);
    }
}

void ParserGBashOrg::parsePageDomDocument(QDomDocument *doc)
{
    int sectionSize = 50;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 0, j = 0; j < sectionSize; i += 4, j++)
    {
        quotes.text[j] = nodeList.at(i+3).toElement().text().trimmed();
        quotes.text[j].replace("\n\n", "\n");
        QString numAndRait = nodeList.at(i+2).toElement().text().trimmed();
        numAndRait.replace(" ", "");
        numAndRait.replace("    ", "");
        numAndRait.replace("	", "");
        numAndRait.replace("\n", "");
        numAndRait.replace("		", "");
        quotes.number[j] = numAndRait.mid(1, numAndRait.indexOf("|") - 1);
        quotes.raiting[j] = numAndRait.mid(numAndRait.indexOf("[")+1, numAndRait.indexOf("]")-numAndRait.indexOf("[") -1);

        if (0 == i || 20 == i || 44 == i || 68 == i)
            i += 4;
    }
}

void ParserGBashOrg::parseTopDomDocument(QDomDocument *doc)
{
    int sectionSize = 50;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 9, j = 0; j < sectionSize; i += 5, j++)
    {
        quotes.text[j] = nodeList.at(i+4).toElement().text().trimmed();
        quotes.text[j].replace("\n\n", "\n");
        QString numAndRait = nodeList.at(i+3).toElement().text().trimmed();
        numAndRait.replace(" ", "");
        numAndRait.replace("    ", "");
        numAndRait.replace("	", "");
        numAndRait.replace("\n", "");
        numAndRait.replace("		", "");
        quotes.number[j] = numAndRait.mid(1, numAndRait.indexOf("|") - 1);
        quotes.raiting[j] = numAndRait.mid(numAndRait.indexOf("[")+1, numAndRait.indexOf("]")-numAndRait.indexOf("[") -1);

        if (9 == i || 33 == i || 62 == i || 91 == i)
            i += 4;
    }
}

void ParserGBashOrg::parseRandomDomDocument(QDomDocument *doc)
{
    int sectionSize = 5;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 9, j = 0; j < sectionSize; i += 4, j++)
    {
        quotes.text[j] = nodeList.at(i+3).toElement().text().trimmed();
        quotes.text[j].replace("\n\n", "\n");
        QString numAndRait = nodeList.at(i+2).toElement().text().trimmed();
        numAndRait.replace(" ", "");
        numAndRait.replace("    ", "");
        numAndRait.replace("	", "");
        numAndRait.replace("\n", "");
        numAndRait.replace("		", "");
        quotes.number[j] = numAndRait.mid(1, numAndRait.indexOf("|") - 1);
        quotes.raiting[j] = numAndRait.mid(numAndRait.indexOf("[")+1, numAndRait.indexOf("]")-numAndRait.indexOf("[") -1);

        if (9 == i /*|| 20 == i || 44 == i || 68 == i*/)
            i += 4;
    }
}

void ParserGBashOrg::parseBroseDomDocument(QDomDocument *doc)
{
    int sectionSize = 20;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 9, j = 0; j < sectionSize; i += 4, j++)
    {
        quotes.text[j] = nodeList.at(i+3).toElement().text().trimmed();
        quotes.text[j].replace("\n\n", "\n");
        QString numAndRait = nodeList.at(i+2).toElement().text().trimmed();
        numAndRait.replace(" ", "");
        numAndRait.replace("    ", "");
        numAndRait.replace("	", "");
        numAndRait.replace("\n", "");
        numAndRait.replace("		", "");
        quotes.number[j] = numAndRait.mid(1, numAndRait.indexOf("|") - 1);
        quotes.raiting[j] = numAndRait.mid(numAndRait.indexOf("[")+1, numAndRait.indexOf("]")-numAndRait.indexOf("[") -1);

        if (9 == i || 29 == i || 53 == i || 77 == i)
            i += 4;
    }
}

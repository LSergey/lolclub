#ifndef ABSTRACTPARSER_H
#define ABSTRACTPARSER_H

#include <QObject>
#include <QString>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QWebView>
#include <QNetworkReply>
#include <QtXml/qdom.h>
#include <QtXml/QtXml>
#include <QDomDocument>
#include <QXmlInputSource>
#include <QTextCodec>
#include <cstring>
#include <QVector>
#include <QXmlStreamReader>
#include <QVector>

#include <libxml2reader.h>
#include "QuoteStruc.h"

class AbstractParser : public QObject
{
    Q_OBJECT
public:
    explicit AbstractParser(QObject *parent = 0);
    virtual ~AbstractParser();
    virtual void parse(QNetworkReply*) = 0;

    QuoteStruct quotes;
    QString url;
    
signals:
    void parseResult(QVector<QString>&, QVector<QString>&, QVector<QString>&);
    void connectionFail();
    void domDocument(QDomDocument&);
    
public slots:

    
};

#endif // ABSTRACTPARSER_H

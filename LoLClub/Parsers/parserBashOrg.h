#ifndef PARSER_H
#define PARSER_H

#include "AbstractParser.h"

class ParserBashOrg : public AbstractParser
{
    Q_OBJECT
public:
    explicit ParserBashOrg(AbstractParser *parent = 0);
    ~ParserBashOrg();

signals:
    void bashOrgDomDocument(QDomDocument&);

public slots:
    void parse(QNetworkReply*);

private:
    void parseBashOrgDomDocument(QDomDocument*);
};

#endif // PARSER_H

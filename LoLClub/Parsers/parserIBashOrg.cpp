#include "parserIBashOrg.h"

ParserIBashOrg::ParserIBashOrg(AbstractParser *parent) :
    AbstractParser(parent)
{
}

ParserIBashOrg::~ParserIBashOrg()
{
}

void ParserIBashOrg::parse(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
        emit connectionFail();
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        QTextCodec *codec = QTextCodec::codecForName("CP1251");
        QTextCodec *codecUnicode = QTextCodec::codecForName("Utf-8");

        QString stringData = codec->toUnicode(data);
        stringData.replace("windows-1251", "utf-8");

        int firstQuote = stringData.indexOf("<div class=\"quote\">");
        QString resultData = stringData.mid(firstQuote-1);
        resultData.replace("<br>", "\n");
        resultData.replace("<br />", "\n");

        src.setData(codecUnicode->fromUnicode(resultData));

        doc.setContent(&src, &reader);

        quotes.text.clear();
        quotes.number.clear();
        quotes.raiting.clear();

        if (reply->url().toString().contains(QString("http://ibash.org.ru/random")))
            parseRandomDomDocument(&doc);
        else
            parsePageDomDocument(&doc);

        emit(parseResult(quotes.text, quotes.number, quotes.raiting));
    }
}

void ParserIBashOrg::parsePageDomDocument(QDomDocument *doc)
{
    int sectionSize = 50;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 0, j = 0; j < sectionSize; i += 3, j++)
    {
        QString numAndRait = nodeList.at(i+1).toElement().text();

        numAndRait.remove(" ");
        numAndRait.remove("\n");
        numAndRait.remove("br /");
        numAndRait.remove("	");

        int quoteNumberStart = numAndRait.indexOf("#") + 1;
        int quoteNumberEnd = numAndRait.indexOf("+");
        QString num = numAndRait.mid(quoteNumberStart, quoteNumberEnd - quoteNumberStart);
        quotes.number[j] = num;

        int quoteRaitStart = numAndRait.indexOf("(") + 1;
        int quoteRaitEnd = numAndRait.indexOf(")");
        QString rait = numAndRait.mid(quoteRaitStart, quoteRaitEnd - quoteRaitStart);
        quotes.raiting[j] = rait;

        quotes.text[j] = nodeList.at(i+2).toElement().text();
    }
}

void ParserIBashOrg::parseRandomDomDocument(QDomDocument *doc)
{
    int sectionSize = 1;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    QString numAndRait = nodeList.at(1).toElement().text();

    numAndRait.remove(" ");
    numAndRait.remove("\n");
    numAndRait.remove("br /");
    numAndRait.remove("	");

    int quoteNumberStart = numAndRait.indexOf("#") + 1;
    int quoteNumberEnd = numAndRait.indexOf("+");
    QString num = numAndRait.mid(quoteNumberStart, quoteNumberEnd - quoteNumberStart);
    quotes.number[0] = num;

    int quoteRaitStart = numAndRait.indexOf("(") + 1;
    int quoteRaitEnd = numAndRait.indexOf(")");
    QString rait = numAndRait.mid(quoteRaitStart, quoteRaitEnd - quoteRaitStart);
    quotes.raiting[0] = rait;

    quotes.text[0] = nodeList.at(2).toElement().text();
}

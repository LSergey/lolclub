#ifndef PARSERMMTCOM_H
#define PARSERMMTCOM_H

#include "AbstractParser.h"

class ParserMMTCom : public AbstractParser
{
    Q_OBJECT
public:
    explicit ParserMMTCom(AbstractParser *parent = 0);
    ~ParserMMTCom();

public slots:
    void parse(QNetworkReply*);

private:
    void parsePageDomDocument(QDomDocument*);
    void parseBestDomDocument(QDomDocument*);
    void parseRandomDomDocument(QDomDocument*);
    
};

#endif // PARSERMMTCOM_H

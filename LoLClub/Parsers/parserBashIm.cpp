#include "parserBashIm.h"

ParserBashIm::ParserBashIm(AbstractParser *parent) :
    AbstractParser(parent)
{
}

ParserBashIm::~ParserBashIm()
{
}

void ParserBashIm::parse(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
    {
        emit connectionFail();
    }
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        QTextCodec *codec = QTextCodec::codecForName("CP1251");
        QTextCodec *codecUnicode = QTextCodec::codecForName("Utf-8");

        QString stringData = codec->toUnicode(data);
        stringData.replace("windows-1251", "utf-8");

        int firstQuote = stringData.indexOf("div class=\"quote\"");
        QString resultData = stringData.mid(firstQuote-1);
        resultData.replace("<br>", "\n");

        src.setData(codecUnicode->fromUnicode(resultData));
        doc.setContent(&src, &reader);

        quotes.text.clear();
        quotes.number.clear();
        quotes.raiting.clear();

        if(reply->url().toString() == "http://bash.im/" || reply->url().toString().contains("http://bash.im/index/"))
            parseNewDomDocument(&doc);
        else if(reply->url().toString().contains("http://bash.im/best"))
            parseBestDomDocument(&doc);
        else if(reply->url().toString().contains("http://bash.im/random"))
            parseRandomDomDocument(&doc);
        else if(reply->url().toString().contains("http://bash.im/byrating"))
            parseByRaitingDomDocument(&doc);
        else if(reply->url().toString() == "http://bash.im/abyss")
            parseAbyssDomDocument(&doc);
        else if(reply->url().toString().contains("http://bash.im/abysstop"))
            parseAbyssTopDomDocument(&doc);
        else if(reply->url().toString().contains("http://bash.im/abyssbest"))
        {
            int quotesSize = resultData.count("div class=\"quote\"");
            parseAbyssbestDomDocument(&doc, /*std::max(*/quotesSize - 3/*, 0)*/);
        }

        emit(parseResult(quotes.text, quotes.number, quotes.raiting));
    }
}

void ParserBashIm::parseNewDomDocument(QDomDocument *doc)
{
    int sectionSize = 50;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");
    int nodeListSize = nodeList.count();

    for(int ii = 2, j=0; j <= 4 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("Цитата#");
        int endNumberPlace = numberAndRaiting.indexOf("'});");
        QString num = numberAndRaiting.mid(startNumberPlace + 7, endNumberPlace - startNumberPlace - 7);
        quotes.number[j] = num;
    }

    for(int ii = 19, j = 5; j <= 9 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("Цитата#");
        int endNumberPlace = numberAndRaiting.indexOf("'});");
        QString num = numberAndRaiting.mid(startNumberPlace + 7, endNumberPlace - startNumberPlace - 7);
        quotes.number[j] = num;
    }

    for(int ii = 35, j = 10; j < sectionSize && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif
        int startNumberPlace = numberAndRaiting.indexOf("Цитата#");
        int endNumberPlace = numberAndRaiting.indexOf("'});");
        QString num = numberAndRaiting.mid(startNumberPlace + 7, endNumberPlace - startNumberPlace - 7);
        quotes.number[j] = num;
    }
}

void ParserBashIm::parseRandomDomDocument(QDomDocument *doc)
{
    int sectionSize = 50;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");
    int nodeListSize = nodeList.count();

    for(int ii = 2, j=0; j <= 4 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("Цитата#");
        int endNumberPlace = numberAndRaiting.indexOf("'});");
        QString num = numberAndRaiting.mid(startNumberPlace + 7, endNumberPlace - startNumberPlace - 7);
        quotes.number[j] = num;
    }

    for(int ii = 19, j = 5; j <= 9 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("Цитата#");
        int endNumberPlace = numberAndRaiting.indexOf("'});");
        QString num = numberAndRaiting.mid(startNumberPlace + 7, endNumberPlace - startNumberPlace - 7);
        quotes.number[j] = num;
    }

    for(int ii = 35, j = 10; j < sectionSize && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("Цитата#");
        int endNumberPlace = numberAndRaiting.indexOf("'});");
        QString num = numberAndRaiting.mid(startNumberPlace + 7, endNumberPlace - startNumberPlace - 7);
        quotes.number[j] = num;
    }
}

void ParserBashIm::parseBestDomDocument(QDomDocument *doc)
{
    int sectionSize = 30;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");
    int nodeListSize = nodeList.count();

    for(int ii = 2, j=0; j < 5 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("Цитата#");
        int endNumberPlace = numberAndRaiting.indexOf("'});");
        QString num = numberAndRaiting.mid(startNumberPlace + 7, endNumberPlace - startNumberPlace - 7);
        quotes.number[j] = num;
    }

    for(int ii = 19, j = 5; j < 30 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("Цитата#");
        int endNumberPlace = numberAndRaiting.indexOf("'});");
        QString num = numberAndRaiting.mid(startNumberPlace + 7, endNumberPlace - startNumberPlace - 7);
        quotes.number[j] = num;
    }
}

void ParserBashIm::parseByRaitingDomDocument(QDomDocument *doc)
{
    int sectionSize = 50;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");
    int nodeListSize = nodeList.count();

    for(int ii = 2, j=0; j <= 4 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("Цитата#");
        int endNumberPlace = numberAndRaiting.indexOf("'});");
        QString num = numberAndRaiting.mid(startNumberPlace + 7, endNumberPlace - startNumberPlace - 7);
        quotes.number[j] = num;
    }

    for(int ii = 19, j = 5; j <= 9 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("Цитата#");
        int endNumberPlace = numberAndRaiting.indexOf("'});");
        QString num = numberAndRaiting.mid(startNumberPlace + 7, endNumberPlace - startNumberPlace - 7);
        quotes.number[j] = num;
    }

    for(int ii = 35, j = 10; j < sectionSize && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("Цитата#");
        int endNumberPlace = numberAndRaiting.indexOf("'});");
        QString num = numberAndRaiting.mid(startNumberPlace + 7, endNumberPlace - startNumberPlace - 7);
        quotes.number[j] = num;
    }
}

void ParserBashIm::parseAbyssDomDocument(QDomDocument *doc)
{
    int sectionSize = 25;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");
    int nodeListSize = nodeList.count();

    for(int ii = 2, j=0; j <= 4 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("#");
        QString num = numberAndRaiting.mid(startNumberPlace + 1);
        quotes.number[j] = num;
    }

    for(int ii = 19, j = 5; j <= 9 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("#");
        QString num = numberAndRaiting.mid(startNumberPlace + 1);
        quotes.number[j] = num;
    }

    for(int ii = 35, j = 10; j < sectionSize && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");
#ifdef WIN32
        {
            int minusPlace = numberAndRaiting.indexOf("[");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 4);
            raiting[j] = rait;
        }
#else
        {
            int minusPlace = numberAndRaiting.indexOf("–");
            int plusPlace = numberAndRaiting.indexOf("+");
            QString rait = numberAndRaiting.mid(plusPlace + 1, minusPlace - plusPlace - 1);
            quotes.raiting[j] = rait;
        }
#endif

        int startNumberPlace = numberAndRaiting.indexOf("#");
        QString num = numberAndRaiting.mid(startNumberPlace + 1);
        quotes.number[j] = num;
    }
}

void ParserBashIm::parseAbyssTopDomDocument(QDomDocument *doc)
{
    int sectionSize = 25;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");
    int nodeListSize = nodeList.count();

    for(int ii = 2, j=0; j <= 4 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();
        quotes.raiting[j] = "";
        quotes.number[j] = "Top #" + QString(QString::number(j+1));
    }

    for(int ii = 19, j = 5; j <= 9 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();
        quotes.raiting[j] = "";
        quotes.number[j] = "Top #" + QString(QString::number(j+1));
    }

    for(int ii = 35, j = 10; j < sectionSize && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();
        quotes.raiting[j] = "";
        quotes.number[j] = "Top #" + QString(QString::number(j+1));
    }
}

void ParserBashIm::parseAbyssbestDomDocument(QDomDocument *doc, int sectionSize)
{
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");
    int nodeListSize = nodeList.count();

    for(int ii = 9, j=0; j <= 4 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");

        quotes.raiting[j] = "";

        int startNumberPlace = numberAndRaiting.lastIndexOf("#");
        QString num = numberAndRaiting.mid(startNumberPlace + 1);
        quotes.number[j] = num;
    }

    for(int ii = 26, j = 5; j <= 9 && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");

        quotes.raiting[j] = "";

        int startNumberPlace = numberAndRaiting.lastIndexOf("#");
        QString num = numberAndRaiting.mid(startNumberPlace + 1);
        quotes.number[j] = num;
    }

    for(int ii = 42, j = 10; j < sectionSize && ii < nodeListSize; ii+=3, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();

        quotes.text[j] = el.text();

        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        numberAndRaiting.remove(" ");
        numberAndRaiting.remove("\n");
        numberAndRaiting.remove("br /");
        numberAndRaiting.remove("	");

        quotes.raiting[j] = "";

        int startNumberPlace = numberAndRaiting.lastIndexOf("#");
        QString num = numberAndRaiting.mid(startNumberPlace + 1);
        quotes.number[j] = num;
    }
}

#include "parserMmtCom.h"

ParserMMTCom::ParserMMTCom(AbstractParser *parent) :
    AbstractParser(parent)
{
}

ParserMMTCom::~ParserMMTCom()
{
}

void ParserMMTCom::parse(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
        emit connectionFail();
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        QString stringData(data);

        int firstQuote = stringData.indexOf("<div class=\"post\">");
        QString resultData = stringData.mid(firstQuote-1);
        resultData.replace("<br>", "\n");
        resultData.replace("<br />", "\n");

        int index = resultData.indexOf("<div class=\"date\" style=\"float:left;\">");
        while (index != -1)
        {
            resultData.insert(index - 1, "</div>");
            resultData.insert(resultData.indexOf("<div class=\"date\" style=\"float:left;\">") + 5, "lololo");
            index = resultData.indexOf("<div class=\"date\" style=\"float:left;\">");
        }

        src.setData(resultData);

        doc.setContent(&src, &reader);

        quotes.text.clear();
        quotes.number.clear();
        quotes.raiting.clear();

        if (reply->url().toString().contains(QString("http://makesmethink.com/top")))
            parseBestDomDocument(&doc);
        else if (reply->url().toString().contains(QString("http://makesmethink.com/random")))
            parseRandomDomDocument(&doc);
        else
            parsePageDomDocument(&doc);

        emit parseResult(quotes.text, quotes.number, quotes.raiting);
    }
}

void ParserMMTCom::parsePageDomDocument(QDomDocument *doc)
{
    int sectionSize = 20;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 0, j = 0; i < 77; i += 4, j++)
    {
        QString a = "            	";
        quotes.text[j] = nodeList.at(i).toElement().text().mid(a.size() * 2
                                                        /*, nodeList.at(i).toElement().text().lastIndexOf("MMT") + 4*/);
        QString numAndRait = nodeList.at(i+1).toElement().text();
        QStringList lines = numAndRait.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);
        quotes.number[j] = lines[0].mid(a.size() + 1, lines[0].indexOf("(") - 1 - a.size());
        quotes.raiting[j] = lines[3].mid(lines[3].lastIndexOf("(") + 1,
                lines[3].lastIndexOf(")") - lines[3].lastIndexOf("(") - 1);
        QString secondRaitPart = lines[6].mid(lines[6].lastIndexOf("(") + 1,
                lines[6].lastIndexOf(")") - lines[6].lastIndexOf("(") - 1);
        quotes.raiting[j] += "/" + secondRaitPart;
    }
}

void ParserMMTCom::parseBestDomDocument(QDomDocument *doc)
{
    int sectionSize = 50;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 4, j = 0; i < 201; i += 4, j++)
    {
        QString a = "            	";
        quotes.text[j] = nodeList.at(i).toElement().text().mid(a.size() * 2
                                                        /*, nodeList.at(i).toElement().text().lastIndexOf("MMT") + 4*/);
        QString numAndRait = nodeList.at(i+1).toElement().text();
        QStringList lines = numAndRait.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);
        quotes.number[j] = lines[0].mid(a.size() + 1, lines[0].indexOf("(") - 1 - a.size());
        quotes.raiting[j] = lines[3].mid(lines[3].lastIndexOf("(") + 1,
                lines[3].lastIndexOf(")") - lines[3].lastIndexOf("(") - 1);
        QString secondRaitPart = lines[6].mid(lines[6].lastIndexOf("(") + 1,
                lines[6].lastIndexOf(")") - lines[6].lastIndexOf("(") - 1);
        quotes.raiting[j] += "/" + secondRaitPart;
    }
}

void ParserMMTCom::parseRandomDomDocument(QDomDocument *doc)
{
    int sectionSize = 20;
    quotes.text.resize(sectionSize);
    quotes.number.resize(sectionSize);
    quotes.raiting.resize(sectionSize);

    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("div");

    for (int i = 4, j = 0; i < 81; i += 4, j++)
    {
        QString a = "            	";
        quotes.text[j] = nodeList.at(i).toElement().text().mid(a.size() * 2
                                                        /*, nodeList.at(i).toElement().text().lastIndexOf("MMT") + 4*/);
        QString numAndRait = nodeList.at(i+1).toElement().text();
        QStringList lines = numAndRait.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);
        quotes.number[j] = lines[0].mid(a.size() + 1, lines[0].indexOf("(") - 1 - a.size());
        quotes.raiting[j] = lines[3].mid(lines[3].lastIndexOf("(") + 1,
                lines[3].lastIndexOf(")") - lines[3].lastIndexOf("(") - 1);
        QString secondRaitPart = lines[6].mid(lines[6].lastIndexOf("(") + 1,
                lines[6].lastIndexOf(")") - lines[6].lastIndexOf("(") - 1);
        quotes.raiting[j] += "/" + secondRaitPart;
    }
}

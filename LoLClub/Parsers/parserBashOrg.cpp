#include "parserBashOrg.h"

ParserBashOrg::ParserBashOrg(AbstractParser *parent) :
    AbstractParser(parent)
{
}

ParserBashOrg::~ParserBashOrg()
{

}

void ParserBashOrg::parse(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
        emit connectionFail();
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        src.setData(data);
        doc.setContent(&src, &reader);

        //signal to saveManager for temporary save
        emit bashOrgDomDocument(doc);

        quotes.text.clear();
        quotes.number.clear();
        quotes.raiting.clear();

        parseBashOrgDomDocument(&doc);

        emit(parseResult(quotes.text, quotes.number, quotes.raiting));
    }
}

void ParserBashOrg::parseBashOrgDomDocument(QDomDocument* doc)
{
    QDomElement docElem = doc->documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("p");

    int size = nodeList.count() / 2;
    quotes.text.resize(size);
    quotes.number.resize(size);
    quotes.raiting.resize(size);

    for(int ii = 2, j=0;ii < nodeList.count(); ii+=2, j++)
    {
        QDomElement el = nodeList.at(ii).toElement();
        quotes.text[j] = el.text();
        el = nodeList.at(ii-1).toElement();
        QString numberAndRaiting = el.text();
        uint i = 1;
        while(numberAndRaiting[i].isNumber())
        {
            quotes.number[j] += numberAndRaiting[i];
            i++;
        }
        i += 3;
        while(numberAndRaiting[i].isNumber())
        {
            quotes.raiting[j] += numberAndRaiting[i];
            i++;
        }
    }
}

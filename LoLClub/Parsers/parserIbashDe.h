#ifndef PARSERIBASHDE_H
#define PARSERIBASHDE_H

#include "AbstractParser.h"

class ParserIbashDe : public AbstractParser
{
    Q_OBJECT
public:
    explicit ParserIbashDe(AbstractParser *parent = 0);
    ~ParserIbashDe();
    
public slots:
    void parse(QNetworkReply*);

private:
    void parsePageDomDocument(QDomDocument*);
    void parseTopDomDocument(QDomDocument *);
    
};

#endif // PARSERIBASHDE_H

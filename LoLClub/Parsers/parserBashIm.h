#ifndef PARSERBASHIM_H
#define PARSERBASHIM_H

#include "AbstractParser.h"

class ParserBashIm : public AbstractParser
{
    Q_OBJECT
public:
    explicit ParserBashIm(AbstractParser *parent = 0);
    ~ParserBashIm();

public slots:
    void parse(QNetworkReply*);

private:
    void parseNewDomDocument(QDomDocument*);
    void parseRandomDomDocument(QDomDocument*);
    void parseBestDomDocument(QDomDocument*);
    void parseByRaitingDomDocument(QDomDocument*);
    void parseAbyssDomDocument(QDomDocument*);
    void parseAbyssbestDomDocument(QDomDocument*, int);
    void parseAbyssTopDomDocument(QDomDocument*);
};

#endif // PARSERBASHIM_H

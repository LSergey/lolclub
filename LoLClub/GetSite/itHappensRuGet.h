#ifndef ITHAPPENSRUGET_H
#define ITHAPPENSRUGET_H

#include <QObject>
#include <QDomDocument>
#include <QNetworkAccessManager>

#include "parserItHappensRu.h"
#include "state.h"

class ItHappensRuGet : public QObject
{
    Q_OBJECT
public:
    explicit ItHappensRuGet(QObject *parent = 0);
    ~ItHappensRuGet();

    ParserItHappensRu *parser;
    
signals:
    void parseResult(QVector<QString>& text,QVector<QString>& number,
                     QVector<QString>& raiting, SITE site, SECTIONS section);
    void loading();
    void closeLoading();
    void connectionFail();

private slots:
    void getNewPage();
    void getRandomPage();
    void getBestPage();

    void getNextPage();
    void getPreviosePage();

    void setPageNumbers(int);
    void pageParsed(QVector<QString>& text,QVector<QString>& number, QVector<QString>& raiting);

    void noConnection();

private:
    QNetworkAccessManager *networkManager, *pageNumManager;
    SECTIONS section;
    int currentPageNumber;
    int totalPageNumber;
};

#endif // ITHAPPENSRUGET_H

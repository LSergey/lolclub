#include "qdbUsGet.h"

QdbUsGet::QdbUsGet(QObject *parent) :
    QObject(parent)
{
    parser = new ParserQdbUs();
    networkManager = new QNetworkAccessManager(this);

    latestPageNumber = 1;
    bestPageNumber = 1;
    topPageNumber = 1;

    connect(parser, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&)),
            this, SLOT(pageParsed(QVector<QString>& , QVector<QString>& , QVector<QString>& )));
    connect(parser, SIGNAL(connectionFail()), this, SLOT(noConnection()));
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(parse(QNetworkReply*)));
}

QdbUsGet::~QdbUsGet()
{
    delete parser;
    delete networkManager;
}

void QdbUsGet::getTodayPage()
{
    emit loading();
    QString url = "http://qdb.us/today";
    section = QDB_US_TODAY;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void QdbUsGet::getLatestPage()
{
    emit loading();
    QString url = "http://qdb.us/";
    section = QDB_US_LATEST;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void QdbUsGet::getBestPage()
{
    emit loading();
    QString url = "http://qdb.us/best";
    section = QDB_US_BEST;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void QdbUsGet::getTopPage()
{
    emit loading();
    QString url = "http://qdb.us/top";
    section = QDB_US_TOP;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void QdbUsGet::getRandomPage()
{
    emit loading();
    QString url = "http://qdb.us/random";
    section = QDB_US_TOP;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void QdbUsGet::getNextPage(SECTIONS section)
{
    if (section == QDB_US_TODAY)
    {
        getTodayPage();
    } else if (section == QDB_US_LATEST)
    {
        emit loading();
        latestPageNumber += 1;
        QString url = "http://qdb.us/latest/" + QString::number(latestPageNumber);

        networkManager->get(QNetworkRequest(QUrl(url)));
    } else if (section == QDB_US_BEST)
    {
        emit loading();
        bestPageNumber += 1;
        QString url = "http://qdb.us/best/" + QString::number(bestPageNumber);

        networkManager->get(QNetworkRequest(QUrl(url)));
    } else if (section == QDB_US_TOP)
    {
        emit loading();
        topPageNumber += 1;
        QString url = "http://qdb.us/top/" + QString::number(topPageNumber);

        networkManager->get(QNetworkRequest(QUrl(url)));
    } else if (section == QDB_US_RANDOM)
    {
        getRandomPage();
    }
}

void QdbUsGet::getPreviosePage(SECTIONS section)
{
    if (section == QDB_US_TODAY)
    {
        getTodayPage();
    } else if (section == QDB_US_LATEST)
    {
        if (latestPageNumber > 1)
        {
            emit loading();
            latestPageNumber -= 1;
            QString url = "http://qdb.us/latest/" + QString::number(latestPageNumber);

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    } else if (section == QDB_US_BEST)
    {
        if (bestPageNumber > 1)
        {
            emit loading();
            bestPageNumber += 1;
            QString url = "http://qdb.us/best/" + QString::number(bestPageNumber);

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    } else if (section == QDB_US_TOP)
    {
        if (topPageNumber > 1)
        {
            emit loading();
            topPageNumber += 1;
            QString url = "http://qdb.us/top/" + QString::number(topPageNumber);

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    } else if (section == QDB_US_RANDOM)
    {
        getRandomPage();
    }
}

void QdbUsGet::pageParsed(QVector<QString> &text, QVector<QString> &number, QVector<QString> &raiting)
{
    emit(parseResult(text, number, raiting, QDB_US, section));
    emit closeLoading();
}

void QdbUsGet::noConnection()
{
    emit connectionFail();
}

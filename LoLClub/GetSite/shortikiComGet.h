#ifndef SHORTIKICOMGET_H
#define SHORTIKICOMGET_H

#include <QObject>
#include <QDomDocument>
#include <QNetworkAccessManager>

#include "parserShortikiCom.h"
#include "state.h"

class ShortikiComGet : public QObject
{
    Q_OBJECT
public:
    explicit ShortikiComGet(QObject *parent = 0);
    ~ShortikiComGet();

    ParserShortikiCom *parser;

signals:
    void parseResult(QVector<QString>& text,QVector<QString>& number,
                     QVector<QString>& raiting, SITE site, SECTIONS section);
    void loading();
    void closeLoading();
    void connectionFail();

private slots:
    void getNewPage();
    void getRandomPage();
    void getByRaitingPage();
    void getTopPage();

    void getNextPage();
    void getPreviosePage();

    void setPageNumbers(int);
    void pageParsed(QVector<QString>& text,QVector<QString>& number, QVector<QString>& raiting);

    void noConnection();

private:
    QNetworkAccessManager *networkManager, *pageNumManager;
    SECTIONS section;
    int currentPageNumber;
    int raitPageNumber;
    int totalPageNumber;
};

#endif // SHORTIKICOMGET_H

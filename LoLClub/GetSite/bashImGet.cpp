#include "bashImGet.h"

BashImGet::BashImGet(QObject *parent) :
    QObject(parent)
{
    parser = new ParserBashIm();
    networkManager = new QNetworkAccessManager(this);
    newPagesManager = new QNetworkAccessManager(this);
    abyssPagesManager = new QNetworkAccessManager(this);

    currentRaitingPage = 1;

    connect(parser, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&)), this,
            SLOT(pageParsed(QVector<QString>& , QVector<QString>& , QVector<QString>& )));
    connect(parser, SIGNAL(connectionFail()), this, SLOT(noConnection()));
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(parse(QNetworkReply*)));
    connect(newPagesManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(parseNumOfPage(QNetworkReply*)));
    connect(abyssPagesManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(parseNumOfAbyssPage(QNetworkReply*)));
}

BashImGet::~BashImGet()
{
    delete parser;
    delete networkManager;
    delete newPagesManager;
    delete abyssPagesManager;
}

void BashImGet::getBashImPage()
{
    emit loading();
    QString url = "http://bash.im/";
    section = BASH_IM_NEW;

    networkManager->get(QNetworkRequest(QUrl(url)));
    newPagesManager->get(QNetworkRequest(QUrl(url)));
}

void BashImGet::getRandomPage()
{
    emit loading();
    QString url = "http://bash.im/random/";
    section = BASH_IM_RANDOM;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void BashImGet::getBestPage()
{
    emit loading();
    QString url = "http://bash.im/best";
    section = BASH_IM_BEST;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void BashImGet::getByRaitingPage()
{
    emit loading();
    QString url = "http://bash.im/byrating";
    section = BASH_IM_BYRAITING;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void BashImGet::getAbyssPage()
{
    emit loading();
    QString url = "http://bash.im/abyss";
    section = BASH_IM_ABYSS;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void BashImGet::getAbyssBestPage()
{
    emit loading();
    QString url = "http://bash.im/abyssbest";
    section = BASH_IM_ABYSSBEST;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void BashImGet::getAbyssTopPage()
{
    emit loading();
    QString url = "http://bash.im/abysstop";
    section = BASH_IM_ABYSSTOP;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void BashImGet::nextPage(SECTIONS section)
{
    switch (section) {
    case BASH_IM_NEW:       getBashImNextPage();    break;
    case BASH_IM_RANDOM:    getRandomNextPage();    break;
    case BASH_IM_BYRAITING: getByRaitingNextPage(); break;
    case BASH_IM_ABYSS:     getAbyssNextPage();     break;
    case BASH_IM_ABYSSBEST: getAbyssBestNextPage(); break;
    case BASH_IM_ABYSSTOP:  getAbyssTopNextPage();  break;
    default: break;
    }
}

void BashImGet::previousPage(SECTIONS section)
{
    switch (section) {
    case BASH_IM_NEW:       getBashImPreviousPage();    break;
    case BASH_IM_RANDOM:    getRandomPreviousPage();    break;
    case BASH_IM_BYRAITING: getByRaitingPreviousPage(); break;
    case BASH_IM_ABYSS:     getAbyssPreviousPage();     break;
    case BASH_IM_ABYSSBEST: getAbyssBestPreviousPage(); break;
    case BASH_IM_ABYSSTOP:  getAbyssTopPreviousPage();  break;
    default: break;
    }
}

void BashImGet::pageParsed(QVector<QString> &text, QVector<QString> &number,
                           QVector<QString> &raiting)
{
    emit(parseResult(text, number, raiting, BASH_IM, section));
    emit closeLoading();
    if(section != BASH_IM_NEW
            && section != BASH_IM_BYRAITING && section != BASH_IM_ABYSSBEST)
        emit pageNums(BASH_IM, section, 1, 1);
}

void BashImGet::getBashImNextPage()
{
    if(currentPageNumber < numOfPages)
    {
        emit loading();
        currentPageNumber += 1;
        QString url = "http://bash.im/index/" + QString::number(currentPageNumber);

        networkManager->get(QNetworkRequest(QUrl(url)));
    }
}

void BashImGet::getRandomNextPage()
{
    getRandomPage();
}

void BashImGet::getBestNextPage()
{
    getBestPage();
}

void BashImGet::getByRaitingNextPage()
{
    if(currentRaitingPage != numOfPages)
    {
        emit loading();
        currentRaitingPage += 1;
        QString url = "http://bash.im/byrating/" + QString::number(currentRaitingPage);

        networkManager->get(QNetworkRequest(QUrl(url)));
    }
}

void BashImGet::getAbyssNextPage()
{
    getAbyssPage();
}

void BashImGet::getAbyssBestNextPage()
{
    if(currentAbyssbestNumber < abyssbestNumOfPages)
    {
        emit loading();
        QString currentNumStr = QString::number(currentAbyssbestNumber);
        QString yearStr = currentNumStr.mid(0, 4);
        QString monthStr = currentNumStr.mid(4, 2);
        QString dateStr = currentNumStr.mid(6, 2);

        if(dateStr.toInt() < 28)
            currentAbyssbestNumber += 1;
        else if(dateStr.toInt() == 31 && monthStr.toInt() == 12)
            currentAbyssbestNumber = QString(QString::number(yearStr.toInt() + 1) + "01" + "01").toInt();
        else if(dateStr.toInt() == 28)
        {
            if(monthStr.toInt() == 2 && (yearStr.toInt() % 4 == 0))
                currentAbyssbestNumber += 1;
            else
                currentAbyssbestNumber = QString(yearStr + QString::number(monthStr.toInt() + 1) + "01").toInt();
        }
        else if(dateStr.toInt() == 29)
        {
            if(monthStr.toInt() == 2)
                currentAbyssbestNumber = QString(yearStr + QString::number(monthStr.toInt() + 1) + "01").toInt();
            else
                currentAbyssbestNumber += 1;
        }
        else if(dateStr.toInt() == 30)
        {
            if( monthStr.toInt() == 4 || monthStr.toInt() == 6 || monthStr.toInt() == 9 || monthStr.toInt() == 11)
                currentAbyssbestNumber = QString(yearStr + QString::number(monthStr.toInt() + 1) + "01").toInt();
            else
                currentAbyssbestNumber += 1;
        }
        else //date == 31 -> next month
            currentAbyssbestNumber = QString(yearStr + QString::number(monthStr.toInt() + 1) + "01").toInt();

        QString url = "http://bash.im/abyssbest/" + QString::number(currentAbyssbestNumber);

        networkManager->get(QNetworkRequest(QUrl(url)));
    }
}

void BashImGet::getAbyssTopNextPage()
{
    getAbyssTopPage();
}

void BashImGet::getBashImPreviousPage()
{
    if(currentPageNumber != 1)
    {
        emit loading();
        currentPageNumber -= 1;
        QString url = "http://bash.im/index/" + QString::number(currentPageNumber);

        networkManager->get(QNetworkRequest(QUrl(url)));
    }
}

void BashImGet::getRandomPreviousPage()
{
    getRandomPage();
}

void BashImGet::getBestPreviousPage()
{
    getBestPage();
}

void BashImGet::getByRaitingPreviousPage()
{
    if(currentRaitingPage != 1)
    {
        emit loading();
        currentRaitingPage -= 1;
        QString url = "http://bash.im/index/" + QString::number(currentRaitingPage);

        networkManager->get(QNetworkRequest(QUrl(url)));
    }
}

void BashImGet::getAbyssPreviousPage()
{
    getAbyssPage();
}

void BashImGet::getAbyssBestPreviousPage()
{
    QString year = QString::number(abyssbestNumOfPages).mid(0, 4);
    QString yearAgo = QString(QString::number(year.toInt()-1)
                              + QString::number(abyssbestNumOfPages).right(4));

    if(currentAbyssbestNumber > yearAgo.toInt())
    {
        emit loading();
        QString currentNumStr = QString::number(currentAbyssbestNumber);
        QString yearStr = currentNumStr.mid(0, 4);
        QString monthStr = currentNumStr.mid(4, 2);
        QString dateStr = currentNumStr.mid(6, 2);

        if(dateStr.toInt() != 1)
            currentAbyssbestNumber -= 1;
        else if(dateStr.toInt() == 1 && monthStr.toInt() == 1)
            currentAbyssbestNumber = QString(QString::number(yearStr.toInt() - 1) + "12" + "31").toInt();
        else if(monthStr.toInt() == 5 || monthStr.toInt() == 7 || monthStr.toInt() == 10 || monthStr.toInt() == 12)
        {
            QString monthString = QString::number(monthStr.toInt() - 1);
            if (monthString.size() == 1)
                monthString = "0" + monthString;
            currentAbyssbestNumber = QString(yearStr + monthString + "30").toInt();
        }
        else
        {
            QString monthString = QString::number(monthStr.toInt() - 1);
            if (monthString.size() == 1)
                monthString = "0" + monthString;
            currentAbyssbestNumber = QString(yearStr + monthString + "31").toInt();
        }

        QString url = "http://bash.im/abyssbest/" + QString::number(currentAbyssbestNumber);

        networkManager->get(QNetworkRequest(QUrl(url)));
    }
}

void BashImGet::getAbyssTopPreviousPage()
{
    getAbyssTopPage();
}

void BashImGet::getNumOfPages()
{
    QString url = "http://bash.im";

    newPagesManager->get(QNetworkRequest(QUrl(url)));
}

void BashImGet::parseNumOfPage(QNetworkReply* reply)
{
    if(reply->error() != QNetworkReply::NoError)
    {
        emit connectionFail();
    }
    else
    {
        QByteArray data = reply->readAll();

        QTextCodec *codec = QTextCodec::codecForName("CP1251");

        QString stringData = codec->toUnicode(data);
        stringData.replace("windows-1251", "utf-8");

        int serchedNumberIsFirst = stringData.indexOf("numeric=\"integer\" min=\"1\" max=\"");
        QString resultData = stringData.mid(serchedNumberIsFirst + 31, 10);
        int numberEndIndex = resultData.indexOf("\"");
        QString numOfPagesStr = resultData.left(numberEndIndex);
        numOfPages = numOfPagesStr.toInt() - 6;
        currentPageNumber = numOfPages;

        if(section == BASH_IM_NEW )
            emit pageNums(BASH_IM, section, numOfPages, currentPageNumber);
        else if(section == BASH_IM_BYRAITING)
            emit pageNums(BASH_IM, section, numOfPages, currentRaitingPage);
    }
}

void BashImGet::getNumOfAbyssbestPages()
{
    QString url = "http://bash.im/abyssbest";

    abyssPagesManager->get(QNetworkRequest(QUrl(url)));
}

void BashImGet::parseNumOfAbyssPage(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
    {
        emit connectionFail();
    }
    else
    {
        QByteArray data = reply->readAll();

        QTextCodec *codec = QTextCodec::codecForName("CP1251");

        QString stringData = codec->toUnicode(data);
        stringData.replace("windows-1251", "utf-8");
        int serchedNumberIsFirst = stringData.indexOf("data-date=\"");
        QString resultData = stringData.mid(serchedNumberIsFirst + 11, 10);
        int numberEndIndex = resultData.indexOf("\"");
        QString numOfPagesStr = resultData.left(numberEndIndex);
        abyssbestNumOfPages = numOfPagesStr.toInt();
        currentAbyssbestNumber = abyssbestNumOfPages;

        emit pageNums(BASH_IM,section, abyssbestNumOfPages, currentAbyssbestNumber);
    }
}

void BashImGet::noConnection()
{
    emit connectionFail();
}

#ifndef MMTCOMGET_H
#define MMTCOMGET_H

#include <QObject>
#include <QDomDocument>
#include <QNetworkAccessManager>

#include "parserMmtCom.h"
#include "state.h"

class MMTComGet : public QObject
{
    Q_OBJECT
public:
    explicit MMTComGet(QObject *parent = 0);
    ~MMTComGet();

    ParserMMTCom *parser;

signals:
    void parseResult(QVector<QString>& text,QVector<QString>& number,
                     QVector<QString>& raiting, SITE site, SECTIONS section);
    void loading();
    void closeLoading();
    void connectionFail();

private slots:
    void getNewPage();
    void getTopPage();
    void getRandomPage();

    void getNextPage(SECTIONS);
    void getPreviosePage(SECTIONS);

    void pageParsed(QVector<QString>& text,QVector<QString>& number, QVector<QString>& raiting);

    void noConnection();

private:
    QNetworkAccessManager *networkManager;
    SECTIONS section;
    int currentPageNumber;
};

#endif // MMTCOMGET_H

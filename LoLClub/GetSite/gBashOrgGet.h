#ifndef GBASHORGGET_H
#define GBASHORGGET_H

#include <QObject>
#include <QDomDocument>
#include <QNetworkAccessManager>

#include "parserGBashOrg.h"
#include "state.h"

class GBashOrgGet : public QObject
{
    Q_OBJECT
public:
    explicit GBashOrgGet(QObject *parent = 0);
    ~GBashOrgGet();

    ParserGBashOrg *parser;

signals:
    void parseResult(QVector<QString>& text,QVector<QString>& number,
                     QVector<QString>& raiting, SITE site, SECTIONS section);
    void loading();
    void closeLoading();
    void connectionFail();

private slots:
    void getLatestPage();
    void getBrowsePage();
    void getTopPage();
    void getRandomPage();

    void getNextPage(SECTIONS);
    void getPreviosePage(SECTIONS);

    void pageParsed(QVector<QString>& text,QVector<QString>& number, QVector<QString>& raiting);

    void noConnection();

private:
    QNetworkAccessManager *networkManager;
    SECTIONS section;
    int pageNumber;
};

#endif // GBASHORGGET_H

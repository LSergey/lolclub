#include "bashOrgGet.h"

BashOrgGet::BashOrgGet(QObject *parent) :
    QObject(parent)
{
    parser = new ParserBashOrg();
    networkManager = new QNetworkAccessManager(this);
    browsePageManager = new QNetworkAccessManager(this);

    browsePageNumber = 419;
    currentBrowsePage = 419;

    connect(parser, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&)), this,
            SLOT(pageParsed(QVector<QString>& , QVector<QString>& , QVector<QString>& )));
    connect(parser, SIGNAL(bashOrgDomDocument(QDomDocument&)), this, SLOT(getDomDocument(QDomDocument&)));
    connect(parser, SIGNAL(connectionFail()), this, SLOT(noConnection()));
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(parse(QNetworkReply*)));
    connect(browsePageManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(parseBrowsePage(QNetworkReply*)));
}

BashOrgGet::~BashOrgGet()
{
    delete parser;
    delete networkManager;
    delete browsePageManager;
}

void BashOrgGet::getLatestPage()
{
    emit loading();
    QString url = "http://www.bash.org/?latest";
    section = BASH_ORG_LATEST;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void BashOrgGet::getBrowsePage()
{
    emit loading();
    QString url = "http://www.bash.org/?browse=" + QString::number(browsePageNumber);
    section = BASH_ORG_BROWSE;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void BashOrgGet::getRandomPage()
{
    emit loading();
    QString url = "http://www.bash.org/?random1";
    section = BASH_ORG_RANDOM;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void BashOrgGet::getTopp100_200Page()
{
    emit loading();
    QString url = "http://www.bash.org/?top";
    section = BASH_ORG_TOP100_200;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void BashOrgGet::nextPage(SECTIONS bashSection)
{
    switch (bashSection)
    {
    case BASH_ORG_LATEST:       getLatestNextPage();      break;
    case BASH_ORG_BROWSE:       getBrowseNextPage();      break;
    case BASH_ORG_RANDOM:       getRandomNextPage();      break;
    case BASH_ORG_TOP100_200:   getTopp100_200NextPage(); break;
    default: break;
    }
}

void BashOrgGet::previousePage(SECTIONS bashSection)
{
    switch (bashSection)
    {
    case BASH_ORG_LATEST:     getLatestPreviousePage();      break;
    case BASH_ORG_BROWSE:     getBrowsePreviousePage();      break;
    case BASH_ORG_RANDOM:     getRandomPreviousePage();      break;
    case BASH_ORG_TOP100_200: getTopp100_200PreviousePage(); break;
    default: break;
    }
}

void BashOrgGet::getLatestNextPage()
{
    section = BASH_ORG_BROWSE;
    getBrowsePage();
}

void BashOrgGet::getBrowseNextPage()
{
    if(currentBrowsePage < browsePageNumber)
    {
        emit loading();
        currentBrowsePage += 1;
        QString url = "http://www.bash.org/?browse=" + QString::number(currentBrowsePage);

        networkManager->get(QNetworkRequest(QUrl(url)));
    }
}

void BashOrgGet::getRandomNextPage()
{
    getRandomPage();
}

void BashOrgGet::getTopp100_200NextPage()
{
    getTopp100_200Page();
}

void BashOrgGet::getLatestPreviousePage()
{
    section = BASH_ORG_BROWSE;
    getBrowsePage();
}

void BashOrgGet::getBrowsePreviousePage()
{
    if (currentBrowsePage > 1)
    {
        emit loading();
        currentBrowsePage -= 1;
        QString url = "http://www.bash.org/?browse=" + QString::number(currentBrowsePage);

        networkManager->get(QNetworkRequest(QUrl(url)));
    }
}

void BashOrgGet::getRandomPreviousePage()
{
    getRandomPage();
}

void BashOrgGet::getTopp100_200PreviousePage()
{
    getTopp100_200Page();
}

void BashOrgGet::getNumberOfBrowsePages()
{
    QString url = "http://www.bash.org/?browse";

    browsePageManager->get(QNetworkRequest(QUrl(url)));
}

void BashOrgGet::parseBrowsePage(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
        emit connectionFail();
    else
    {
        QByteArray data = reply->readAll();
        QXmlInputSource src;
        LibXml2Reader reader;
        QDomDocument doc;

        src.setData(data);
        doc.setContent(&src, &reader);

        QDomElement testElem = doc.documentElement();
        QDomNodeList testList = testElem.elementsByTagName("option");
        browsePageNumber = testList.count();
        currentBrowsePage = browsePageNumber;
    }
}

void BashOrgGet::pageParsed(QVector<QString> &text, QVector<QString> &number,
                            QVector<QString> &raiting)
{
    emit(parseResult(text, number, raiting, BASH_ORG, section));
    emit closeLoading();
    if(section == BASH_ORG_BROWSE)
        emit pageNums(BASH_ORG, section, browsePageNumber, currentBrowsePage);
    else
        emit pageNums(BASH_ORG, section, 1, 1);
}

void BashOrgGet::noConnection()
{
    emit connectionFail();
}

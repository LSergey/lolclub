#include "shortikiComGet.h"

ShortikiComGet::ShortikiComGet(QObject *parent) :
    QObject(parent)
{
    parser = new ParserShortikiCom();
    networkManager = new QNetworkAccessManager(this);
    pageNumManager = new QNetworkAccessManager(this);

    totalPageNumber = 0;

    connect(parser, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&)),
            this, SLOT(pageParsed(QVector<QString>& , QVector<QString>& , QVector<QString>& )));
    connect(parser, SIGNAL(pagesNumParseResult(int)), this, SLOT(setPageNumbers(int)));
    connect(parser, SIGNAL(connectionFail()), this, SLOT(noConnection()));
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(parse(QNetworkReply*)));
    connect(pageNumManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(pageNumParser(QNetworkReply*)));
}

ShortikiComGet::~ShortikiComGet()
{
    delete parser;
    delete networkManager;
    delete pageNumManager;
}

void ShortikiComGet::getNewPage()
{
    emit loading();
    QString url = "http://shortiki.com/";
    section = SHORTIKI_NEW;

    networkManager->get(QNetworkRequest(QUrl(url)));
    pageNumManager->get(QNetworkRequest(QUrl(url)));
}

void ShortikiComGet::getRandomPage()
{
    emit loading();
    QString url = "http://shortiki.com/random.php";
    section = SHORTIKI_RANDOM;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void ShortikiComGet::getTopPage()
{
    emit loading();
    QString url = "http://shortiki.com/top.php";
    section = SHORTIKI_TOP;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void ShortikiComGet::getByRaitingPage()
{
    emit loading();
    QString url = "http://shortiki.com/rating.php";
    section = SHORTIKI_BYRAITING;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void ShortikiComGet::getNextPage()
{
    if (section == SHORTIKI_RANDOM)
            getRandomPage();
    else if (section == SHORTIKI_TOP)
        getTopPage();
    else if (section == SHORTIKI_BYRAITING)
    {
        if (raitPageNumber > 1)
        {
            emit loading();
            raitPageNumber -= 1;
            QString url = "http://shortiki.com/rating.php?page=" + QString::number(raitPageNumber);

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
    else if (section == SHORTIKI_NEW)
    {
        if (currentPageNumber > 1)
        {
            emit loading();
            currentPageNumber -= 1;
            QString url = "http://shortiki.com/index.php?page=" + QString::number(currentPageNumber);

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
}

void ShortikiComGet::getPreviosePage()
{
    if (section == SHORTIKI_RANDOM)
        getRandomPage();
    else if (section == SHORTIKI_TOP)
        getTopPage();
    else if (section == SHORTIKI_BYRAITING)
    {
        if (raitPageNumber < totalPageNumber)
        {
            emit loading();
            raitPageNumber += 1;
            QString url = "http://shortiki.com/rating.php?page=" + QString::number(raitPageNumber);

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
    else if (section == SHORTIKI_NEW)
    {
        if (currentPageNumber < totalPageNumber)
        {
            emit loading();
            currentPageNumber += 1;
            QString url = "http://shortiki.com/index.php?page=" + QString::number(currentPageNumber);

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
}

void ShortikiComGet::setPageNumbers(int num)
{
    totalPageNumber = num;
    currentPageNumber = totalPageNumber;
    raitPageNumber = num;
}

void ShortikiComGet::pageParsed(QVector<QString> &text, QVector<QString> &number,
                                QVector<QString> &raiting)
{
    emit(parseResult(text, number, raiting, SHORTIKI_COM, section));
    emit closeLoading();
}

void ShortikiComGet::noConnection()
{
    emit connectionFail();
}

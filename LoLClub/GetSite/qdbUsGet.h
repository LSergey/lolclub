#ifndef QDBUSGET_H
#define QDBUSGET_H

#include <QObject>
#include <QDomDocument>
#include <QNetworkAccessManager>

#include "parserQdbUs.h"
#include "state.h"

class QdbUsGet : public QObject
{
    Q_OBJECT
public:
    explicit QdbUsGet(QObject *parent = 0);
    ~QdbUsGet();
    
    ParserQdbUs *parser;
signals:
    void parseResult(QVector<QString>& text,QVector<QString>& number,
                     QVector<QString>& raiting, SITE site, SECTIONS section);
    void loading();
    void closeLoading();
    void connectionFail();
    
private slots:
    void getTodayPage();
    void getLatestPage();
    void getBestPage();
    void getTopPage();
    void getRandomPage();

    void getNextPage(SECTIONS);
    void getPreviosePage(SECTIONS);

    void pageParsed(QVector<QString>& text,QVector<QString>& number, QVector<QString>& raiting);

    void noConnection();

private:
    QNetworkAccessManager *networkManager;
    SECTIONS section;
    int latestPageNumber, bestPageNumber, topPageNumber;
};

#endif // QDBUSGET_H

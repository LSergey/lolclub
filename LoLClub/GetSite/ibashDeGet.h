#ifndef IBASHDEGET_H
#define IBASHDEGET_H

#include <QObject>
#include <QDomDocument>
#include <QNetworkAccessManager>

#include "parserIbashDe.h"
#include "state.h"

class IbashDeGet : public QObject
{
    Q_OBJECT
public:
    explicit IbashDeGet(QObject *parent = 0);
    ~IbashDeGet();
    
    ParserIbashDe *parser;
signals:
    void parseResult(QVector<QString>& text,QVector<QString>& number,
                     QVector<QString>& raiting, SITE site, SECTIONS section);
    void loading();
    void closeLoading();
    void connectionFail();

private slots:
    void getNewPage();
    void getTopPage();
    void getRandomPage();

    void getNextPage(SECTIONS);
    void getPreviosePage(SECTIONS);

    void pageParsed(QVector<QString>& text,QVector<QString>& number,
                    QVector<QString>& raiting);

    void noConnection();

private:
    QNetworkAccessManager *networkManager;
    SECTIONS section;
    int pageNumber;
    int topPageNumber;
};

#endif // IBASHDEGET_H

#ifndef DANSTONCHATCOMGET_H
#define DANSTONCHATCOMGET_H

#include <QObject>
#include <QDomDocument>
#include <QNetworkAccessManager>

#include "parserDanstonchatCom.h"
#include "state.h"

class DanstonchatComGet : public QObject
{
    Q_OBJECT
public:
    explicit DanstonchatComGet(QObject *parent = 0);
    ~DanstonchatComGet();

    ParserDanstonchatCom *parser;

signals:
    void parseResult(QVector<QString>& text,QVector<QString>& number,
                     QVector<QString>& raiting, SITE site, SECTIONS section);
    void loading();
    void closeLoading();
    void connectionFail();

private slots:
    void getLatestPage();
    void getTopPage();
    void getRandomPage();
    void getBrowsePage();

    void getNextPage(SECTIONS);
    void getPreviosePage(SECTIONS);

    void pageParsed(QVector<QString>& text,QVector<QString>& number,
                    QVector<QString>& raiting);

    void noConnection();

private:
    QNetworkAccessManager *networkManager;
    SECTIONS section;
    int pageNumber;
    int browsePageNumber;
};

#endif // DANSTONCHATCOMGET_H

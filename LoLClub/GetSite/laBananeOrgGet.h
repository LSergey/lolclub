#ifndef LABANANEORGGET_H
#define LABANANEORGGET_H

#include <QObject>
#include <QDomDocument>
#include <QNetworkAccessManager>

#include "parserLaBananeOrg.h"
#include "state.h"

class LaBananeOrgGet : public QObject
{
    Q_OBJECT
public:
    explicit LaBananeOrgGet(QObject *parent = 0);
    ~LaBananeOrgGet();

    ParserLaBananeOrg *parser;

signals:
    void parseResult(QVector<QString>& text,QVector<QString>& number,
                     QVector<QString>& raiting, SITE site, SECTIONS section);
    void loading();
    void closeLoading();
    void connectionFail();

private slots:
    void getLatestPage();
    void getBrowsePage();
    void getTopPage();
    void getRandomPage();

    void getNextPage(SECTIONS);
    void getPreviosePage(SECTIONS);

    void pageParsed(QVector<QString>& text,QVector<QString>& number, QVector<QString>& raiting);

    void noConnection();

private:
    QNetworkAccessManager *networkManager;
    SECTIONS section;
    int latestPageNumber;
    int browsePageNumber;
};

#endif // LABANANEORGGET_H

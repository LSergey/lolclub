#include "gBashOrgGet.h"

GBashOrgGet::GBashOrgGet(QObject *parent) :
    QObject(parent)
{
    parser = new ParserGBashOrg();
    networkManager = new QNetworkAccessManager(this);

    pageNumber = 1;

    connect(parser, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&)),
            this, SLOT(pageParsed(QVector<QString>& , QVector<QString>& , QVector<QString>& )));
    connect(parser, SIGNAL(connectionFail()), this, SLOT(noConnection()));
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(parse(QNetworkReply*)));
}

GBashOrgGet::~GBashOrgGet()
{
    delete parser;
    delete networkManager;
}

void GBashOrgGet::getLatestPage()
{
    emit loading();
    QString url = "http://german-bash.org/action/latest";
    section = GBASH_ORG_LATEST;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void GBashOrgGet::getBrowsePage()
{
    emit loading();
    QString url = "http://german-bash.org/action/browse";
    section = GBASH_ORG_BROWSE;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void GBashOrgGet::getRandomPage()
{
    emit loading();
    QString url = "http://german-bash.org/action/random";
    section = GBASH_ORG_RANDOM;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void GBashOrgGet::getTopPage()
{
    emit loading();
    QString url = "http://german-bash.org/action/top/";
    section = GBASH_ORG_TOP;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void GBashOrgGet::getNextPage(SECTIONS sections)
{
    if (sections == GBASH_ORG_LATEST)
    {
        section = GBASH_ORG_BROWSE;
        getBrowsePage();
    }
    else if (sections == GBASH_ORG_RANDOM)
        getRandomPage();
    else if (sections == GBASH_ORG_TOP)
        getTopPage();
    else if (sections == GBASH_ORG_BROWSE)
    {
        if (pageNumber < 305)
        {
            emit loading();
            pageNumber += 1;
            QString url = "http://german-bash.org/action/browse/page/" + QString::number(pageNumber);

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
}

void GBashOrgGet::getPreviosePage(SECTIONS sections)
{
    if (sections == GBASH_ORG_LATEST)
        getLatestPage();
    else if (sections == GBASH_ORG_RANDOM)
        getRandomPage();
    else if (sections == GBASH_ORG_TOP)
        getTopPage();
    else if (sections == GBASH_ORG_BROWSE)
    {
        if (pageNumber > 1)
        {
            emit loading();
            pageNumber -= 1;
            QString url = "http://german-bash.org/action/browse/page/" + QString::number(pageNumber);

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
}

void GBashOrgGet::pageParsed(QVector<QString> &text, QVector<QString> &number, QVector<QString> &raiting)
{
    emit(parseResult(text, number, raiting, GBASH_ORG, section));
    emit closeLoading();
}

void GBashOrgGet::noConnection()
{
    emit connectionFail();
}

#include "itHappensRuGet.h"

ItHappensRuGet::ItHappensRuGet(QObject *parent) :
    QObject(parent)
{
    parser = new ParserItHappensRu();
    networkManager = new QNetworkAccessManager(this);
    pageNumManager = new QNetworkAccessManager(this);

    totalPageNumber = 0;

    connect(parser, SIGNAL(itHappensRuParseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&)),
            this, SLOT(pageParsed(QVector<QString>& , QVector<QString>& , QVector<QString>& )));
    connect(parser, SIGNAL(pagesNumParseResult(int)), this, SLOT(setPageNumbers(int)));
    connect(parser, SIGNAL(connectionFail()), this, SLOT(noConnection()));
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(parseItHappensRuhtml(QNetworkReply*)));
    connect(pageNumManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(pageNumParser(QNetworkReply*)));
}

ItHappensRuGet::~ItHappensRuGet()
{
    delete parser;
    delete networkManager;
    delete pageNumManager;
}

void ItHappensRuGet::getNewPage()
{
    emit loading();
    QString url = "http://ithappens.ru/";
    section = ITHAPPENS_RU_NEW;

    networkManager->get(QNetworkRequest(QUrl(url)));
    pageNumManager->get(QNetworkRequest(QUrl(url)));
}

void ItHappensRuGet::getBestPage()
{
    emit loading();
    QString url = "http://ithappens.ru/top/";
    section = ITHAPPENS_RU_BEST;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void ItHappensRuGet::getRandomPage()
{
    emit loading();
    int storyNumber = rand() % 11527 + 1;
    QString url = "http://ithappens.ru/story/" + QString::number(storyNumber);
    section = ITHAPPENS_RU_RANDOM;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void ItHappensRuGet::getNextPage()
{
    if (section == ITHAPPENS_RU_RANDOM)
        getRandomPage();
    else if (section == ITHAPPENS_RU_BEST)
        getBestPage();
    else
    {
        if (currentPageNumber > 1)
        {
            emit loading();
            currentPageNumber -= 1;
            QString url = "http://ithappens.ru/page/" + QString::number(currentPageNumber);
            section = ITHAPPENS_RU_NEW;

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
}

void ItHappensRuGet::getPreviosePage()
{
    if (section == ITHAPPENS_RU_RANDOM)
        getRandomPage();
    else if (section == ITHAPPENS_RU_BEST)
        getBestPage();
    else
    {
        if (currentPageNumber < totalPageNumber)
        {
            emit loading();
            currentPageNumber += 1;
            QString url = "http://ithappens.ru/page/" + QString::number(currentPageNumber);
            section = ITHAPPENS_RU_NEW;

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
}

void ItHappensRuGet::setPageNumbers(int num)
{
    totalPageNumber = num;
    currentPageNumber = totalPageNumber;
}

void ItHappensRuGet::pageParsed(QVector<QString> &text,
                                QVector<QString> &number, QVector<QString> &raiting)
{
    emit(parseResult(text, number, raiting, ITHAPPENS_RU, section));
    emit closeLoading();
}

void ItHappensRuGet::noConnection()
{
    emit connectionFail();
}

#ifndef BASHIMGET_H
#define BASHIMGET_H

#include <QObject>
#include <QDomDocument>
#include <QNetworkAccessManager>

#include "parserBashIm.h"
#include "state.h"

class BashImGet : public QObject
{
    Q_OBJECT
public:
    explicit BashImGet(QObject *parent = 0);
    ~BashImGet();
    
    ParserBashIm* parser;

signals:
    void parseResult(QVector<QString>& text,QVector<QString>& number,
                     QVector<QString>& raiting, SITE site, SECTIONS section);
    void pageNums(SITE site, SECTIONS section, int total, int current);
    void loading();
    void closeLoading();
    void connectionFail();
    
private slots:
    void getBashImPage();
    void getRandomPage();
    void getBestPage();
    void getByRaitingPage();
    void getAbyssPage();
    void getAbyssBestPage();
    void getAbyssTopPage();

    void nextPage(SECTIONS);
    void previousPage(SECTIONS);

    void pageParsed(QVector<QString>& text,QVector<QString>& number,
                    QVector<QString>& raiting);

    void getNumOfPages();
    void getNumOfAbyssbestPages();
    void parseNumOfPage(QNetworkReply*);
    void parseNumOfAbyssPage(QNetworkReply*);

    void noConnection();

private:
    QNetworkAccessManager *networkManager, *newPagesManager, *abyssPagesManager;
    int numOfPages, currentPageNumber, currentRaitingPage, abyssbestNumOfPages,
            currentAbyssbestNumber, previousAbyssbestNumber, nextAbyssbestNumber;
    SECTIONS section;

    void getBashImNextPage();
    void getRandomNextPage();
    void getBestNextPage();
    void getByRaitingNextPage();
    void getAbyssNextPage();
    void getAbyssBestNextPage();
    void getAbyssTopNextPage();

    void getBashImPreviousPage();
    void getRandomPreviousPage();
    void getBestPreviousPage();
    void getByRaitingPreviousPage();
    void getAbyssPreviousPage();
    void getAbyssBestPreviousPage();
    void getAbyssTopPreviousPage();
};

#endif // BASHIMGET_H

#ifndef TJBASHORGGET_H
#define TJBASHORGGET_H

#include <QObject>
#include <QDomDocument>
#include <QNetworkAccessManager>

#include "parserTjBashOrg.h"
#include "state.h"

class TjBashOrgGet : public QObject
{
    Q_OBJECT
public:
    explicit TjBashOrgGet(QObject *parent = 0);
    ~TjBashOrgGet();

    ParserTjBashOrg *parser;

signals:
    void parseResult(QVector<QString>& text,QVector<QString>& number,
                     QVector<QString>& raiting, SITE site, SECTIONS section);
    void loading();
    void closeLoading();
    void connectionFail();

private slots:
    void getBrowsePage();
    void getTopPage();
    void getRandomPage();

    void getNextPage(SECTIONS);
    void getPreviosePage(SECTIONS);

    void pageParsed(QVector<QString>& text,QVector<QString>& number, QVector<QString>& raiting);

    void noConnection();

private:
    QNetworkAccessManager *networkManager;
    SECTIONS section;
    int browsePageNumber;
    int topPageNumber;
    
};

#endif // TJBASHORGGET_H

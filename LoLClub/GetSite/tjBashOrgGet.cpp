#include "tjBashOrgGet.h"

TjBashOrgGet::TjBashOrgGet(QObject *parent) :
    QObject(parent)
{
    parser = new ParserTjBashOrg();
    networkManager = new QNetworkAccessManager(this);

    browsePageNumber = 0;
    topPageNumber = 0;

    connect(parser, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&)),
            this, SLOT(pageParsed(QVector<QString>& , QVector<QString>& , QVector<QString>& )));
    connect(parser, SIGNAL(connectionFail()), this, SLOT(noConnection()));
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(parse(QNetworkReply*)));
}

TjBashOrgGet::~TjBashOrgGet()
{
    delete parser;
    delete networkManager;
}

void TjBashOrgGet::getBrowsePage()
{
    emit loading();
    QString url = "http://www.tjbash.org/browse";
    section = TJBASH_ORG_BROWSE;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void TjBashOrgGet::getRandomPage()
{
    emit loading();
    QString url = "http://www.tjbash.org/random1";
    section = TJBASH_ORG_RANDOM;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void TjBashOrgGet::getTopPage()
{
    emit loading();
    QString url = "http://www.tjbash.org/top";
    section = TJBASH_ORG_TOP;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void TjBashOrgGet::getNextPage(SECTIONS sections)
{
    if (sections == TJBASH_ORG_RANDOM)
        getRandomPage();
    else if (sections == TJBASH_ORG_BROWSE)
    {
        if (browsePageNumber < 3425)
        {
            emit loading();
            browsePageNumber += 25;
            QString url = "http://www.tjbash.org/browse?start=" + QString::number(browsePageNumber);
            section = TJBASH_ORG_BROWSE;

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
    else if (sections == TJBASH_ORG_TOP)
    {
        if (topPageNumber < 3425)
        {
            emit loading();
            topPageNumber += 25;
            QString url = "http://www.tjbash.org/top?start=" + QString::number(topPageNumber);
            section = TJBASH_ORG_TOP;

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
}

void TjBashOrgGet::getPreviosePage(SECTIONS sections)
{
    if (sections == TJBASH_ORG_RANDOM)
        getRandomPage();
    else if (sections == TJBASH_ORG_BROWSE)
    {
        if (browsePageNumber > 25)
        {
            emit loading();
            browsePageNumber -= 25;
            QString url = "http://www.tjbash.org/browse?start=" + QString::number(browsePageNumber);
            section = TJBASH_ORG_BROWSE;

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
        else if (browsePageNumber == 25)
        {
            browsePageNumber = 0;
            getBrowsePage();
        }
    }
    else if (sections == TJBASH_ORG_TOP)
    {
        if (topPageNumber > 25)
        {
            emit loading();
            topPageNumber -= 25;
            QString url = "http://www.tjbash.org/top?start=" + QString::number(topPageNumber);
            section = TJBASH_ORG_TOP;

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
        else if (topPageNumber == 25)
        {
            topPageNumber = 0;
            getTopPage();
        }
    }
}

void TjBashOrgGet::pageParsed(QVector<QString> &text, QVector<QString> &number, QVector<QString> &raiting)
{
    emit(parseResult(text, number, raiting, TJBASH_ORG, section));
    emit closeLoading();
}

void TjBashOrgGet::noConnection()
{
    emit connectionFail();
}

#include "mmtComGet.h"

MMTComGet::MMTComGet(QObject *parent) :
    QObject(parent)
{
    parser = new ParserMMTCom();
    networkManager = new QNetworkAccessManager(this);

    currentPageNumber = 1;

    connect(parser, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&)),
            this, SLOT(pageParsed(QVector<QString>& , QVector<QString>& , QVector<QString>& )));
    connect(parser, SIGNAL(connectionFail()), this, SLOT(noConnection()));
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(parse(QNetworkReply*)));
}

MMTComGet::~MMTComGet()
{
    delete parser;
    delete networkManager;
}

void MMTComGet::getNewPage()
{
    emit loading();
    QString url = "http://makesmethink.com/";
    section = MMT_NEW;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void MMTComGet::getTopPage()
{
    emit loading();
    QString url = "http://makesmethink.com/top";
    section = MMT_TOP;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void MMTComGet::getRandomPage()
{
    emit loading();
    QString url = "http://makesmethink.com/random";
    section = MMT_RANDOM;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void MMTComGet::getNextPage(SECTIONS sections)
{
    if (sections == MMT_RANDOM)
        getRandomPage();
    else if (sections == MMT_TOP)
        getTopPage();
    else if (sections == MMT_NEW)
    {
        if (currentPageNumber < 181)
        {
            emit loading();
            currentPageNumber += 1;
            QString url = "http://makesmethink.com/?page=" + QString::number(currentPageNumber);
            section = MMT_NEW;

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
}

void MMTComGet::getPreviosePage(SECTIONS sections)
{
    if (sections == MMT_RANDOM)
        getRandomPage();
    else if (sections == MMT_TOP)
        getTopPage();
    else if (sections == MMT_NEW)
    {
        if (currentPageNumber > 1)
        {
            emit loading();
            currentPageNumber -= 1;
            QString url = "http://makesmethink.com/?page=" + QString::number(currentPageNumber);
            section = MMT_NEW;

            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
}

void MMTComGet::pageParsed(QVector<QString> &text, QVector<QString> &number, QVector<QString> &raiting)
{
    emit(parseResult(text, number, raiting, MMT_COM, section));
    emit closeLoading();
}

void MMTComGet::noConnection()
{
    emit connectionFail();
}

#include "iBashOrgGet.h"

IBashOrgGet::IBashOrgGet(QObject *parent) :
    QObject(parent)
{
    parser = new ParserIBashOrg();
    networkManager = new QNetworkAccessManager(this);

    byDatePageNumber = 1;
    raitPageNumber = 1;

    connect(parser, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&)),
            this, SLOT(pageParsed(QVector<QString>& , QVector<QString>& , QVector<QString>& )));
    connect(parser, SIGNAL(connectionFail()), this, SLOT(noConnection()));
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(parse(QNetworkReply*)));
}

IBashOrgGet::~IBashOrgGet()
{
    delete parser;
    delete networkManager;
}

void IBashOrgGet::getIBashPage()
{
    emit loading();
    QString url = "http://ibash.org.ru/";
    section = IBASH_ORG_BYDATE;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void IBashOrgGet::getByRaitingPage()
{
    emit loading();
    QString url = "http://ibash.org.ru/best.php";
    section = IBASH_ORG_BYRAITING;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void IBashOrgGet::getRandomPage()
{
    emit loading();
    QString url = "http://ibash.org.ru/random.php";
    section = IBASH_ORG_RANDOM;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void IBashOrgGet::getNextPage()
{
    if (section == IBASH_ORG_BYDATE)
    {
        emit loading();
        byDatePageNumber += 1;
        QString url = "http://ibash.org.ru/?page=" + QString::number(byDatePageNumber);

        networkManager->get(QNetworkRequest(QUrl(url)));
    } else if (section == IBASH_ORG_BYRAITING)
    {
        emit loading();
        raitPageNumber += 1;
        QString url = "http://ibash.org.ru/best.php?page=" + QString::number(raitPageNumber);

        networkManager->get(QNetworkRequest(QUrl(url)));
    } else
    {
        getRandomPage();
    }

}

void IBashOrgGet::getPreviosePage()
{
    if (section == IBASH_ORG_BYDATE)
    {
        if (byDatePageNumber > 1)
        {
            emit loading();
            byDatePageNumber -= 1;
            QString url = "http://ibash.org.ru/?page=" + QString::number(byDatePageNumber);
            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    } else if (section == IBASH_ORG_BYRAITING)
    {
        if (raitPageNumber > 1)
        {
            emit loading();
            raitPageNumber -= 1;
            QString url = "http://ibash.org.ru/best.php?page=" + QString::number(raitPageNumber);
            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    } else
    {
        getRandomPage();
    }

}

void IBashOrgGet::pageParsed(QVector<QString> &text, QVector<QString> &number,
                             QVector<QString> &raiting)
{
    emit parseResult(text, number, raiting, IBASH_ORG, section);
    emit closeLoading();
}

void IBashOrgGet::noConnection()
{
    emit connectionFail();
}

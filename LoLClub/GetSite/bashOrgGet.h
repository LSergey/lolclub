#ifndef BASHORGGET_H
#define BASHORGGET_H

#include <QObject>
#include <QNetworkAccessManager>

#include "parserBashOrg.h"
#include "state.h"

class BashOrgGet : public QObject
{
    Q_OBJECT
public:
    explicit BashOrgGet(QObject *parent = 0);
    ~BashOrgGet();

    ParserBashOrg* parser;
    
signals:
    void parseResult(QVector<QString>& text,QVector<QString>& number,
                     QVector<QString>& raiting, SITE site, SECTIONS section);
    void pageNums(SITE site, SECTIONS section, int total, int current);
    void loading();
    void closeLoading();
    void connectionFail();

private slots:
    void getLatestPage();
    void getBrowsePage();
    void getRandomPage();
    void getTopp100_200Page();

    void nextPage(SECTIONS);
    void previousePage(SECTIONS);

    void pageParsed(QVector<QString>& text,QVector<QString>& number,
                    QVector<QString>& raiting);

    void getNumberOfBrowsePages();
    void parseBrowsePage(QNetworkReply*);

    void noConnection();

private:
    QNetworkAccessManager *networkManager, *browsePageManager;
    SECTIONS section;
    int browsePageNumber;
    int currentBrowsePage;

    void getLatestNextPage();
    void getBrowseNextPage();
    void getRandomNextPage();
    void getTopp100_200NextPage();

    void getLatestPreviousePage();
    void getBrowsePreviousePage();
    void getRandomPreviousePage();
    void getTopp100_200PreviousePage();

};

#endif // BASHORGGET_H

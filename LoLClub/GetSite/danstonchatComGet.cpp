#include "danstonchatComGet.h"

DanstonchatComGet::DanstonchatComGet(QObject *parent) :
    QObject(parent)
{
    parser = new ParserDanstonchatCom();
    networkManager = new QNetworkAccessManager(this);

    pageNumber = 1;
    browsePageNumber = 1;

    connect(parser, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&)),
            this, SLOT(pageParsed(QVector<QString>& , QVector<QString>& , QVector<QString>& )));
    connect(parser, SIGNAL(connectionFail()), this, SLOT(noConnection()));
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(parse(QNetworkReply*)));
}

DanstonchatComGet::~DanstonchatComGet()
{
    delete parser;
    delete networkManager;
}

void DanstonchatComGet::getLatestPage()
{
    emit loading();
    QString url = "http://danstonchat.com/latest.html";
    section = DANSTONCHAT_COM_LATEST;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void DanstonchatComGet::getBrowsePage()
{
    emit loading();
    QString url = "http://danstonchat.com/browse.html";
    section = DANSTONCHAT_COM_BROWSE;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void DanstonchatComGet::getRandomPage()
{
    emit loading();
    QString url = "http://danstonchat.com/random0.html";
    section = DANSTONCHAT_COM_RANDOM;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void DanstonchatComGet::getTopPage()
{
    emit loading();
    QString url = "http://danstonchat.com/top50.html";
    section = DANSTONCHAT_COM_TOP50;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void DanstonchatComGet::getNextPage(SECTIONS sections)
{
    if (sections == DANSTONCHAT_COM_LATEST)
    {
        emit loading();
        pageNumber += 1;
        QString url = "http://danstonchat.com/latest/" + QString::number(pageNumber) + ".html";
        networkManager->get(QNetworkRequest(QUrl(url)));
    }
    else if (sections == DANSTONCHAT_COM_BROWSE)
    {
        {
            emit loading();
            browsePageNumber += 1;
            QString url = "http://danstonchat.com/browse/" + QString::number(browsePageNumber) + ".html";
            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
    else if (sections == DANSTONCHAT_COM_RANDOM)
        getRandomPage();
    else if (sections == DANSTONCHAT_COM_TOP50)
        getTopPage();
}

void DanstonchatComGet::getPreviosePage(SECTIONS sections)
{
    if (sections == DANSTONCHAT_COM_LATEST)
    {
        if (pageNumber > 1)
        {
            emit loading();
            pageNumber -= 1;
            QString url = "http://danstonchat.com/latest/" + QString::number(pageNumber) + ".html";
            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
    else if (sections == DANSTONCHAT_COM_BROWSE)
    {
        {
            if (browsePageNumber > 1)
            {
                emit loading();
                browsePageNumber -= 1;
                QString url = "http://danstonchat.com/browse/" + QString::number(browsePageNumber) + ".html";
                networkManager->get(QNetworkRequest(QUrl(url)));
            }
        }
    }
    else if (sections == DANSTONCHAT_COM_RANDOM)
        getRandomPage();
    else if (sections == DANSTONCHAT_COM_TOP50)
        getTopPage();
}

void DanstonchatComGet::pageParsed(QVector<QString> &text, QVector<QString> &number, QVector<QString> &raiting)
{
    emit(parseResult(text, number, raiting, DANSTONCHAT_COM, section));
    emit closeLoading();
}

void DanstonchatComGet::noConnection()
{
    emit connectionFail();
}

#include "laBananeOrgGet.h"

LaBananeOrgGet::LaBananeOrgGet(QObject *parent) :
    QObject(parent)
{
    parser = new ParserLaBananeOrg();
    networkManager = new QNetworkAccessManager(this);

    latestPageNumber = 1;
    browsePageNumber = 1;

    connect(parser, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&)),
            this, SLOT(pageParsed(QVector<QString>& , QVector<QString>& , QVector<QString>& )));
    connect(parser, SIGNAL(connectionFail()), this, SLOT(noConnection()));
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(parse(QNetworkReply*)));
}

LaBananeOrgGet::~LaBananeOrgGet()
{
    delete parser;
    delete networkManager;
}

void LaBananeOrgGet::getLatestPage()
{
    emit loading();
    QString url = "http://labanane.org/?sort=latest&p=1";
    section = LABANANE_ORG_LATEST;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void LaBananeOrgGet::getBrowsePage()
{
    emit loading();
    QString url = "http://labanane.org/?sort=browse&p=1";
    section = LABANANE_ORG_BROWSE;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void LaBananeOrgGet::getTopPage()
{
    emit loading();
    QString url = "http://labanane.org/?sort=top50";
    section = LABANANE_ORG_TOP;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void LaBananeOrgGet::getRandomPage()
{
    emit loading();
    QString url = "http://labanane.org/?sort=random2";
    section = LABANANE_ORG_RANDOM;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void LaBananeOrgGet::getNextPage(SECTIONS sections)
{
    if (sections == LABANANE_ORG_LATEST)
    {
        if(latestPageNumber < 93)
        {
            emit loading();
            latestPageNumber += 1;
            QString url = "http://labanane.org/?sort=latest&p=" + QString::number(latestPageNumber);
            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
    else if (sections == LABANANE_ORG_BROWSE)
    {
        if(browsePageNumber < 93)
        {
            emit loading();
            browsePageNumber += 1;
            QString url = "http://labanane.org/?sort=browse&p=" + QString::number(browsePageNumber);
            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
    else if (sections == LABANANE_ORG_RANDOM)
        getRandomPage();
    else if (sections == LABANANE_ORG_TOP)
        getTopPage();
}

void LaBananeOrgGet::getPreviosePage(SECTIONS sections)
{
    if (sections == LABANANE_ORG_LATEST)
    {
        if(latestPageNumber > 1)
        {
            emit loading();
            latestPageNumber -= 1;
            QString url = "http://labanane.org/?sort=latest&p=" + QString::number(latestPageNumber);
            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
    else if (sections == LABANANE_ORG_BROWSE)
    {
        if(browsePageNumber > 1)
        {
            emit loading();
            browsePageNumber -= 1;
            QString url = "http://labanane.org/?sort=browse&p=" + QString::number(browsePageNumber);
            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
    else if (sections == LABANANE_ORG_RANDOM)
        getRandomPage();
    else if (sections == LABANANE_ORG_TOP)
        getTopPage();
}

void LaBananeOrgGet::pageParsed(QVector<QString> &text, QVector<QString> &number, QVector<QString> &raiting)
{
    emit(parseResult(text, number, raiting, LABANANE_ORG, section));
    emit closeLoading();
}

void LaBananeOrgGet::noConnection()
{
    emit connectionFail();
}

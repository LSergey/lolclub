#include "ibashDeGet.h"

IbashDeGet::IbashDeGet(QObject *parent) :
    QObject(parent)
{
    parser = new ParserIbashDe();
    networkManager = new QNetworkAccessManager(this);

    pageNumber = 1;
    topPageNumber = 1;

    connect(parser, SIGNAL(parseResult(QVector<QString>&,QVector<QString>&,QVector<QString>&)),
            this, SLOT(pageParsed(QVector<QString>& , QVector<QString>& , QVector<QString>& )));
    connect(parser, SIGNAL(connectionFail()), this, SLOT(noConnection()));
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), parser, SLOT(parse(QNetworkReply*)));
}

IbashDeGet::~IbashDeGet()
{
    delete parser;
    delete networkManager;
}

void IbashDeGet::getNewPage()
{
    emit loading();
    QString url = "http://ibash.de/neueste_1.html";
    section = IBASH_DE_NEW;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void IbashDeGet::getTopPage()
{
    emit loading();
    QString url = "http://ibash.de/top_1.html";
    section = IBASH_DE_TOP;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void IbashDeGet::getRandomPage()
{
    emit loading();
    QString url = "http://ibash.de/random.html";
    section = IBASH_DE_RANDOM;

    networkManager->get(QNetworkRequest(QUrl(url)));
}

void IbashDeGet::getNextPage(SECTIONS sections)
{
    if (sections == IBASH_DE_NEW)
    {
        emit loading();
        pageNumber += 1;
        QString url = "http://ibash.de/neueste_" + QString::number(pageNumber) + ".html";
        networkManager->get(QNetworkRequest(QUrl(url)));
    }
    else if (sections == IBASH_DE_TOP)
    {
        {
            emit loading();
            topPageNumber += 1;
            QString url = "http://ibash.de/top_" + QString::number(topPageNumber) + ".html";
            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
    else
        getRandomPage();
}

void IbashDeGet::getPreviosePage(SECTIONS sections)
{
    if (sections == IBASH_DE_NEW)
    {
        if (pageNumber > 1)
        {
            emit loading();
            pageNumber -= 1;
            QString url = "http://ibash.de/neueste_" + QString::number(pageNumber) + ".html";
            networkManager->get(QNetworkRequest(QUrl(url)));
        }
    }
    else if (sections == IBASH_DE_TOP)
    {
        {
            if (topPageNumber > 1)
            {
                emit loading();
                topPageNumber -= 1;
                QString url = "http://ibash.de/top_" + QString::number(topPageNumber) + ".html";
                networkManager->get(QNetworkRequest(QUrl(url)));
            }
        }
    }
    else
        getRandomPage();
}

void IbashDeGet::pageParsed(QVector<QString> &text, QVector<QString> &number,
                            QVector<QString> &raiting)
{
    emit(parseResult(text, number, raiting, IBASH_DE, section));
    emit closeLoading();
}

void IbashDeGet::noConnection()
{
    emit connectionFail();
}

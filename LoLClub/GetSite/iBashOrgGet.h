#ifndef IBASHORGGET_H
#define IBASHORGGET_H

#include <QObject>
#include <QDomDocument>
#include <QNetworkAccessManager>

#include "parserIBashOrg.h"
#include "state.h"

class IBashOrgGet : public QObject
{
    Q_OBJECT
public:
    explicit IBashOrgGet(QObject *parent = 0);
    ~IBashOrgGet();
    
    ParserIBashOrg *parser;
signals:
    void parseResult(QVector<QString>& text,QVector<QString>& number,
                     QVector<QString>& raiting, SITE site, SECTIONS section);
    void loading();
    void closeLoading();
    void connectionFail();
    
private slots:
    void getIBashPage();
    void getRandomPage();
    void getByRaitingPage();

    void getNextPage();
    void getPreviosePage();

    void pageParsed(QVector<QString>& text,QVector<QString>& number,
                    QVector<QString>& raiting);

    void noConnection();

private:
    QNetworkAccessManager *networkManager;
    SECTIONS section;
    int byDatePageNumber;
    int raitPageNumber;

};

#endif // IBASHORGGET_H

#ifndef DONATEWINDOW_H
#define DONATEWINDOW_H

#include <QWidget>
#include <QNetworkAccessManager>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QDesktopServices>

namespace Ui {
class DonateWindow;
}

class DonateWindow : public QWidget
{
    Q_OBJECT
    
public:
    explicit DonateWindow(QWidget *parent = 0);
    ~DonateWindow();
    
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::DonateWindow *ui;
    QNetworkAccessManager *networkManager;
};

#endif // DONATEWINDOW_H

#ifndef VERTICALMENUBAR_H
#define VERTICALMENUBAR_H

#include <QWidget>

namespace Ui {
class VerticalMenuBar;
}

class VerticalMenuBar : public QWidget
{
    Q_OBJECT
    
public:
    explicit VerticalMenuBar(QWidget *parent = 0);
    ~VerticalMenuBar();

signals:
    void nextPage();
    void previousePage();
    void save();
    void dayModeChange();
    
private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();

private:
    Ui::VerticalMenuBar *ui;
};

#endif // VERTICALMENUBAR_H

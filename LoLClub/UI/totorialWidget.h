#ifndef TOTORIALWIDGET_H
#define TOTORIALWIDGET_H

#include <QWidget>

namespace Ui {
class TotorialWidget;
}

class TotorialWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit TotorialWidget(QWidget *parent = 0);
    ~TotorialWidget();
    
private slots:
    void on_prevButton_clicked();

    void on_nextButton_clicked();

private:
    Ui::TotorialWidget *ui;

    QVector<QString> images;
    int imageNumber;
};

#endif // TOTORIALWIDGET_H

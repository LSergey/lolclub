#include "donateWindow.h"
#include "ui_donateWindow.h"

DonateWindow::DonateWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DonateWindow)
{
    ui->setupUi(this);
    networkManager = new QNetworkAccessManager(this);
    this->setStyleSheet("background-image: url(:/donate/res/donate.png)");
    ui->pushButton->setIconSize(QSize(195,75));
    ui->pushButton->setIcon(QIcon(":/buttons/res/donateButton.png"));
}

DonateWindow::~DonateWindow()
{
    delete ui;
    delete networkManager;
}

void DonateWindow::on_pushButton_clicked()
{
    QUrlQuery url("https://www.paypal.com/cgi-bin/webscr/");
    url.addQueryItem("cmd"     ,  "_s-xclick");
    url.addQueryItem("hosted_button_id"  , "RY9FEBHDQV6FE"  );

    QDesktopServices::openUrl(QUrl(url.toString()));
    this->close();
}

void DonateWindow::on_pushButton_2_clicked()
{
    this->close();
}

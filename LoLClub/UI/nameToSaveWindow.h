#ifndef NAMETOSAVEWINDOW_H
#define NAMETOSAVEWINDOW_H

#include <QWidget>
#include <QString>

namespace Ui {
class NameToSaveWindow;
}

class NameToSaveWindow : public QWidget
{
    Q_OBJECT
    
public:
    explicit NameToSaveWindow(QWidget *parent = 0);
    ~NameToSaveWindow();
    
signals:
    void nameSavedPage(QString);

private slots:
    void on_saveNameButton_clicked();
    void setStartOfsavedName(QString);

    void on_pushButton_clicked();

private:
    Ui::NameToSaveWindow *ui;
    QString name;
};

#endif // NAMETOSAVEWINDOW_H

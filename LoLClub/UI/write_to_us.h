#ifndef WRITE_TO_US_H
#define WRITE_TO_US_H

#include <QDialog>

namespace Ui {
class write_to_us;
}

class write_to_us : public QDialog
{
    Q_OBJECT
    
public:
    explicit write_to_us(QWidget *parent = 0);
    ~write_to_us();
    
private:
    Ui::write_to_us *ui;
};

#endif // WRITE_TO_US_H

#include "quoteView.h"
#include "ui_quoteView.h"
#include <QMenu>

QuoteView::QuoteView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QuoteView)
{
    ui->setupUi(this);
    ui->quoteText->setWordWrap(true);
    ui->numAndRate->setWordWrap(true);
    ui->socialButton->setIconSize(QSize(16,16));
    ui->socialButton->setIcon(QIcon(":/buttons/res/LC_Share_16X16.png"));

    socialMenu = new QMenu();
    vkAction_ = socialMenu->addAction("vkontake");
    fbAction_ = socialMenu->addAction("facebook");
    saveToTopAction_ = socialMenu->addAction("save to top");
    ui->socialButton->setMenu(socialMenu);

    saveToTopAction_->setIcon(QIcon(":/buttons/res/LC_Save_16X16.png"));
    vkAction_->setIcon(QIcon(":/buttons/res/vkontakte.png"));
    fbAction_->setIcon(QIcon(":/buttons/res/Facebook-icon_16x16.png"));
    connect(vkAction_, SIGNAL(triggered()), this, SLOT(vkPost_cliced()));
    connect(fbAction_, SIGNAL(triggered()), this, SLOT(fbPost_cliced()));
    connect(saveToTopAction_, SIGNAL(triggered()), this, SLOT(saveToTop_cliced()));

    ui->quoteText->setTextInteractionFlags(Qt::TextSelectableByMouse);
}

QuoteView::~QuoteView()
{
    delete ui;
    delete vkAction_;
    delete fbAction_;
    delete saveToTopAction_;
    delete socialMenu;
}

void QuoteView::setNumAndRate(QString &num, QString &rate, const int i)
{
    QString numAndRate("");
    numAndRate += num;
    numAndRate += "      ";
    numAndRate += rate;
    ui->numAndRate->setText(numAndRate);
}

void QuoteView::setQuote(QString &quote, const int i)
{
    ui->quoteText->setText(quote);
}

void QuoteView::on_socialButton_clicked()
{
    QString text = ui->quoteText->text();
    emit runVkPost(text);
}

void QuoteView::vkPost_cliced()
{
    QString text = ui->quoteText->text();
    emit runVkPost(text);
}

void QuoteView::fbPost_cliced()
{
    QString text = ui->quoteText->text();
    emit runFbPost(text);
}

void QuoteView::saveToTop_cliced()
{
    QString text = ui->quoteText->text();
    QTextCodec *codec = QTextCodec::codecForName("Utf-8");
    QTextCodec *codecUnicode = QTextCodec::codecForName("CP1251");
    QByteArray a;
    a = codec->fromUnicode(text);
    text = codecUnicode->toUnicode(a);
    QString numAndRait = ui->numAndRate->text();
    emit saveToTopSignal(numAndRait, text);
}

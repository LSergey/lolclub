#include "totorialWidget.h"
#include "ui_totorialWidget.h"

TotorialWidget::TotorialWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TotorialWidget)
{
    imageNumber = 0;

    this->setStyleSheet("background-color: white;");
    images.push_back(":tootorial/res/tootorial/first.png");
    images.push_back(":/tootorial/res/tootorial/hot_keys.png");
    images.push_back(":/tootorial/res/tootorial/socials.png");
    images.push_back(":/tootorial/res/tootorial/text_wrapingNew.png");
    images.push_back(":/tootorial/res/tootorial/color_mode.png");
    images.push_back(":/tootorial/res/tootorial/menuNew.png");
    ui->setupUi(this);
    ui->label->clear();
    ui->label->setPixmap(QPixmap(images[0]));
    ui->nextButton->setIconSize(QSize(150,80));
    ui->prevButton->setIconSize(QSize(150,80));
    ui->nextButton->setIcon(QIcon(":/buttons/res/next.png"));
    ui->prevButton->setIcon(QIcon(":/buttons/res/prev.png"));
}

TotorialWidget::~TotorialWidget()
{
    delete ui;
}

void TotorialWidget::on_prevButton_clicked()
{
    if (imageNumber > 0)
    {
        imageNumber--;
        ui->label->setPixmap(QPixmap(images[imageNumber]));
    }
}

void TotorialWidget::on_nextButton_clicked()
{
    if (imageNumber < images.size() - 1)
    {
        imageNumber++;
        ui->label->setPixmap(QPixmap(images[imageNumber]));
    }
}

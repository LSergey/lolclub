#ifndef QUOTEVIEW_H
#define QUOTEVIEW_H

#include <QWidget>
#include <QTextCodec>
#include "vkAuth.h"

namespace Ui {
class QuoteView;
}

class QuoteView : public QWidget
{
    Q_OBJECT
    
public:
    explicit QuoteView(QWidget *parent = 0);
    ~QuoteView();

    void setNumAndRate(QString&, QString&, const int);
    void setQuote(QString&, const int);

signals:
    void runVkPost(QString&);
    void runFbPost(QString&);
    void saveToTopSignal(QString&, QString&);

private slots:
    void on_socialButton_clicked();
    void vkPost_cliced();
    void fbPost_cliced();
    void saveToTop_cliced();

private:
    Ui::QuoteView *ui;
    QAction *vkAction_, *fbAction_, *saveToTopAction_;
    QMenu *socialMenu;
};

#endif // QUOTEVIEW_H

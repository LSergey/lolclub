#include "nameToSaveWindow.h"
#include "ui_nameToSaveWindow.h"

NameToSaveWindow::NameToSaveWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NameToSaveWindow)
{
    ui->setupUi(this);
}

NameToSaveWindow::~NameToSaveWindow()
{
    delete ui;
}

void NameToSaveWindow::on_saveNameButton_clicked()
{
    emit nameSavedPage(ui->nameEdit->text());

    this->close();
}

void NameToSaveWindow::setStartOfsavedName(QString startName)
{
    ui->nameEdit->setText(startName);
}

void NameToSaveWindow::on_pushButton_clicked()
{
    this->close();
}

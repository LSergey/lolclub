#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "windows.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->hide();
    ui->setupUi(this);
    this->hide();

    bool mainAppClosed = false;

    while(!mainAppClosed)
    {
        mainAppClosed = checkForClosed();
        Sleep(5000);
    }

    Sleep(7000);
    if (!QDir("OldExec").exists())
        QDir().mkdir("OldExec");

    QFile exec("lolclub.exe");
    exec.copy("OldExec/lolclub.exe");

    QFile toRemove("lolclub.exe");
    toRemove.remove();

    QFile newExec("NewExec/lolclub.exe");
    newExec.copy("lolclub.exe");

    QDir current("");
    current.rmdir("Closed");
    this->close();
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::checkForClosed()
{
    return QDir("Closed").exists();
}
